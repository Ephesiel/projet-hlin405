<?php
require_once __DIR__ . '/../core-tests-header.php';
use PHPUnit\Framework\TestCase;

/**
 * Cette classe a pour but de permettre les tests sur la classe abstraite \Core\Renderer\PageRenderer.
 * Les méthodes abstraites font le minimum de ce qu'elles sont censées faire.
 * L'attribut public $user_permissions permet de changer les permissions à tester.
 * 
 * @author Benoît Huftier
 */
final class NonAbstractPageRenderer extends \Core\Renderer\PageRenderer {
    public \Core\Permissions $user_permissions;

    public function __construct() {
        parent::__construct();
        $this->user_permissions = new \Core\Permissions(0);
    }

    public function forbidAccessIfNotPermitted() : void {
        $this->testPermissionsForAndForbidIfNeeded($this->user_permissions);
    }

    public function setUserPermissions(int $permissions) : void {
        $this->user_permissions = new \Core\Permissions($permissions);
    }

    protected function make() : \DOMDocument {
        $doc = $this->generatePage();
        $this->removeNotAllowedNodes($doc, $this->user_permissions);
        return $doc;
    }
}

final class TestPageRenderer extends TestCase {
    public function testNonExistingBody() : void {
        $page_renderer = new NonAbstractPageRenderer;

        $this->expectException(Exception::class);
        $page_renderer->setBody(__DIR__ . '/includes/non-existing-body.php');
    }

    public function testAddBodySimple() : void {
        $page_renderer = new NonAbstractPageRenderer;
        $page_renderer->setBody(__DIR__ . '/includes/simple-body.php');

        $content = preg_quote(file_get_contents(__DIR__ . '/includes/simple-body.php'), '/');
        $this->expectOutputRegex("/(?s)<body>.*$content.*<\/body>/");
        $page_renderer->render();
    }

    public function testAddBodyNotSimple() : void {
        $page_renderer = new NonAbstractPageRenderer;
        $page_renderer->setBody(__DIR__ . '/includes/not-simple-body.php');

        $content = preg_quote(file_get_contents(__DIR__ . '/includes/not-simple-body.php'), '/');
        $this->expectOutputRegex("/(?s)<body>.*$content.*<\/body>/");
        $page_renderer->render();
    }

    public function testAddCSS() : void {
        $page_renderer = new NonAbstractPageRenderer;
        $page_renderer->setBody(__DIR__ . '/includes/simple-body.php');
        $page_renderer->addStylesheets(array(
            new \Core\Tags\LinkCSS('test.css')
        ));

        $this->expectOutputRegex('/(?s)<head>.*<link [^>]*href="test\.css"[^>]*>.*<\/head>/');
        $page_renderer->render();
    }

    public function testAddMultipleCSS() : NonAbstractPageRenderer {
        $page_renderer = new NonAbstractPageRenderer;
        $page_renderer->setBody(__DIR__ . '/includes/simple-body.php');
        $page_renderer->addStylesheets(array(
            new \Core\Tags\LinkCSS('test.css'),
            new \Core\Tags\LinkCSS('test2.css')
        ));
        
        $this->assertTrue(true);
        return $page_renderer;
    }

    /**
     * @depends testAddMultipleCSS
     */
    public function testAddMultipleCSS1(NonAbstractPageRenderer $page_renderer) : void {
        $this->expectOutputRegex('/(?s)<head>.*<link [^>]*href="test\.css"[^>]*>.*<\/head>/');
        $page_renderer->render();
    }

    /**
     * @depends testAddMultipleCSS
     */
    public function testAddMultipleCSS2(NonAbstractPageRenderer $page_renderer) : void {
        $this->expectOutputRegex('/(?s)<head>.*<link [^>]*href="test2\.css"[^>]*>.*<\/head>/');
        $page_renderer->render();
    }

    public function testAddMultipleSameCSS() : void {
        $page_renderer = new NonAbstractPageRenderer;
        $page_renderer->setBody(__DIR__ . '/includes/simple-body.php');
        $page_renderer->addStylesheets(array(
            new \Core\Tags\LinkCSS('test.css'),
            new \Core\Tags\LinkCSS('test.css')
        ));
        
        $css = '<link [^>]*href="test\.css"[^>]*>';
        $this->expectOutputRegex("/(?s)<head>((?!($css)).)*$css((?!($css)).)*<\/head>/");
        $page_renderer->render();
    }

    public function testAddJS() : void {
        $page_renderer = new NonAbstractPageRenderer;
        $page_renderer->setBody(__DIR__ . '/includes/simple-body.php');
        $page_renderer->addScripts(array(
            new \Core\Tags\ScriptJS('test.js')
        ));

        $this->expectOutputRegex('/(?s)<body>.*<script [^>]*src="test\.js"[^>]*>[^<>]*<\/script>.*<\/body>/');
        $page_renderer->render();
    }

    public function testAddMultipleJS() : NonAbstractPageRenderer {
        $page_renderer = new NonAbstractPageRenderer;
        $page_renderer->setBody(__DIR__ . '/includes/simple-body.php');
        $page_renderer->addScripts(array(
            new \Core\Tags\ScriptJS('test.js'),
            new \Core\Tags\ScriptJS('test2.js')
        ));
        
        $this->assertTrue(true);
        return $page_renderer;
    }

    /**
     * @depends testAddMultipleJS
     */
    public function testAddMultipleJS1(NonAbstractPageRenderer $page_renderer) : void {
        $this->expectOutputRegex('/(?s)<body>.*<script [^>]*src="test\.js"[^>]*>[^<>]*<\/script>.*<\/body>/');
        $page_renderer->render();
    }

    /**
     * @depends testAddMultipleJS
     */
    public function testAddMultipleJS2(NonAbstractPageRenderer $page_renderer) : void {
        $this->expectOutputRegex('/(?s)<body>.*<script [^>]*src="test2\.js"[^>]*>[^<>]*<\/script>.*<\/body>/');
        $page_renderer->render();
    }

    public function testAddMultipleSameJS() : void {
        $page_renderer = new NonAbstractPageRenderer;
        $page_renderer->setBody(__DIR__ . '/includes/simple-body.php');
        $page_renderer->addScripts(array(
            new \Core\Tags\ScriptJS('test.js'),
            new \Core\Tags\ScriptJS('test.js')
        ));
        
        $script = '<script [^>]*src="test\.js"[^>]*>[^<>]*<\/script>';
        $this->expectOutputRegex("/(?s)<body>((?!($script)).)*$script((?!($script)).)*<\/body>/");
        $page_renderer->render();
    }

    public function testAddMeta() : NonAbstractPageRenderer {
        $page_renderer = new NonAbstractPageRenderer;
        $page_renderer->setBody(__DIR__ . '/includes/simple-body.php');
        $page_renderer->addOrSetMetaTags(array(
            new \Core\Tags\MetaCharsetTag('ascii'),
            new \Core\Tags\MetaContentTag('description', 'value'),
            new \Core\Tags\MetaHTTPEquivTag('truc', 'bidule'),
        ));

        $this->assertTrue(true);
        return $page_renderer;
    }

    /**
     * @depends testAddMeta
     */
    public function testAddMeta1(NonAbstractPageRenderer $page_renderer) : void {
        $this->expectOutputRegex('/(?s)<head>.*<meta [^>]*charset="ascii"[^>]*>.*<\/head>/');
        $page_renderer->render();
    }

    /**
     * @depends testAddMeta
     */
    public function testAddMeta2(NonAbstractPageRenderer $page_renderer) : void {
        $this->expectOutputRegex('/(?s)<head>.*<meta [^>]*name="description"[^>]*content="value"[^>]*>.*<\/head>/');
        $page_renderer->render();
    }

    /**
     * @depends testAddMeta
     */
    public function testAddMeta3(NonAbstractPageRenderer $page_renderer) : void {
        $this->expectOutputRegex('/(?s)<head>.*<meta [^>]*http-equiv="truc"[^>]*content="bidule"[^>]*>.*<\/head>/');
        $page_renderer->render();
    }

    public function testAddSameMeta() : NonAbstractPageRenderer {
        $page_renderer = new NonAbstractPageRenderer;
        $page_renderer->setBody(__DIR__ . '/includes/simple-body.php');
        $page_renderer->addOrSetMetaTags(array(
            new \Core\Tags\MetaCharsetTag('ascii'),
            new \Core\Tags\MetaCharsetTag('ascii'),
            new \Core\Tags\MetaContentTag('description', 'value'),
            new \Core\Tags\MetaContentTag('description', 'value'),
            new \Core\Tags\MetaHTTPEquivTag('truc', 'bidule'),
            new \Core\Tags\MetaHTTPEquivTag('truc', 'bidule'),
        ));

        $this->assertTrue(true);
        return $page_renderer;
    }

    /**
     * @depends testAddSameMeta
     */
    public function testAddSameMeta1(NonAbstractPageRenderer $page_renderer) : void {
        $meta = '<meta [^>]*charset="ascii"[^>]*>';
        $this->expectOutputRegex("/(?s)<head>((?!($meta)).)*$meta((?!($meta)).)*<\/head>/");
        $page_renderer->render();
    }

    /**
     * @depends testAddSameMeta
     */
    public function testAddSameMeta2(NonAbstractPageRenderer $page_renderer) : void {
        $meta = '<meta [^>]*name="description"[^>]*content="value"[^>]*>';
        $this->expectOutputRegex("/(?s)<head>((?!($meta)).)*$meta((?!($meta)).)*<\/head>/");
        $page_renderer->render();
    }

    /**
     * @depends testAddSameMeta
     */
    public function testAddSameMeta3(NonAbstractPageRenderer $page_renderer) : void {
        $meta = '<meta [^>]*http-equiv="truc"[^>]*content="bidule"[^>]*>*>';
        $this->expectOutputRegex("/(?s)<head>((?!($meta)).)*$meta((?!($meta)).)*<\/head>/");
        $page_renderer->render();
    }

    public function testTitle() : void {
        $page_renderer = new NonAbstractPageRenderer;
        $page_renderer->setBody(__DIR__ . '/includes/simple-body.php');
        $page_renderer->setTitle('Ma page');

        $this->expectOutputRegex('/(?s)<head>.*<title>Ma page<\/title>.*<\/head>/');
        $page_renderer->render();
    }

    public function testMultipleTitle() : void {
        $page_renderer = new NonAbstractPageRenderer;
        $page_renderer->setBody(__DIR__ . '/includes/simple-body.php');
        $page_renderer->setTitle('Ma page');
        $page_renderer->setTitle('Mon titre');
        $page_renderer->setTitle('Mon vrai titre');

        $this->expectOutputRegex('/(?s)<head>.*<title>Mon vrai titre<\/title>.*<\/head>/');
        $page_renderer->render();
    }

    public function testPermission() : NonAbstractPageRenderer {
        $page_renderer = new NonAbstractPageRenderer;
        $page_renderer->setBody(__DIR__ . '/includes/permission-body.php');
        $page_renderer->addRestrictedSections(array(
            RoleTest::Administrator => array('admin-only'),
            RoleTest::Moderator => array('modo-only', 'modo-restricted'),
            RoleTest::Connected => array('connected-only'),
            RoleTest::Public => array('public-only')
        ));
        $page_renderer->addRestrictedSections(array(
            RoleTest::Administrator => array('admin-restricted')
        ));

        $this->assertTrue(true);
        return $page_renderer;
    }

    /**
     * @depends testPermission
     */
    public function testPermission1(NonAbstractPageRenderer $page_renderer) : NonAbstractPageRenderer {
        $this->expectOutputRegex('/(?s)^((?!(<div class="(admin-only|modo-only|connected-only|public-only|modo-restricted|admin-restricted)">Lorem ipsum<\/div>)).)*$/');
        $page_renderer->render();
        return $page_renderer;
    }

    /**
     * @depends testPermission1
     */
    public function testPermission2(NonAbstractPageRenderer $page_renderer) : NonAbstractPageRenderer {
        $this->expectOutputRegex('/(?s).*<div class="all">Lorem ipsum<\/div>.*/');
        $page_renderer->render();
        return $page_renderer;
    }

    /**
     * @depends testPermission2
     */
    public function testPermission3(NonAbstractPageRenderer $page_renderer) : NonAbstractPageRenderer {
        $page_renderer->setUserPermissions(RoleTest::Administrator);
        $this->expectOutputRegex('/(?s)^((?!(<div class="(modo-only|public-only|modo-restricted)">Lorem ipsum<\/div>)).)*$/');
        $page_renderer->render();
        return $page_renderer;
    }

    /**
     * @depends testPermission3
     */
    public function testPermission4(NonAbstractPageRenderer $page_renderer) : NonAbstractPageRenderer {
        $this->expectOutputRegex('/(?s).*<div class="admin-only">Lorem ipsum<\/div>.*<div class="connected-only">Lorem ipsum<\/div>.*<div class="all">Lorem ipsum<\/div>.*<div class="admin-restricted">Lorem ipsum<\/div>.*/');
        $page_renderer->render();
        return $page_renderer;
    }

    /**
     * @depends testPermission4
     */
    public function testPermission5(NonAbstractPageRenderer $page_renderer) : NonAbstractPageRenderer {
        $page_renderer->setUserPermissions(RoleTest::Public);
        $this->expectOutputRegex('/(?s)^((?!(<div class="(admin-only|modo-only|connected-only|modo-restricted|admin-restricted)">Lorem ipsum<\/div>)).)*$/');
        $page_renderer->render();
        return $page_renderer;
    }

    /**
     * @depends testPermission5
     */
    public function testPermission6(NonAbstractPageRenderer $page_renderer) : NonAbstractPageRenderer {
        $this->expectOutputRegex('/(?s).*<div class="public-only">Lorem ipsum<\/div>.*<div class="all">Lorem ipsum<\/div>.*/');
        $page_renderer->render();
        return $page_renderer;
    }

    /**
     * @depends testPermission6
     */
    public function testPermission7(NonAbstractPageRenderer $page_renderer) : NonAbstractPageRenderer {
        $page_renderer->setUserPermissions(RoleTest::Moderator | RoleTest::Administrator);
        $this->expectOutputRegex('/(?s)^((?!(<div class="public-only">Lorem ipsum<\/div>)).)*$/');
        $page_renderer->render();
        return $page_renderer;
    }

    /**
     * @depends testPermission7
     */
    public function testPermission8(NonAbstractPageRenderer $page_renderer) : void {
        $this->expectOutputRegex('/(?s).*<div class="admin-only">Lorem ipsum<\/div>.*<div class="modo-only">Lorem ipsum<\/div>.*<div class="connected-only">Lorem ipsum<\/div>.*<div class="all">Lorem ipsum<\/div>.*<div class="admin-restricted">Lorem ipsum<\/div>.*<div class="modo-restricted">Lorem ipsum<\/div>.*/');
        $page_renderer->render();
    }

    public function actionOnForbidden() : void {
        throw new \Exception('test forbidden');
    }

    public function testForbidden1() : void {
        $page_renderer = new NonAbstractPageRenderer;
        $page_renderer->setActionOnForbidden(array($this, 'actionOnForbidden'));

        try {
            $page_renderer->forbidAccessIfNotPermitted();
        }
        catch (\Exception $e) {
            $this->fail("Une exception a été levée et n'aurait pas dû : {$e->getMessage()}");
        }

        $page_renderer->setUserPermissions(RoleTest::Public);

        try {
            $page_renderer->forbidAccessIfNotPermitted();
        }
        catch (\Exception $e) {
            $this->fail("Une exception a été levée et n'aurait pas dû : {$e->getMessage()}");
        }

        $page_renderer->setUserPermissions(RoleTest::Administrator);

        try {
            $page_renderer->forbidAccessIfNotPermitted();
        }
        catch (\Exception $e) {
            $this->fail("Une exception a été levée et n'aurait pas dû : {$e->getMessage()}");
        }

        $page_renderer->setUserPermissions(RoleTest::Moderator);

        try {
            $page_renderer->forbidAccessIfNotPermitted();
        }
        catch (\Exception $e) {
            $this->fail("Une exception a été levée et n'aurait pas dû : {$e->getMessage()}");
        }

        $this->assertTrue(true);
    }

    public function testForbidden2() : void {
        $page_renderer = new NonAbstractPageRenderer;
        $page_renderer->setActionOnForbidden(array($this, 'actionOnForbidden'));
        $page_renderer->setPermissionsNeeded(RoleTest::Connected);

        try {
            $page_renderer->forbidAccessIfNotPermitted();
            $this->fail('L\'utilisateur n\'est pas censé avoir le droit de regarder la page');
        }
        catch (\Exception $e) {
            $this->assertSame($e->getMessage(), 'test forbidden');
        }

        $page_renderer->setUserPermissions(RoleTest::Public);

        try {
            $page_renderer->forbidAccessIfNotPermitted();
            $this->fail('L\'utilisateur n\'est pas censé avoir le droit de regarder la page');
        }
        catch (\Exception $e) {
            $this->assertSame($e->getMessage(), 'test forbidden');
        }

        $page_renderer->setUserPermissions(RoleTest::Administrator);

        try {
            $page_renderer->forbidAccessIfNotPermitted();
        }
        catch (\Exception $e) {
            $this->fail("Une exception a été levée et n'aurait pas dû : {$e->getMessage()}");
        }

        $page_renderer->setUserPermissions(RoleTest::Moderator);

        try {
            $page_renderer->forbidAccessIfNotPermitted();
        }
        catch (\Exception $e) {
            $this->fail("Une exception a été levée et n'aurait pas dû : {$e->getMessage()}");
        }
    }

    public function testForbidden3() : void {
        $page_renderer = new NonAbstractPageRenderer;
        $page_renderer->setActionOnForbidden(array($this, 'actionOnForbidden'));
        $page_renderer->setPermissionsNeeded(RoleTest::Administrator);

        try {
            $page_renderer->forbidAccessIfNotPermitted();
            $this->fail('L\'utilisateur n\'est pas censé avoir le droit de regarder la page');
        }
        catch (\Exception $e) {
            $this->assertSame($e->getMessage(), 'test forbidden');
        }

        $page_renderer->setUserPermissions(RoleTest::Public);

        try {
            $page_renderer->forbidAccessIfNotPermitted();
            $this->fail('L\'utilisateur n\'est pas censé avoir le droit de regarder la page');
        }
        catch (\Exception $e) {
            $this->assertSame($e->getMessage(), 'test forbidden');
        }

        $page_renderer->setUserPermissions(RoleTest::Moderator);

        try {
            $page_renderer->forbidAccessIfNotPermitted();
            $this->fail('L\'utilisateur n\'est pas censé avoir le droit de regarder la page');
        }
        catch (\Exception $e) {
            $this->assertSame($e->getMessage(), 'test forbidden');
        }

        $page_renderer->setUserPermissions(RoleTest::Administrator);

        try {
            $page_renderer->forbidAccessIfNotPermitted();
        }
        catch (\Exception $e) {
            $this->fail("Une exception a été levée et n'aurait pas dû : {$e->getMessage()}");
        }

        $page_renderer->setUserPermissions(RoleTest::Administrator | RoleTest::Moderator);

        try {
            $page_renderer->forbidAccessIfNotPermitted();
        }
        catch (\Exception $e) {
            $this->fail("Une exception a été levée et n'aurait pas dû : {$e->getMessage()}");
        }
    }
}