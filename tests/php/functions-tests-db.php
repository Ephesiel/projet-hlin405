<?php

/**
 * Renvoie une valeur valide pour une clé unique n'étant pas encore dans la base de
 * donnée pour la table donnée.
 * 
 * @param string $table_name Le nom de la table où l'on veut chercher cette clé unique.
 * @param string $id_row     Le nom du champ "id" de la table. Défault à "id"
 * @param string $id_type    Le type de donnée du champ id. Il peut être à 3 valeurs
 *                           différentes : "int", "string" et "date". Le défaut est "int"
 * @param array  $params     Des paramètres spécifiques selon le type du champ :
 *                           - int : "min" => le minimum
 *                                   "max" => le maximum
 *                           - string : "length" => la taille de la clé à générer 
 *                           - date : "min" => date minimale
 *                                    "max" => date maximale
 * @return mixed             La nouvelle clé, un int si c'est un type "int", en string
 *                           sinon
 * 
 * @author Benoît Huftier
 */
function ph_test_create_new_unique_value_for_table(string $table_name, string $id_row = 'id', string $id_type = 'int', array $params = array()) : mixed {
    global $phdb;

    $statement = $phdb->query("SELECT $id_row FROM $table_name");
    $fetch = $statement->fetchAll();

    $ids = array();

    foreach ($fetch as $value) {
        $ids[] = $value[$id_row];
    }
    
    switch ($id_type) {
        case 'int':
            return ph_test_create_random_int_not_in_array($ids, $params);
        case 'string':
            return ph_test_create_random_string_not_in_array($ids, $params);
        case 'date':
            return ph_test_create_random_date_not_in_array($ids, $params);
    }
}

function ph_test_create_random_int_not_in_array(array $ints, array $params) : int {
    $min = array_key_exists('min', $params) ? $params['min'] : 1;
    $max = array_key_exists('max', $params) ? $params['max'] : 9999;
    do {
        $value = rand($min, $max);
    } while (in_array($value, $ints, $strict = false)); // <- Strict false car PDO renvoie des string

    return $value;
}

function ph_test_create_random_string_not_in_array(array $strings, array $params) : string {
    $length = array_key_exists('length', $params) ? $params['length'] : 10;
    $chars = array_key_exists('chars', $params) ? $params['chars'] : '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

    $chars_length = strlen($chars);
    $value = '';
    
    do {
        for ($i = 0; $i < $length; $i++) {
            $value .= $chars[rand(0, $chars_length - 1)];
        }
    } while (in_array($value, $strings, $strict = true));

    return $value;
}

function ph_test_create_random_date_not_in_array(array $dates, array $params) : string {
    // Les dates renvoyées par la bdd ont toujours ce format
    $format = 'Y-m-d h:i:s';
    $min = array_key_exists('min', $params) ? strtotime($params['min']) : 0; // <- '1970-01-01 00:00:00'
    $max = array_key_exists('max', $params) ? strtotime($params['max']) : 4102358400; // <- '2099-12-31 00:00:00'
    do {
        $value = date($format, rand($min, $max));
    } while (in_array($value, $dates, $strict = true));

    return $value;

}