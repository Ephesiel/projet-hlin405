<?php 
require_once __DIR__ . '/../site-tests-header.php';
use PHPUnit\Framework\TestCase;

final class TestTypeEnum extends TestCase {

    // ------------------------------------------------------------------------
    // Tests conversion string -> Type.

    public function stringProvider() : array {
        return array(
            array('Coupe', Type::Coupe),
            array('Championnat', Type::Championnat),
            array('Poules', Type::Poules),
        );
    }

    /**
     * @dataProvider stringProvider
     */
    public function testFromString(string $string, int $expected) : void {
        $type = Type::fromString($string);
        $this->assertSame($type, $expected);
    }

    // ------------------------------------------------------------------------
    // Test conversion Type -> string 

    public function typeProvider() : array {
        return array(
            array(Type::Coupe, 'Coupe'),
            array(Type::Championnat, 'Championnat'),
            array(Type::Poules, 'Poules'),
        );
    }

    /**
     * @dataProvider typeProvider
     */
    public function testToString(int $type, string $expected) : void {
        $type = Type::toString($type);
        $this->assertSame($type, $expected);
    }

    // ------------------------------------------------------------------------
    // Test exceptions

    public function testFromStringException() : void {
        $this->expectException(Exception::class);
        $type = Type::fromString('');
    }

    public function testToStringException() : void {
        $this->expectException(Exception::class);
        $type = Type::toString(0);
    }
}