# Projet HLIN405 - Installation

![pipeline_develop](https://gitlab.com/Ephesiel/projet-hlin405/badges/develop/pipeline.svg)
![coverage_develop](https://gitlab.com/Ephesiel/projet-hlin405/badges/develop/coverage.svg)

Ce projet a été fait pour avoir une installation facile, rapide, et sans prise de tête. Il y a cependant quelques étapes à suivre pour que tout se passe parfaitement.

## 1 - Récupération des dépendances

Ce projet possède deux prérequis pour marcher : 
- PHP8.0 ([voir guide d'installation](https://gitlab.com/Ephesiel/projet-hlin405/-/wikis/Guides/Installation-de-PHP-8))
- PHP8.0-xml (actif par défaut sur WAMP, à installer sur UNIX, package de nom `php8.0-xml`)

Pour aller plus loin, et pouvoir exécuter les tests, `PHPUnit` peut aussi être installé : [voir page wiki](https://gitlab.com/Ephesiel/projet-hlin405/-/wikis/Guides/Installation-de-PHPUnit-(--exemple))

> Exécuter le petit script `phptests.sh` permet d'exécuter tous les tests et de générer une coverage du projet.

## 2 - Permissions apache

De plus, pour que toutes les pages soient accessibles, il faut mettre à jour le fichier les accès du site avec cette petite configuration (où `${SITE_DIRECTORY}` est le répertoire où vous avez installé le site) :
```apache
<Directory "${SITE_DIRECTORY}/src/public_html">
    Options FollowSymlinks
    AllowOverride All
    Require all granted
    Order Allow,Deny
    Allow from all
</Directory>
```

C'est assez direct sous UNIX, il suffit d'ajouter ces lignes dans la configuration du *virtual host*.

Sous `WAMP`, par contre, il faut modifier le `httpd.conf` d'`apache2`, qui doit se trouver à l'adresse suivante : `${INSTALL_DIR}/bin/apache2/conf/` 

## 3 - Configuration

Enfin, il faudra mettre à jour les fichiers de configuration. Tout d'abord, le fichier `env.cfg` qui se trouve à la racine du dossier `src`. Son contenu est le suivant : 
```
# Configuration de l'environnement de développement et de mise en place du serveur.
# La configuration ci-dessous est adaptée pour le serveur.

# Racine HTML & mode de développement
root: /
development: 0
```
Il faudra modifier les 2 lignes, pour mettre :
* La racine HTML du site (si le *virtual host* est configuré pour que la racine soit `${SITE_DIRECTORY}/src/public_html/`, laisser `/`, sinon, bien mettre `/src/public_html/`)
* Le mode de développement, à `1` de préférence, afin de pouvoir réparer la base de données en cas de pépin.

Dernière étape : éditer le fichier `config.php` qui se trouve, lui aussi, à la racine du dossier `src` :
```php
<?php
// ----------------------------------------------------------------------------
// Paramètres de la base de données.

define('DB_NAME', 'projet_hlin405'); 
define('DB_USER', 'root');           
define('DB_PASSWORD', 'root');
define('DB_HOST', 'localhost');
define('DB_CHARSET', 'utf8mb4');
define('DB_COLLATE', 'utf8mb4_general_ci');

// ----------------------------------------------------------------------------
// Configuration du compte administrateur. Peut être changé à tout moment!

define('ADMIN_EMAIL', 'webmaster@localhost');
define('ADMIN_NAME', 'admin');
define('ADMIN_PASSWD', 'root');

// ----------------------------------------------------------------------------
// Constantes utilitaires en mode développement

define('RESET_PLACEHOLDERS', false);
```
Il faut absolument :
* Modifier la constante `DB_USER` pour l'utilisateur définit dans votre base de données.
* Modifier la constante `DB_PASSWORD` pour le mot de passe définit dans votre base de données ; **ne peut pas être vide**.

Si vous êtes sous `Windows`, vous voudrez aussi modifier la constante `DB_HOST` pour mettre :
* `127.0.0.1:3306` si vous utilisez la base de données `MySql`.
* `127.0.0.1:3307` si vous utilisez la base de données `MariaDB`.

Vous aurez alors accès au site, avec un compte administrateur créé. Vous pouvez vous connecter en mettant les valeurs des constantes `ADMIN_EMAIL` et `ADMIN_PASSWD`. 

**RESET_PLACEHOLDERS** : ce sont des entrées en base de données qui permettent de tester les pages. Elle fournit des utilisateurs, des équipes, des tournois, bref, tout ce qu'il faut pour faire tourner le site. Si vous voulez essayer toutes les fonctionnalités, il faut le passer à `true`, rafraîchir la page, et le repasser à `false` (afin d'avoir une installation de ces données, mais de pouvoir les modifier dans le site !).