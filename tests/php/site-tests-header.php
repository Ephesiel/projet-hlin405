<?php
// S'occupe des inclusions nécessaires pour le bon fonctionnement des tests sur les classes du site.

declare(strict_types = 1);
require_once __DIR__ . '/../../src/load.php';
require_once __DIR__ . '/functions-tests-db.php';
require_once __DIR__ . '/class-multiple-test.php';