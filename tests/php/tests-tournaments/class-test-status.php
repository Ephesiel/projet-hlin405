<?php 
require_once __DIR__ . '/../site-tests-header.php';
use PHPUnit\Framework\TestCase;

final class TestStatus extends TestCase {

    // ------------------------------------------------------------------------
    // Tests conversion string -> Status.

    public function dateProvider() : array {
        return array(
            array('yesterday', 7, Status::OnGoing, false),
            array('tomorrow', 30, Status::Forthcoming, false),
            array('+ 3 days', 30, Status::PreRegistrations, false),
            array('1990-01-01', 30, Status::Finished, false),
            array('', 30, Status::Finished, true),
        );
    }

    /**
     * @dataProvider dateProvider
     */
    public function testFromDate(string $date, int $duration, int $expected, bool $expect_exception) : void {
        if ($expect_exception) {
            $this->expectException(Exception::class);
        }

        $end_inscriptions = new DateTime($date);
        $end_inscriptions->sub(new DateInterval('P1D'));
        $status = Status::fromDate($end_inscriptions->format('Y-m-d'), $date, $duration);

        if (!$expect_exception) {
            $this->assertSame($status, $expected);
        }
    }

    // ------------------------------------------------------------------------
    // Tests conversion Status -> string 

    public function statusProvider() : array {
        return array(
            array(Status::Finished, 'Terminé', false),
            array(Status::OnGoing, 'En cours', false),
            array(Status::Forthcoming, 'À venir', false),
            array(Status::PreRegistrations, 'Phase de pré-inscriptions', false),
            array(0, '', true),
        );
    }

    /**
     * @dataProvider statusProvider
     */
    public function testToString(int $status, string $expected, bool $expect_exception) : void {
        if ($expect_exception) {
            $this->expectException(Exception::class);
        }

        $status = Status::toString($status);
        
        if (!$expect_exception) {
            $this->assertSame($status, $expected);
        }
    }
}