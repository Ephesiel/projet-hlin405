<?php 
require_once __DIR__ . '/../site-tests-header.php';
use PHPUnit\Framework\TestCase;

final class TestSchema extends TestCase {
    private array $tables;

    protected function setUp() : void {
        $this->tables = array(
            'user',
            'role',
            'user_role',
            'player',
            'zip_code',
            'city',
            'location',
            'contact',
            'team',
            'player_team',
            'postulate_team',
            'tournament_type',
            'outcome_type',
            'tournament',
            'score_tournament',
            'team_match', 
            'team_result',
            'postulate_tournament',
        );
    }

    public function testDbSchema() : void {
        $db_schema = array_filter(explode(';', ph_get_db_schema()));
        $this->assertSame(count($db_schema), count($this->tables));
        foreach ($db_schema as $query) {
            preg_match('/^.*CREATE TABLE ([^(]+)\(.+\)$/', $query, $match);
            $this->assertTrue(isset($match[1]) && !empty($match[1]));
            $this->assertTrue(in_array($match[1], $this->tables, $strict = true));
        }
    }

    public function validAssetsProvider() : array {
        return array(
            array('valide'),
            array('VALIDE'),
            array('va_lide'),
            array('va-li-de'),
        );
    }

    public function invalidAssetsProvider() : array {
        return array(
            array(''),
            array('in va lide'),
            array('in\\va\\lide'),
            array('"nul '),
        );
    }

    /**
     * @dataProvider validAssetsProvider
     */
    public function testValidCreateTableNameRecuperation(string $table_name) : void {
        $query = "CREATE TABLE $table_name(id INT);";
        $to_evaluate = ph_get_table_name_from_schema($query);
        $this->assertSame($to_evaluate, $table_name);
    }

    /**
     * @dataProvider invalidAssetsProvider
     */
    public function testInvalidCreateTableNameRecuperation(string $table_name) : void {
        $query = "CREATE TABLE $table_name(id INT);";
        $to_evaluate = ph_get_table_name_from_schema($query);
        $this->assertTrue(empty($to_evaluate));
    }

    /**
     * @dataProvider validAssetsProvider
     */
    public function testValidInsertTableNameRecuperation(string $table_name) : void {
        $query = "INSERT INTO $table_name VALUES(1);";
        $to_evaluate = ph_get_table_name_from_row($query);
        $this->assertSame($to_evaluate, $table_name);
    }

    /**
     * @dataProvider invalidAssetsProvider
     */
    public function testInvalidInsertTableNameRecuperation(string $table_name) : void {
        $query = "INSERT INTO $table_name VALUES(1);";
        $to_evaluate = ph_get_table_name_from_row($query);
        $this->assertTrue(empty($to_evaluate));
    }

    public function validRowProvider() : array {
        return array(
            array('INSERT INTO test_table(email, nickname) VALUES(\'webmaster@localhost\', \'Admin\');', array('webmaster@localhost', 'Admin')),
            array('INSERT INTO table VALUES(1, \'webmaster@localhost\', \'Admin\');', array('1', 'webmaster@localhost', 'Admin')),
            array('INSERT INTO test_table(email, nickname) VALUES(\'webmaster@localhost\', \'Admin\')', array('webmaster@localhost', 'Admin')),
            array('INSERT INTO table VALUES(1, \'webmaster@localhost\', \'Admin\')', array('1', 'webmaster@localhost', 'Admin')),
        );
    }

    public function invalidRowProvider() : array {
        return array(
            array(''),
            array('CREATE TABLE nul(id INT);'),
            array('UPDATE nul SET id = 1 WHERE name = \'Admin\''),
            array('INSERT INTO asoeihjfgasoupiheg()'),
        );
    }
    
    /**
     * @dataProvider validRowProvider
     */
    public function testValidRowValuesRecuperation(string $query, array $fields) : void {
        $to_evaluate = ph_get_row_values($query);
        $this->assertSame($to_evaluate, $fields);
    }

    /**
     * @dataProvider invalidRowProvider
     */
    public function testInvalidRowValuesRecuperation(string $query) : void {
        $to_evaluate = ph_get_row_values($query);
        $this->assertTrue(empty($to_evaluate));
    }

    public function validSchemaProvider() : array {
        return array(
            array('CREATE TABLE test_table(id INT,name TEXT,email VARCHAR(255));', array('id', 'name', 'email')),
            array('CREATE TABLE test_table(id INT,name TEXT,email VARCHAR(255),foreign_id INT REFERENCES foreign(id));', array('id', 'name', 'email', 'foreign_id')),
            array('CREATE TABLE table(id INT,PRIMARY KEY(id))', array('id')),
        );
    }

    public function invalidSchemaProvider() : array {
        return array(
            array(''),
            array('UPDATE nul SET id = 1 WHERE name = \'Admin\''),
            array('INSERT INTO asoeihjfgasoupiheg()'),
            array('CREATE TABLE z\\z\\z'),
            array('CREATE TABLE test_table;'),
        );
    }
    
    /**
     * @dataProvider validSchemaProvider
     */
    public function testValidSchemaValuesRecuperation(string $query, array $fields) : void {
        $to_evaluate = ph_get_attributes_from_schema($query);
        $this->assertSame(array_keys($to_evaluate), $fields);
    }

    /**
     * @dataProvider invalidSchemaProvider
     */
    public function testInvalidSchemaValuesRecuperation(string $query) : void {
        $to_evaluate = ph_get_attributes_from_schema($query);
        $this->assertTrue(empty($to_evaluate));
    }

    /**
     * @group serverTestsOnly
     */
    public function testDumpFileName() : void {
        $filename = ABSPATH . '/php/database/dump.sql';
        $file = fopen($filename, 'w');
        fclose($file);

        $this->assertSame(ph_get_next_dump(), ABSPATH . '/php/database/dump_1.sql');
        unlink($filename);
    }
}