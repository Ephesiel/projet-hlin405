<?php
require_once __DIR__ . '/../core-tests-header.php';
use PHPUnit\Framework\TestCase;

final class TestConfigurationFileParser extends TestCase {
    
    public function testParseOfTestEnv() : void {
        $configuration_parser = new Core\ConfigurationFileParser(__DIR__ . '/testEnv.cfg');
        $configuration_parser->addMultipleAppearancesKey('root');
        $configuration_parser->readAndParseFile();
        try {
            $this->assertSame(array('/public_html/', 'C:\\wamp64\\www\\projet-hlin405\\src\\public_html'), $configuration_parser->getConfigurationValue('root'));
            $this->assertSame('/var/www/projet-hlin405/src/', $configuration_parser->getConfigurationValue('abspath'));
            $this->assertSame('www.google.com', $configuration_parser->getConfigurationValue('host'));
            $this->assertSame('admin', $configuration_parser->getConfigurationValue('login'));
            $this->assertSame('root', $configuration_parser->getConfigurationValue('passwd'));
            $this->assertFalse(is_null($configuration_parser->getConfigurationValue('empty')));
            $this->assertTrue(empty($configuration_parser->getConfigurationValue('empty')));
        }
        catch (Core\NoSuchConfigurationKeyException $e) {
            $this->fail("La clé {$e->getKey()} n'a pas été trouvée dans la configuration.");
        }
        
        try {
            $configuration_parser->getConfigurationValue('foobar');
            $this->fail('Exception attendue `NoSuchConfigurationKeyException` non envoyée.');
        }
        catch (Core\NoSuchConfigurationKeyException $e) {
            $this->assertEquals($e->getKey(), 'foobar');
        }
    }

    /**
     * @group localTestsOnly
     */
    public function testOfFailedOpening() : void {
        $configuration_parser = new Core\ConfigurationFileParser('a');
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('Le fichier n\'a pas pu être ouvert.');
        $configuration_parser->readAndParseFile(function() {
            throw new Exception('Le fichier n\'a pas pu être ouvert.');
        });
    }

    /**
     * @group localTestsOnly
     */
    public function testParseOfLocalEnv() : void {
        $configuration_parser = new Core\ConfigurationFileParser(CORE . '/../env.cfg');
        $configuration_parser->readAndParseFile();
        try {
            $root_value = $configuration_parser->getConfigurationValue('root');
            $this->assertFalse(is_null($root_value) || empty($root_value));
            $dev_value = $configuration_parser->getConfigurationValue('development');
            $this->assertFalse(is_null($dev_value));
            $this->assertEquals($dev_value, '1');
        }
        catch (Core\NoSuchConfigurationKeyException $e) {
            $this->fail("La clé {$e->getKey()} n'a pas été trouvée dans la configuration.");
        }
    }

    /**
     * @group serverTestsOnly
     */
    public function testParseOfServerEnv() : void {
        $configuration_parser = new Core\ConfigurationFileParser(CORE . '/../env.cfg');
        $configuration_parser->readAndParseFile();
        try {
            $root_value = $configuration_parser->getConfigurationValue('root');
            $this->assertEquals($root_value, '/');
            $dev_value = $configuration_parser->getConfigurationValue('development');
            $this->assertEquals($dev_value, '0');
        }
        catch (Core\NoSuchConfigurationKeyException $e) {
            $this->fail("La clé {$e->getKey()} n'a pas été trouvée dans la configuration.");
        }
    }
}