<?php 
require_once __DIR__ . '/../site-tests-header.php';
use PHPUnit\Framework\TestCase;

final class TestCache extends TestCase {

    /**
     * @group longTests
     */
    public function testValidity() : void {
        $cache = new PH\Database\Cache('+3 seconds');
        $cache->set('test', 'test');
        sleep(5);
        $this->assertFalse($cache->isValid('test'));
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('La clé test n\'est pas valide.');
        $cache->get('test');
    }

    public function getProvider() : array {
        return array(
            array('test', 'une chaîne classique'),
            array('huitre', array('data' => array(1, 2, 3), 'valid' => true)),
            array('entier', 2),
        );
    }

    /**
     * @dataProvider getProvider
     */
    public function testGet(string $key, mixed $value) : void {
        $cache = new PH\Database\Cache('+5 seconds');
        $cache->set($key, $value);
        $this->assertSame($cache->get($key), $value);

        $this->expectException(Exception::class);
        $this->expectExceptionMessage('La clé ' . $key . 'LUL n\'est pas valide.');
        $cache->get($key . 'LUL');
    }
}