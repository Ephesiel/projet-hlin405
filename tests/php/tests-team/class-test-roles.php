<?php
require_once __DIR__ . '/../site-tests-header.php';
use PHPUnit\Framework\TestCase;

final class TestRoles extends TestCase {
    public function testRoleValues() : void {
        $this->assertEquals(Role::Administrator, 1);
        $this->assertEquals(Role::Manager, 2);
        $this->assertEquals(Role::Player, 4);
        $this->assertEquals(Role::Public, 8);
        $this->assertEquals(Role::Connected, 7);
        $this->assertEquals(Role::All, 15);
    }

    public function providerRoleString() : array {
        return array(
            array('Administrateur', Role::Administrator),
            array('Gestionnaire de tournois', Role::Manager),
            array('Joueur', Role::Player),
            array('administrateur', Role::Public),
            array('gestionnaire de tournois', Role::Public),
            array('joueur', Role::Public),
            array('public', Role::Public),
            array('sasa', Role::Public),
            array('capitaine', Role::Public),
            array('Publique', Role::Public)
        );
    }

    /**
     * @depends testRoleValues
     * @dataProvider providerRoleString
     */
    public function testRoleString(string $name, int $expected) : void {
        $this->assertEquals(Role::fromString($name), $expected);
    }

    public function permsProvider() : array {
        return array(
            array(
                new Core\Permissions(Role::Administrator),
                array('Administrateur')
            ),
            array(
                new Core\Permissions(Role::Administrator | Role::Manager),
                array('Administrateur', 'Gestionnaire de tournois')
            ),
            array(
                new Core\Permissions(Role::All),
                array('Administrateur', 'Gestionnaire de tournois', 'Joueur')
            ),
            array(
                new Core\Permissions(0),
                array()
            )
        );
    }

    /**
     * @dataProvider permsProvider
     */
    public function testRoles(Core\Permissions $perms, array $strings) : void {
        $this->assertSame(Role::toArray($perms), $strings);
    }
}