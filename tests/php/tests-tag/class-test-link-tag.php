<?php
require_once __DIR__ . '/../core-tests-header.php';
use PHPUnit\Framework\TestCase;

final class TestLinkTag extends TestCase {

    public function testEmptyTag() : void {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage("Un lien ne peut pas être nul ou vide.");
        $script_tag = new Core\Tags\LinkTag(''); 
    }

    public function testLinkTag() : void {
        $link_tag1 = new Core\Tags\LinkTag('on teste des liens !');
        $this->assertSame($link_tag1->make(), '<link href=\'on teste des liens !\' />');
        
        $this->expectOutputString('<link href=\'on teste des liens !\' />');
        $link_tag1->render();

        $link_tag2 = new Core\Tags\LinkTag("on teste des liens !");
        $this->assertSame($link_tag2->isEquivalent($link_tag1), true);
        $link_tag3 = new Core\Tags\LinkTag('n\'importe quoi');
        $this->assertSame($link_tag2->isEquivalent($link_tag3), false);

    }
    
    public function attributesProvider() : array {
        return array(
            array('je ne sais pà', 'sa fé mal', 'no-referrer', 'anonymous'),
            array('truc du genre 85èçè-', 'tu trouve ?', 'origin', 'use-credentials'),
            array('eh ça fait &é":', 'Oh ! tu fait quoi là !!!!', 'salut mon gars', 'use-credentials'),
            array('j\'ai pas de chance', 'oupsi', 'unsafe-url', 'screpeu'),
            array('tu crois""\'\'"""\'?"', 't\'es "BG" toi tu sais !', 'cfbcbhnfvbnvgj,vg', 'shhhhhh'),
        );
    }

    /**
     * @dataProvider attributesProvider
     */
    public function testSetAttributesString(string $type, string $integrity, string $referrer_policy, string $cross_origin) : void {

        $link_tag = new Core\Tags\LinkTag('juste pour le test');

        $valid_cross_origin = Core\Tags\is_cross_origin($cross_origin);
        $valid_referrer_policy = Core\Tags\is_referrer_policy($referrer_policy);

        if (false === $valid_cross_origin || false === $valid_referrer_policy) {
            $this->expectException(Exception::class);
        }
        $link_tag->setCrossOrigin($cross_origin);
        $link_tag->setReferrerPolicy($referrer_policy);
        $link_tag->setType($type);
        $link_tag->setIntegrity($integrity);
        
        $tester = $link_tag->make();

        $cross_origin = preg_quote($cross_origin);
        $referrer_policy = preg_quote($referrer_policy);
        $type = preg_quote($type);
        $integrity = preg_quote($integrity);

        if (true === $valid_cross_origin) {
            $regex = "/^<link[^<>]* crossorigin=('$cross_origin'|\"$cross_origin\")[^<>]*\/>[^<>]*$/";
            $this->assertMatchesRegularExpression($regex, $tester);
        }
        else {
            $regex = "/^<link((?!( crossorigin=)).)*\/>[^<>]*$/";
            $this->assertMatchesRegularExpression($regex, $tester);
        }

        if (true === $valid_referrer_policy) {
            $regex = "/^<link[^<>]* referrerpolicy=('$referrer_policy'|\"$referrer_policy\")[^<>]*\/>[^<>]*$/";
            $this->assertMatchesRegularExpression($regex, $tester);
        }
        else {
            $regex = "/^<link((?!( referrerpolicy=)).)*\/>[^<>]*$/";
            $this->assertMatchesRegularExpression($regex, $tester);
        }
        
        $regex = "/^<link[^<>]* type=('$type'|\"$type\")[^<>]*\/>$/";
        $this->assertMatchesRegularExpression($regex, $tester);
        $regex = "/^<link[^<>]* integrity=('$integrity'|\"$integrity\")[^<>]*\/>/";
        $this->assertMatchesRegularExpression($regex, $tester);
    }

    /**
     * @dataProvider attributesProvider
     */
    public function testRender(string $type, string $integrity, string $referrer_policy, string $cross_origin) : void {
        $link_tag = new Core\Tags\LinkTag('index.js');
        $valid_cross_origin = Core\Tags\is_cross_origin($cross_origin);
        $valid_referrer_policy = Core\Tags\is_referrer_policy($referrer_policy);

        if (false === $valid_cross_origin || false === $valid_referrer_policy) {
            $this->expectException(Exception::class);
        }
        $link_tag->setCrossOrigin($cross_origin);
        $link_tag->setReferrerPolicy($referrer_policy);
        $link_tag->setType($type);
        $link_tag->setIntegrity($integrity);

        $cross_origin = preg_quote($cross_origin);
        $referrer_policy = preg_quote($referrer_policy);
        $type = preg_quote($type);
        $integrity = preg_quote($integrity);

        $possibilities = '(type|integrity';
        $attributes = "('$type'|\"$type\"|'$integrity'|\"$integrity\"";
        if (true === $valid_cross_origin) {
            $possibilities .= '|crossorigin';
            $attributes .= "|'$cross_origin'|\"$cross_origin\"";
        }
        if (true === $valid_referrer_policy) {
            $possibilities .= '|referrerpolicy';
            $attributes .= "|'$referrer_policy'|\"$referrer_policy\"";
        }
        $possibilities .= ')=' . $attributes . ')';

        $regex = "/^<link[^<>]* $possibilities $possibilities";
        if (true === $valid_referrer_policy) {
            $regex .= " $possibilities";
        }
        if (true === $valid_cross_origin) {
            $regex .= " $possibilities";
        }
        $regex .= '[^<>]*\/>$/';

        $this->expectOutputRegex($regex);
        $link_tag->render();
    }

    public function relProvider() : array {
        return array(
            array('author', true),
            array('noob', false),
            array('dns-prefetch', true),
            array('test', false),
        );
    }

    /**
     * @dataProvider relProvider
     */
    public function testRel(string $rel, bool $is_valid) : void {
        $link_tag = new Core\Tags\LinkTag('stylesheet.css');

        if (false === $is_valid) {
            $this->expectException(Exception::class);
            $this->expectExceptionMessage("Valeur de rel invalide : $rel");
        }

        $link_tag->setRel($rel);

        $regex = '';
        if (true === $is_valid) {
            $regex = "/^.*<link[^<>]* rel=('$rel'|\"$rel\")[^<>]*\/>.*$/";
        }
        else {
            $regex = "/^<link((?!( rel=)).)*\/>$/";
        }
        $this->assertMatchesRegularExpression($regex, $link_tag->make());
    }

    public function othProvider() : array {
        return array(
            array('', false),
            array('non-empty value', true),
        );
    }

    /**
     * @dataProvider othProvider
     */
    public function testMedia(string $media, bool $is_valid) : void {
        $link_tag = new Core\Tags\LinkTag('stylesheet.css');

        $link_tag->setMedia($media);

        $regex = '';
        if (true === $is_valid) {
            $regex = "/^<link[^<>]* media=('$media'|\"$media\")[^<>]*\/>$/";
        }
        else {
            $regex = "/^<link((?!( media=)).)*\/>$/";
        }
        $this->assertMatchesRegularExpression($regex, $link_tag->make());
    }

    /**
     * @dataProvider othProvider
     */
    public function testHrefLang(string $hreflang, bool $is_valid) : void {
        $link_tag = new Core\Tags\LinkTag('stylesheet.css');

        $link_tag->setHrefLang($hreflang);

        $regex = '';
        if (true === $is_valid) {
            $regex = "/^<link[^<>]* hreflang=('$hreflang'|\"$hreflang\")[^<>]*\/>$/";
        }
        else {
            $regex = "/^<link((?!( hreflang=)).)*\/>$/";
        }
        $this->assertMatchesRegularExpression($regex, $link_tag->make());
    }

    /**
     * @dataProvider othProvider
     */
    public function testTitle(string $title, bool $is_valid) : void {
        $link_tag = new Core\Tags\LinkTag('stylesheet.css');

        $link_tag->setTitle($title);

        $regex = '';
        if (true === $is_valid) {
            $regex = "/^<link[^<>]* title=('$title'|\"$title\")[^<>]*\/>$/";
        }
        else {
            $regex = "/^<link((?!( title=)).)*\/>$/";
        }
        $this->assertMatchesRegularExpression($regex, $link_tag->make());
    }

    public function sizeProvider() : array {
        return array(
            array('any', true),
            array('', false),
            array('lol', false),
            array('50x50', true),
            array('50x', false),
        );
    }

    /**
     * @dataProvider sizeProvider
     */
    public function testSizes(string $size, bool $is_valid) : void {
        $link_tag = new Core\Tags\LinkTag('stylesheet.css');

        if (false === $is_valid) {
            $this->expectException(Exception::class);
            $this->expectExceptionMessage("Valeur de sizes invalid : $size");
        }

        $link_tag->setSizes($size);

        $regex = "/^<link[^<>]* sizes=('$size'|\"$size\")[^<>]*\/>$/";
        $this->assertMatchesRegularExpression($regex, $link_tag->make());
    } 
}