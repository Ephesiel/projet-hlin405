# Projet HLIN405

![pipeline_develop](https://gitlab.com/Ephesiel/projet-hlin405/badges/develop/pipeline.svg)
![coverage_develop](https://gitlab.com/Ephesiel/projet-hlin405/badges/develop/coverage.svg)

## Pour commencer

Ce projet n'est pas *complexe*, mais nous voulons *rester organisés* et garder un Git *propre*. C'est pour cela que je vous conseille d'aller lire les wikis suivants avant de travailler sur le projet :
- Les conventions d'écriture :
  - (S)CSS [à cette adresse](https://gitlab.com/Ephesiel/projet-hlin405/-/wikis/R%C3%A8gles-d'%C3%A9criture/(S)CSS)
  - JS [à cette adresse](https://gitlab.com/Ephesiel/projet-hlin405/-/wikis/R%C3%A8gles-d'%C3%A9criture/JS)
  - PHP [à cette adresse](https://gitlab.com/Ephesiel/projet-hlin405/-/wikis/R%C3%A8gles-d'%C3%A9criture/PHP)
- Les grandes lignes du projet :
  - Description du fonctionnement de l'architecture, [c'est ici](https://gitlab.com/Ephesiel/projet-hlin405/-/wikis/Projet/Fonctionnement-de-l'architecture-du-site)
  - La mise en place du fichier de configuration de l'environnement, [c'est là](https://gitlab.com/Ephesiel/projet-hlin405/-/wikis/Projet/Fichier-de-configuration-de-l'environnement)
  - Et de configuration des constantes, [c'est ici](https://gitlab.com/Ephesiel/projet-hlin405/-/wikis/Projet/Le-fichier-de-configuration)

Il est **fortement conseillé** de lire les 3 pages sur les conventions. En effet, vous n'êtes peut-être pas développeur sur ces 3 langages. Cependant, vous serez amené à écrire des unités de test (en PHP et Javascript), et à contrôler la qualité du code dans ces 3 langages.

Il est **nécessaire** de lire les 2 pages sur les grandes lignes du projet, et notamment de prendre connaissance du fonctionnement du fichier de configuration de l'environnement.

## Avant de clôner le projet, les dépendances

Avant de clôner le projet, de lancer la page principale, et de dire : « Ça ne marche pas ! », lisez attentivement cette partie.

Toutes les dépendances du projet vont être citées ci-dessous *(des dépendances seront rajoutées au fur et à mesure de l'avancée du projet, si quelque chose est cassé chez vous, veillez à revenir voir ce fichier)* :
- Dépendances PHP :
  - PHP8.0. Vous pouvez aller consulter la [page wiki consacrée](https://gitlab.com/Ephesiel/projet-hlin405/-/wikis/Guides/Installation-de-PHP-8) pour l'installer correctement.
  - PHP8.0-xml. C'est un module qui est activé de base dans `WAMP`, mais pas sous UNIX.
  - PHPUnit. Pareil que PHP8.0, il y a une [page wiki consacrée](https://gitlab.com/Ephesiel/projet-hlin405/-/wikis/Guides/Installation-de-PHPUnit-(--exemple)) à l'installation de ce module.
- Dépendances javascript :
  - Aucune pour le moment, mais un module d'unit-testing de javascript sera prochainement installé (probablement [jest](https://jestjs.io/))

## J'ai clôné, je peux travailler maintenant ?

Si vous avez bien consulté les pages wiki conseillées dans la section **Pour commencer**, il ne devrait pas y avoir de problème, allez-y. Cependant, si malgré la phrase qui vous dit que c'est **nécessaire** de les lire, vous ne l'avez pas fait, voici un résumé. Exécutez ces commandes dans votre dossier :
> `git update-index --skip-worktree src/env.cfg`
> `git update-index --skip-worktree src/config.php`

Le pourquoi du comment est bien sûr expliqué dans les wiki dédiés à ces fichiers.

## Liens utiles

### J'ai fini ma fonctionnalité, qu'est-ce que je fais ? 
Réponse [ici](https://gitlab.com/Ephesiel/projet-hlin405/-/wikis/Projet/Le-Code-Reviewing).
### J'ai du mal avec Git, un peu d'aide ne serait pas de refus... 
Consultez [cette page](https://gitlab.com/Ephesiel/projet-hlin405/-/wikis/Guides/Git-:-Commandes-de-base).