<?php
require_once __DIR__ . '/../core-tests-header.php';
use PHPUnit\Framework\TestCase;

final class TestMetaTag extends TestCase {
    public function testRender1() {
        $meta_charset_tag = new \Core\Tags\MetaCharsetTag('UTF-8');
        $this->expectOutputString($meta_charset_tag->make());
        $meta_charset_tag->render();
    }

    public function testRender2() {
        $meta_content_tag = new \Core\Tags\MetaContentTag('viewport', 'width=device-width, initial-scale=1');
        $this->expectOutputString($meta_content_tag->make());
        $meta_content_tag->render();
    }

    public function testRender3() {
        $meta_http_equiv_tag = new \Core\Tags\MetaHTTPEquivTag('Content-type', 'text/html; charset=UTF-8');
        $this->expectOutputString($meta_http_equiv_tag->make());
        $meta_http_equiv_tag->render();
    }

    public function sanitationProvider() : array {
        return array(
            array('utf-8'),
            array('"\'#€}?sd'),
            array('latin'),
            array('\\\\\\\\\\'),
            array('héhé'),
            array('blop\" attr=\"héhé')
        );
    }

    /**
     * @dataProvider sanitationProvider
     */
    public function testSanitation(string $arg) : void {
        $expected = htmlspecialchars($arg, ENT_QUOTES | ENT_HTML5);
        $expected = str_replace('\\', '&#92;', $expected);
        $this->assertEquals($expected, \Core\Tags\sanitize($arg));
    }

    public function charsetAttributesProvider() : array {
        return array(
            array('utf-8'),
            array('"\'#€}?sd'),
            array('latin'),
            array('\\\\\\\\\\'),
            array('héhé'),
            array('blop\" attr=\"héhé')
        );
    }

    /**
     * @dataProvider charsetAttributesProvider
     */
    public function testMetaCharsetTag(string $charset) : void {
        $meta_charset_tag = new Core\Tags\MetaCharsetTag($charset);
        
        $charset = \Core\Tags\sanitize($charset);
        $charset = preg_quote($charset);
        $tester = $meta_charset_tag->make();

        $regex = "/^<meta[^<>]* charset=(\"$charset\"|'$charset')[^<>]*>$/";
        $this->assertMatchesRegularExpression($regex, $tester);
    }

    public function metaContentAttributesProvider() : array {
        return array(
            array('keywords', 'HTML'),
            array('"\'#€}?sd', '"\'#€}?sd'),
            array('latin', 'latin'),
            array('blop\" attr=\"héhé', 'Mashalla !'),
        );
    }

    /**
     * @dataProvider metaContentAttributesProvider
     */
    public function testMetaContentTag(string $name, string $content) : void {
        $meta_content_tag = new Core\Tags\MetaContentTag($name, $content);
        $tester = $meta_content_tag->make();
        
        $name = \Core\Tags\sanitize($name);
        $name = preg_quote($name);

        $regex = "/^<meta[^<>]* name=(\"$name\"|'$name')[^<>]*>$/";
        $this->assertMatchesRegularExpression($regex, $tester);

        $content = \Core\Tags\sanitize($content);
        $content = preg_quote($content);

        $regex = "/^<meta[^<>]* content=(\"$content\"|'$content')[^<>]*>$/";
        $this->assertMatchesRegularExpression($regex, $tester);
    }

    public function metaHTTPEquivAttributesProvider() : array {
        return array(
            array('keywords', 'HTML'),
            array('"\'#€}?sd', '"\'#€}?sd'),
            array('latin', ''),
            array('blop\" attr=\"héhé', ''),
        );
    }

    /**
     * @dataProvider metaHTTPEquivAttributesProvider
     */
    public function testMetaHttpEquivTag(string $http_equiv, string $content) : void {
        $meta_http_equiv_tag = new Core\Tags\MetaHTTPEquivTag($http_equiv, $content);

        $tester = $meta_http_equiv_tag->make();

        $http_equiv = \Core\Tags\sanitize($http_equiv);
        $http_equiv = preg_quote($http_equiv);
        
        $regex = "/^<meta[^<>]* http-equiv=(\"$http_equiv\"|'$http_equiv')[^<>]*>$/";
        $this->assertMatchesRegularExpression($regex, $tester);

        if (!empty($content)) {
            $content = \Core\Tags\sanitize($content);
            $content = preg_quote($content);
    
            $regex = "/^<meta[^<>]* content=(\"$content\"|'$content')[^<>]*>$/";
            $this->assertMatchesRegularExpression($regex, $tester);
        }
        else {
            $regex = "/^<meta((?!( content=))[^>])*>$/";
            $this->assertMatchesRegularExpression($regex, $tester);
        }
    }

    public function equivCharsetAttributesProvider() : array {
        return array(
            array('utf-8', 'UTF-8', true),
            array('utf-8', 'UTF8', false),
            array('latin', 'LATIN', true),
        );
    }

    /**
     * @dataProvider equivCharsetAttributesProvider
     */
    public function testEquivCharsetAttribute(string $charset1, string $charset2, bool $equals) : void {
        $meta_charset_tag1 = new Core\Tags\MetaCharsetTag($charset1);
        $meta_charset_tag2 = new Core\Tags\MetaCharsetTag($charset2);
        $this->assertEquals($equals, $meta_charset_tag1->isEquivalent($meta_charset_tag2));
        $this->assertEquals($equals, $meta_charset_tag2->isEquivalent($meta_charset_tag1));
    }

    public function equivContentAttributesProvider() : array {
        return array(
            array('keywords', 'HTML', 'KEYWORDS', 'html', true),
            array('"\'#€}?sd', '"\'#€}?sd', '"\'#€}?sd', '"', true),
            array('latin', 'si', 'latin', 's', true),
            array('blop\" attr=\"héhé', 'Mashalla !', 'blop\"  attr=\"héhé', 'Mashalla !', false),
        );
    }

    /**
     * @dataProvider equivContentAttributesProvider
     */
    public function testEquivContentAttribute(string $name1, string $content1, string $name2, string $content2, bool $equals) : void {
        $meta_content_tag1 = new Core\Tags\MetaContentTag($name1, $content1);
        $meta_content_tag2 = new Core\Tags\MetaContentTag($name2, $content2);
        $this->assertEquals($equals, $meta_content_tag1->isEquivalent($meta_content_tag2));
        $this->assertEquals($equals, $meta_content_tag2->isEquivalent($meta_content_tag1));
    }

    public function equivHttpEquivAttributesProvider() : array {
        return array(
            array('keywords', 'HTML', 'KEYWORDS', 'html', true),
            array('"\'#€}?sd', '"\'#€}?sd', '"\'#€}?sd', '"\'#€}?sd', true),
            array('"\'#€}?sd', '"\'#€}?sd', '"\'#€}?sd', '', true),
            array('latin', '', 'latin', 's', true),
            array('blop\" attr=\"héhé', 'Mashalla !', 'blop\"  attr=\"héhé', 'Mashalla !', false),
        );
    }

    /**
     * @dataProvider equivHttpEquivAttributesProvider
     */
    public function testEquivHttpEquivAttribute(string $http_equiv1, string $content1, string $http_equiv2, string $content2, bool $equals) : void {
        $meta_http_equiv_tag1 = new Core\Tags\MetaHTTPEquivTag($http_equiv1, $content1);
        $meta_http_equiv_tag2 = new Core\Tags\MetaHTTPEquivTag($http_equiv2, $content2);
        $this->assertEquals($equals, $meta_http_equiv_tag1->isEquivalent($meta_http_equiv_tag2));
        $this->assertEquals($equals, $meta_http_equiv_tag2->isEquivalent($meta_http_equiv_tag1));
    }

    public function testExpectException1() : void {
        $this->expectException(Exception::class);
        new Core\Tags\MetaHTTPEquivTag('', '');
    }

    public function testExpectException2() : void {
        $this->expectException(Exception::class);
        new Core\Tags\MetaHTTPEquivTag('', 'test');
    }

    public function testExpectException3() : void {
        $this->expectException(Exception::class);
        new Core\Tags\MetaContentTag('', '');
    }

    public function testExpectException4() : void {
        $this->expectException(Exception::class);
        new Core\Tags\MetaContentTag('', 'test');
    }

    public function testExpectException5() : void {
        $this->expectException(Exception::class);
        new Core\Tags\MetaContentTag('test', '');
    }

    public function testExpectException6() : void {
        $this->expectException(Exception::class);
        new Core\Tags\MetaCharsetTag('');
    }
}