<?php
require_once __DIR__ . '/../site-tests-header.php';

final class TestUser extends MultipleTest {
    /**
     * @dataProvider multipleTests
     * @group longTests
     */
    public function testPlayerUser() : void {
        global $phdb;

        $user_id = ph_test_create_new_unique_value_for_table('user');
        $player_id = ph_test_create_new_unique_value_for_table('player');
        $user_email = ph_test_create_new_unique_value_for_table('user', 'email', 'string');

        $phdb->runInstallation(
            "INSERT INTO user VALUES($user_id, '$user_email', 'test', 'test', 'test-pp.png');
             INSERT INTO player VALUES($player_id, 'super description', '$user_id');
             INSERT INTO user_role VALUES($user_id, 4)"
        );

        $user1 = PH\User::createFromId($user_id);
        $user2 = PH\User::createFromEmail($user_email);
        
        $this->assertEquals($user1->getId(), $user_id);
        $this->assertEquals($user1->getEmail(), $user_email);
        $this->assertTrue($user1->getPermissions()->hasFlag(Role::Player));
        $this->assertTrue($user1->isPlayer());
        $this->assertFalse($user1->getPermissions()->hasAny(Role::Administrator | Role::Manager | Role::Public));
        $this->assertFalse($user1->isAdmin());
        $this->assertEquals($user1->getName(), 'test');
        $this->assertEquals($user1->getRole(), Role::toString(Role::Player));
        $this->assertEquals($user1->getProfilePicture(), ph_get_upload_link('test-pp.png'));
        $this->assertEquals($user1->getDescription(), 'super description');
        $this->assertEquals($user1->getPlayerId(), $player_id);
        
        $this->assertEquals($user2->getId(), $user1->getId());
        $this->assertEquals($user2->getEmail(), $user1->getEmail());
        $this->assertEquals($user2->isPlayer(), $user1->isPlayer());
        $this->assertEquals($user2->isAdmin(), $user1->isAdmin());
        $this->assertEquals($user2->getName(), $user1->getName());
        $this->assertEquals($user2->getRole(), $user1->getRole());
        $this->assertEquals($user2->getProfilePicture(), $user1->getProfilePicture());
        $this->assertEquals($user2->getDescription(), $user1->getDescription());
        $this->assertEquals($user2->getPlayerId(), $user1->getPlayerId());

        $this->assertTrue($user1->sameUserThan($user2));
        $this->assertTrue($user2->sameUserThan($user1));

        $phdb->runInstallation(
            "DELETE FROM user WHERE id = $user_id;
            DELETE FROM player WHERE id = $player_id;
            DELETE FROM user_role WHERE user_id = $user_id"
        );

        $phdb->resetCache();
    }
    
    /**
     * @dataProvider multipleTests
     * @group longTests
     */
    public function testAdminUser() : void {
        global $phdb;

        $user_id = ph_test_create_new_unique_value_for_table('user');
        $user_email = ph_test_create_new_unique_value_for_table('user', 'email', 'string');

        $phdb->runInstallation(
            "INSERT INTO user VALUES($user_id, '$user_email', 'test', 'test', null);
             INSERT INTO user_role VALUES($user_id, 1)"
        );
        
        $user = PH\User::createFromId($user_id);
        $this->assertFalse($user->isPlayer());
        $this->assertTrue($user->isAdmin());
        $this->assertEquals($user->getProfilePicture(), ph_get_upload_link(PH_USER_DEFAULT_PROFILE_PICTURE));

        try {
            $user->getDescription();
            $this->fail('Un utilisateur non joueur ne devrait pas avoir de description');
        }
        catch (Exception $_) {}

        try {
            $user->getPlayerId();
            $this->fail('Un utilisateur non joueur ne devrait pas avoir d\'identifiant joueur');
        }
        catch (Exception $_) {}

        $phdb->runInstallation(
            "DELETE FROM user WHERE id = $user_id;
             DELETE FROM user_role WHERE user_id = $user_id"
        );

        $phdb->resetCache();
    }
    
    /**
     * @dataProvider multipleTests
     * @group longTests
     */
    public function testExceptionNoRole() : void {
        global $phdb;

        $user_id = ph_test_create_new_unique_value_for_table('user');
        $user_email = ph_test_create_new_unique_value_for_table('user', 'email', 'string');

        $phdb->runInstallation("INSERT INTO user VALUES($user_id, '$user_email', 'test', 'test', null);");

        try {
            PH\User::createFromId($user_id);
            $this->fail('Un utilisateur sans rôle ne devrait pas pouvoir être créé');
        }
        catch (Exception $_) {}

        try {
            PH\User::createFromEmail($user_email);
            $this->fail('Un utilisateur sans rôle ne devrait pas pouvoir être créé');
        }
        catch (Exception $_) {}

        $phdb->runInstallation("DELETE FROM user WHERE id = $user_id;");
        $this->assertTrue(true);

        $phdb->resetCache();
    }
    
    /**
     * @dataProvider multipleTests
     * @group longTests
     */
    public function testExceptionNoId() : void {
        $user_id = ph_test_create_new_unique_value_for_table('user');

        $this->expectException(Exception::class);
        PH\User::createFromId($user_id);
    }
    
    /**
     * @dataProvider multipleTests
     * @group longTests
     */
    public function testExceptionNoEmail() : void {
        $user_email = ph_test_create_new_unique_value_for_table('user', 'email', 'string');

        $this->expectException(Exception::class);
        PH\User::createFromEmail($user_email);
    }

    public function testPublicUser() : void {
        if (isset($_SESSION['ph_user'])) {
            unset($_SESSION['ph_user']);
        }

        $this->assertTrue(ph_get_user()->sameUserThan(PH\User::createPublicUser()));
        
        ph_connect_user(ADMIN_EMAIL);
        $this->assertFalse(ph_get_user()->sameUserThan(PH\User::createPublicUser()));
        
        ph_disconnect_user();
        $this->assertFalse(isset($_SESSION[PH_USER_SESSION_KEY]));
    }

    public function testMatch() : void {
        $this->assertTrue(ph_exists_matching_user(ADMIN_EMAIL, ADMIN_PASSWD));
        $this->assertFalse(ph_exists_matching_user(ADMIN_EMAIL, ADMIN_EMAIL));
        $this->assertFalse(ph_exists_matching_user('', ''));
    }

    public function testAllUsers() : void {
        $users = ph_get_all_users();

        // Il y a toujours au moins l'administrateur qui est créé. 
        $this->assertFalse(empty($users));
        foreach ($users as $user) {
            $this->assertTrue($user instanceof PH\User);
        }
    }

    public function testAllPlayers() : void {
        $users = ph_get_all_players();

        foreach ($users as $user) {
            $this->assertTrue($user instanceof PH\User);
            $this->assertTrue($user->isPlayer());
        }
    }
}