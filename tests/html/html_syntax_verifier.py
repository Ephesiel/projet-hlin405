#! /usr/bin/env python3

from typing import List, NoReturn, Tuple, Union
from enum import Enum
import sys, os, re

class ExitCode(Enum) :
    OK = 0
    INCORRECT_ARGUMENTS_ERROR = 1

class SyntaxErrors(Enum) :
    INCORRECT_INDENTATION = 0
    TAG_DOES_NOT_MATCH = 1
    MULTIPLE_TAGS_ON_SAME_LINE = 2

class ArgumentsChecker :
    def __init__(self, arguments: List[str]) :
        self.arguments = arguments

    def canLocateAndReadAllArguments(self) -> bool :
        allArgumentsAreLocated, i = True, 0
        while allArgumentsAreLocated and i < len(self.arguments) :
            currentArgument = self.arguments[i]
            if not (self.argumentIsFile(currentArgument) and self.argumentIsReadable(currentArgument)) :
                allArgumentsAreLocated = False
            i = i + 1
        return allArgumentsAreLocated

    def allArgumentsAreHtmlFiles(self) -> bool :
        allArgumentsAreHtml, i = True, 0
        while allArgumentsAreHtml and i < len(self.arguments) :
            currentArgument = self.arguments[i]
            if not self.hasHtmlExtension(currentArgument) :
                allArgumentsAreHtml = False
            i = i + 1
        return allArgumentsAreHtml

    def argumentIsFile(self, argument: str) -> bool :
        return os.path.isfile(argument)

    def argumentIsReadable(self, argument: str) -> bool :
        return os.access(argument, os.R_OK)

    def hasHtmlExtension(self, argument: str) -> bool :
        return os.path.splitext(argument)[1] == ".html"

class HtmlSyntaxChecker :
    def __init__(self, fileName: str) :
        self.htmlFileDescriptor = open(fileName, mode="r", encoding="utf-8")

    def __del__(self) :
        self.htmlFileDescriptor.close()

    def checkForIndentationAndTagsErrors(self) -> NoReturn :
        self.tagStack, self.currentWhiteSpaces, self.nextExpectedWhiteSpaces, self.errors, self.currentLine = [], 0, 0, {}, 1
        for line in self.htmlFileDescriptor.readlines() :
            self.checkForIndentationError(line)
            self.addErrorIfMultipleTagsOn(line)
            self.addTagInStackIfExists(line)
            self.updateNextExpectedWhiteSpaces(line)
            self.currentLine += 1
        self.checkTagsValidity()

    def prettyPrintErrors(self) -> NoReturn :
        syntaxEnumToString = {
            SyntaxErrors.INCORRECT_INDENTATION: "Indentation incorrecte !",
            SyntaxErrors.TAG_DOES_NOT_MATCH: "La balise n'a pas été ouverte/fermée !",
            SyntaxErrors.MULTIPLE_TAGS_ON_SAME_LINE: "Il y a plusieurs balises sur cette ligne !"
        }

        if len(self.errors) > 0 :
            for errorLine, ls in self.errors.items() :
                print(f"L{errorLine}\t", "\n\t".join([syntaxEnumToString[x] for x in ls]))
        else :
            print(f"Aucune erreur n'a été trouvée !")
            
    def checkForIndentationError(self, line: str) -> NoReturn :
        if not self.indentationIsAsExpected(line) :
            self.addErrorOnCurrentLineOfType(SyntaxErrors.INCORRECT_INDENTATION)

    def addErrorIfMultipleTagsOn(self, line: str) -> NoReturn :
        if self.hasMultipleOpeningTags(line) or self.hasMultipleEndingTags(line) :
            self.addErrorOnCurrentLineOfType(SyntaxErrors.MULTIPLE_TAGS_ON_SAME_LINE)

    def addTagInStackIfExists(self, line: str) -> NoReturn :
        if self.hasTag(line) :
            if (self.isSectionInline(line)) :
                openingTag, endingTag = self.extractInlineTags(line)
                self.addTagInStack(openingTag)
                self.addTagInStack(endingTag)
            else :
                self.addTagInStack(self.extractTagFrom(line))

    def addErrorOnCurrentLineOfType(self, errorType: SyntaxErrors) -> NoReturn :
        self.addErrorOnLineOfType(self.currentLine, errorType)

    def addErrorOnLineOfType(self, line: int, errorType: SyntaxErrors) -> NoReturn :
        if line in self.errors :
            self.errors[line].append(errorType)
        else :
            self.errors[line] = [errorType]

    def checkTagsValidity(self) -> NoReturn :
        validityTagStack = []
        for tag, line in self.tagStack :
            if len(validityTagStack) > 0 and validityTagStack[-1][0] == tag[1:] :
                validityTagStack = validityTagStack[:-1]
            elif not self.isSolitaryTag(tag) :
                validityTagStack.append((tag, line))
        self.addInvalidLinesInErrors(validityTagStack)

    def addInvalidLinesInErrors(self, validityTagStack: List[Tuple[str, int]]) -> NoReturn :
        validityTagStack = self.removeValidParents(validityTagStack)
        for _, line in validityTagStack :
            self.addErrorOnLineOfType(line, SyntaxErrors.TAG_DOES_NOT_MATCH)

    def removeValidParents(self, validityTagStack: List[Tuple[str, int]]) -> List[Tuple[str, int]] :
        i = 0
        while i < len(validityTagStack) :
            iTag = validityTagStack[i]
            jTag = self.rfindClosingTag(iTag, validityTagStack, i)
            if jTag != None :
                validityTagStack.remove(iTag)
                validityTagStack.remove(jTag)
            else :
                i += 1
        return validityTagStack

    def indentationIsAsExpected(self, line: str) -> bool :
        self.currentWhiteSpaces = line.count(' ', 0, len(line) - len(line.lstrip()))

        # On a malheureusement besoin de ces 3 lignes, car nextExpectedWhiteSpaces est mis à jour après l'appel de cette fonction,
        # ce qui signifie que si le tag est fermant, une erreur d'indentation sera affichée alors que c'est correct.
        expectedWhiteSpaces = self.nextExpectedWhiteSpaces
        if self.hasEndingTag(line) and not(self.isSectionInline(line)):
            expectedWhiteSpaces -= 4

        return self.currentWhiteSpaces == expectedWhiteSpaces

    def hasTag(self, line: str) -> bool :
        return self.hasOpeningTag(line) or self.hasEndingTag(line)

    def isSectionInline(self, line: str) -> bool :
        return self.hasOpeningTag(line) and self.hasEndingTag(line)

    def extractInlineTags(self, line: str) -> Tuple[str, str] :
        match = re.match(r"^ *<([^>^ ]+)[^>]*>[^<]*<(\/[^>]+)>", line)
        return match.groups()

    def extractTagFrom(self, line: str) -> str :
        match = re.match(r"^ *<(\/?[^>^ ]+)[^>]*>", line)
        return match.group(1)

    def addTagInStack(self, tag: str) -> NoReturn :
        self.tagStack.append((tag, self.currentLine))

    def updateNextExpectedWhiteSpaces(self, line: str) -> NoReturn :
        if self.hasOpeningTag(line) and not self.isSolitaryTag(self.extractTagFrom(line)) :
            self.nextExpectedWhiteSpaces += 4
        if self.hasEndingTag(line) :
            self.nextExpectedWhiteSpaces -= 4

    def hasMultipleOpeningTags(self, line: str) -> bool :
        match = re.findall(r" *<[^>^\/^\!]+>", line)
        return len(match) > 1

    def hasMultipleEndingTags(self, line: str) -> bool :
        match = re.findall(r" *<\/[^>]+>", line)
        return len(match) > 1

    def hasOpeningTag(self, line: str) -> bool :
        match = re.match(r" *<[^>^\/^\!]+>", line)
        return match != None

    def hasEndingTag(self, line: str) -> bool :
        match = re.match(r".*<\/[^>]+>", line)
        return match != None

    def isSolitaryTag(self, tag: str) -> bool :
        return tag in ["area", "base", "br", "col", "colgroup", "command", "embed", "hr", "img", "input", "keygen", "link", "meta", "param", "source", "track", "wbr"]

    def rfindClosingTag(self, iTag: Tuple[str, int], validityTagStack: List[Tuple[str, int]], startIndex: int) -> Union[Tuple[str, int], None] :
        found, i = None, len(validityTagStack) - 1
        while found == None and i > startIndex :
            tag = validityTagStack[i]
            if tag[0][1:] == iTag[0] :
                found = tag
            i -= 1
        return found

if __name__ == "__main__" :
    argumentsChecker = ArgumentsChecker(sys.argv[1:])

    if not(argumentsChecker.canLocateAndReadAllArguments() and argumentsChecker.allArgumentsAreHtmlFiles()) :
        print(f"Erreur : un des argument passé au programme n'est pas localisable / lisible / un document html.", file=sys.stderr)
        exit(ExitCode.INCORRECT_ARGUMENTS_ERROR)

    for htmlFile in sys.argv[1:] :
        print(f"Fichier actuel : {htmlFile}")
        htmlSyntaxChecker = HtmlSyntaxChecker(htmlFile)
        htmlSyntaxChecker.checkForIndentationAndTagsErrors()
        htmlSyntaxChecker.prettyPrintErrors()
        print('\n', end='')

    exit(ExitCode.OK)