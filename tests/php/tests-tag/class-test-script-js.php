<?php
require_once __DIR__ . '/../core-tests-header.php';
use PHPUnit\Framework\TestCase;

final class TestScriptJS extends TestCase {
    public function testScriptJS() : void {
        $script_tag = new Core\Tags\ScriptJS('moi avoir mal à la tête');
        $tester = $script_tag->make();
        $regex = "/^<script[^<>]* type='application\/javascript'[^<>]*>[^<>]*<\/script>$/";
        $this->assertMatchesRegularExpression($regex, $tester);
    }
}