<?php
require_once __DIR__ . '/../core-tests-header.php';
use PHPUnit\Framework\TestCase;

final class TestScriptTag extends TestCase {

    public function testEmptyTag() : void {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage("La source d'un script ne peut pas être nul ou vide.");
        $script_tag = new Core\Tags\ScriptTag(''); 
    }

    public function testScriptTag() : void {
        $script_tag1 = new Core\Tags\ScriptTag('c\'est le test');
        $this->assertSame($script_tag1->make(), '<script src=\'c\'est le test\'></script>');

        $this->expectOutputString('<script src=\'c\'est le test\'></script>');
        $script_tag1->render();

        $script_tag2 = new Core\Tags\ScriptTag("c'est le test");
        $this->assertSame($script_tag2->isEquivalent($script_tag1), true);
        $script_tag3 = new Core\Tags\ScriptTag('n\'importe quoi');
        $this->assertSame($script_tag2->isEquivalent($script_tag3), false);
    }

    public function testSetAttributesBool() : void {
        $script_tag = new Core\Tags\ScriptTag('juste pour le test');

        $script_tag->setAsyncMode(true);
        $script_tag->setDeferMode(true);
        $script_tag->setNoModuleMode(true);

        $tester = $script_tag->make();
        $regex = "/^<script[^<>]* async[^<>]*>[^<>]*<\/script>$/";
        $this->assertMatchesRegularExpression($regex, $tester);
        $regex = "/^<script[^<>]* defer[^<>]*>[^<>]*<\/script>$/";
        $this->assertMatchesRegularExpression($regex, $tester);
        $regex = "/^<script[^<>]* nomodule[^<>]*>[^<>]*<\/script>$/";
        $this->assertMatchesRegularExpression($regex, $tester);
        
        $script_tag->setAsyncMode(false);
        $script_tag->setDeferMode(false);
        $script_tag->setNoModuleMode(false);
        
        $tester = $script_tag->make();
        $regex = "/^<script((?!( async)).)*>[^<>]*<\/script>$/";
        $this->assertMatchesRegularExpression($regex, $tester);
        $regex = "/^<script((?!( defer)).)*>[^<>]*<\/script>$/";
        $this->assertMatchesRegularExpression($regex, $tester);
        $regex = "/^<script((?!( nomodule)).)*>[^<>]*<\/script>$/";
        $this->assertMatchesRegularExpression($regex, $tester);
    }

    public function attributesProvider() : array {
        return array(
            array('je ne sais pà', 'sa fé mal', 'no-referrer', 'anonymous'),
            array('truc du genre 85èçè-', 'tu trouve ?', 'origin', 'use-credentials'),
            array('eh ça fait &é":', 'Oh ! tu fait quoi là !!!!', 'salut mon gars', 'use-credentials'),
            array('j\'ai pas de chance', 'oupsi', 'unsafe-url', 'screpeu'),
            array('tu crois""\'\'"""\'?"', 't\'es "BG" toi tu sais !', 'cfbcbhnfvbnvgj,vg', 'shhhhhh'),
        );
    }

    /**
     * @dataProvider attributesProvider
     */
    public function testSetAttributesString(string $type, string $integrity, string $referrer_policy, string $cross_origin) : void {
        
        $script_tag = new Core\Tags\ScriptTag('juste pour le test');

        $valid_cross_origin = Core\Tags\is_cross_origin($cross_origin);
        $valid_referrer_policy = Core\Tags\is_referrer_policy($referrer_policy);

        if (false === $valid_cross_origin || false === $valid_referrer_policy) {
            $this->expectException(Exception::class);
        }
        $script_tag->setCrossOrigin($cross_origin);
        $script_tag->setReferrerPolicy($referrer_policy);
        $script_tag->setType($type);
        $script_tag->setIntegrity($integrity);
        
        $tester = $script_tag->make();

        $cross_origin = preg_quote($cross_origin);
        $referrer_policy = preg_quote($referrer_policy);
        $type = preg_quote($type);
        $integrity = preg_quote($integrity);

        if (true === $valid_cross_origin) {
            $regex = "/^<script[^<>]* crossorigin=('$cross_origin'|\"$cross_origin\")[^<>]*>[^<>]*<\/script>$/";
            $this->assertMatchesRegularExpression($regex, $tester);
        }
        else {
            $regex = "/^<script((?!( crossorigin=)).)*>[^<>]*<\/script>$/";
            $this->assertMatchesRegularExpression($regex, $tester);
        }

        if (true === $valid_referrer_policy) {
            $regex = "/^<script[^<>]* referrerpolicy=('$referrer_policy'|\"$referrer_policy\")[^<>]*>[^<>]*<\/script>$/";
            $this->assertMatchesRegularExpression($regex, $tester);
        }
        else {
            $regex = "/^<script((?!( referrerpolicy=)).)*>[^<>]*<\/script>$/";
            $this->assertMatchesRegularExpression($regex, $tester);
        }
        
        $regex = "/^<script[^<>]* type=('$type'|\"$type\")[^<>]*>[^<>]*<\/script>$/";
        $this->assertMatchesRegularExpression($regex, $tester);
        $regex = "/^<script[^<>]* integrity=('$integrity'|\"$integrity\")[^<>]*>[^<>]*<\/script>$/";
        $this->assertMatchesRegularExpression($regex, $tester);
    }

    /**
     * @dataProvider attributesProvider
     */
    public function testRender(string $type, string $integrity, string $referrer_policy, string $cross_origin) : void {
        $script_tag = new Core\Tags\ScriptTag('index.js');
        $valid_cross_origin = Core\Tags\is_cross_origin($cross_origin);
        $valid_referrer_policy = Core\Tags\is_referrer_policy($referrer_policy);

        if (false === $valid_cross_origin || false === $valid_referrer_policy) {
            $this->expectException(Exception::class);
        }
        $script_tag->setCrossOrigin($cross_origin);
        $script_tag->setReferrerPolicy($referrer_policy);
        $script_tag->setType($type);
        $script_tag->setIntegrity($integrity);

        $cross_origin = preg_quote($cross_origin);
        $referrer_policy = preg_quote($referrer_policy);
        $type = preg_quote($type);
        $integrity = preg_quote($integrity);

        $possibilities = '(type|integrity';
        $attributes = "('$type'|\"$type\"|'$integrity'|\"$integrity\"";
        if (true === $valid_cross_origin) {
            $possibilities .= '|crossorigin';
            $attributes .= "|'$cross_origin'|\"$cross_origin\"";
        }
        if (true === $valid_referrer_policy) {
            $possibilities .= '|referrerpolicy';
            $attributes .= "|'$referrer_policy'|\"$referrer_policy\"";
        }
        $possibilities .= ')=' . $attributes . ')';

        $regex = "/^<script[^<>]* $possibilities $possibilities";
        if (true === $valid_referrer_policy) {
            $regex .= " $possibilities";
        }
        if (true === $valid_cross_origin) {
            $regex .= " $possibilities";
        }
        $regex .= '[^<>]*>[^<>]*<\/script>$/';

        $this->expectOutputRegex($regex);
        $script_tag->render();
    }
}