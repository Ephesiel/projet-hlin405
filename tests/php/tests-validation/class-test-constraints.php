<?php
require_once __DIR__ . '/../core-tests-header.php';
use PHPUnit\Framework\TestCase;

final class TestConstraints extends TestCase {
    public function collectionProvider() : array {
        $test = new Core\Validation\Constraint\IsString();

        return array(
            // Tests sans valeur
            array('', array(), false),
            array(0, array(), false),
            array(10, array(), false),
            array(array(), array(), true),
            array(array('value'), array(), true),
            array(array('field' => 'value'), array(), true),
            array(array(array()), array(), true),
            // Tests avec une contrainte
            array(array('field' => 'value'), array('field' => $test), true),
            array(array('field'), array('field' => $test), false),
            array('', array('field' => $test), false),
            array(array('field' => 'value', 'field'), array('field' => $test), true),
            // Tests avec deux contraintes
            array(
                array('field1' => 'value', 'field2' => 'value'),
                array('field1' => $test, 'field2' => $test),
                true
            ),
            array(
                array('field2' => 'value', 'field1' => 'value'),
                array('field1' => $test, 'field2' => $test),
                true
            ),
            array(
                array('field1' => 'value'),
                array('field1' => $test, 'field2' => $test),
                false
            ),
        );
    }

    /**
     * @dataProvider collectionProvider
     */
    public function testCollection(mixed $value, array $fields, bool $result) : void {
        $constraint_collection = new Core\Validation\Constraint\Collection($fields);
        $r = $constraint_collection->assert('', $value);
        $this->assertEquals($result, $r->isValid());
    }

    public function collectionOptionnalProvider() : array {
        $test = new Core\Validation\Constraint\IsString();

        return array(
            array(
                array('field1' => 'value', 'field2' => 'value'),
                array('field1' => $test, 'field2' => $test),
                array(),
                true   
            ),
            array(
                array('field1' => 'value', 'field2' => 'value'),
                array('field1' => $test, 'field2' => $test),
                array('field1'),
                true   
            ),
            array(
                array('field1' => 'value', 'field2' => 'value'),
                array('field1' => $test, 'field2' => $test),
                array('field1', 'field2'),
                true   
            ),
            array(
                array(),
                array('field1' => $test, 'field2' => $test),
                array('field1', 'field2'),
                true   
            ),
            array(
                array('field1' => 'value'),
                array('field1' => $test, 'field2' => $test),
                array('field1', 'field2'),
                true   
            ),
            array(
                array('field1' => 'value'),
                array('field1' => $test, 'field2' => $test),
                array('field2'),
                true   
            ),
            array(
                array(),
                array('field1' => $test, 'field2' => $test),
                array('field1'),
                false   
            )
        );
    }

    /**
     * @dataProvider collectionOptionnalProvider
     */
    public function testOptionnalCollection(mixed $value, array $fields, array $optionnal, bool $result) : void {
        $constraint_collection = new Core\Validation\Constraint\Collection($fields, $optionnal);
        $r = $constraint_collection->assert('', $value);
        $this->assertEquals($result, $r->isValid());
    }

    public function collectionExceptionProvider() : array {
        $test = new Core\Validation\Constraint\IsString();
        return array(
            array(array(
                'test' => $test,
                'test2' => 's'
            )),
            array(array(
                'test2' => 0,
                'test' => $test
            )),
            array(array(
                'test2' => array()
            )),
            array(array(
                'test' => new StdClass()
            )),
        );
    }

    /**
     * @dataProvider collectionExceptionProvider
     */
    public function testCollectionException(array $fields) : void {
        $this->expectException(InvalidArgumentException::class);
        $constraint_collection = new Core\Validation\Constraint\Collection($fields);
    }

    public function multipleProvider() : array {
        $mail = new Core\Validation\Constraint\Email();
        $min10 = new Core\Validation\Constraint\Length(array('min' => 10));
        return array(
            array('', array($mail, $min10), false),
            array('t@t.tse', array($mail, $min10), false),
            array('t@t.tse', array($mail), true),
            array('teststssttse', array($mail, $min10), false),
            array('teststssttse', array($min10), true),
            array(0, array($mail), false),
        );
    }

    /**
     * @dataProvider multipleProvider
     */
    public function testMultiple(mixed $value, array $constraints, bool $result) : void {
        $constraint_multiple = new Core\Validation\Constraint\Multiple($constraints);
        $r = $constraint_multiple->assert('', $value);
        $this->assertEquals($result, $r->isValid());
    }

    public function multipleExceptionProvider() : array {
        $string = new Core\Validation\Constraint\IsString();
        $collection = new Core\Validation\Constraint\Collection(array());
        return array(
            array(array(
                'test' => $string,
                'test2' => 's'
            )),
            array(array(
                'test2' => 0,
                'test' => $string
            )),
            array(array(
                'test2' => array()
            )),
            array(array(
                'test' => new StdClass()
            )),
            array(array(
                'test' => $collection
            )),
            array(array(
                'test' => $collection,
                'test2' => $string
            )),
        );
    }

    /**
     * @dataProvider multipleExceptionProvider
     */
    public function testMultipleException(array $constraints) : void {
        $this->expectException(InvalidArgumentException::class);
        $constraint_multiple = new Core\Validation\Constraint\Multiple($constraints);
    }

    public function choiceProvider() : array {
        $mail = new Core\Validation\Constraint\Email();
        $min10 = new Core\Validation\Constraint\Length(array('min' => 10));
        return array(
            array('', array(), false),
            array('', array(''), true),
            array(0, array(''), false),
            array('a', array('a', 'b', 'c'), true),
            array('b', array('a', 'b', 'c'), true),
            array('c', array('a', 'b', 'c'), true),
            array('1', array('1', '2', 3), true),
            array('2', array('1', '2', 3), true),
            array('3', array('1', '2', 3), false),
            array(1, array('1', '2', 3), false),
            array(2, array('1', '2', 3), false),
            array(3, array('1', '2', 3), true),
            array(3, array('no-mail', $mail), false),
            array('email@toto.com', array('no-mail', $mail), true),
            array('no-mail', array('no-mail', $mail), true),
            array('no-mails', array('no-mail', $mail), false),
            array('', array('no-mail', $mail), false),
            array('', array($mail, $min10), false),
            array('t@t.tse', array($mail, $min10), true),
            array('teststssttse', array($mail, $min10), true),
            array('teststssttse', array($min10), true),
        );
    }

    /**
     * @dataProvider choiceProvider
     */
    public function testChoice(mixed $value, array $constraints, bool $result) : void {
        $constraint_choice = new Core\Validation\Constraint\Choice($constraints);
        $r = $constraint_choice->assert('', $value);
        $this->assertEquals($result, $r->isValid());
    }

    public function isStringProvider() : array {
        return array(
            array('', true),
            array('string', true),
            array('"{"[-', true),
            array(0, false),
            array(array(), false)
        );
    }

    /**
     * @dataProvider isStringProvider
     */
    public function testIsString(mixed $value, bool $result) : void {
        $constraint_is_string = new Core\Validation\Constraint\IsString();
        $r = $constraint_is_string->assert('', $value);
        $this->assertEquals($result, $r->isValid());
    }

    public function isNumberProvider() : array {
        return array(
            array('', false),
            array('string', false),
            array('"{"[-', false),
            array(0, true),
            array(array(), false),
            array(0b0101, true),
            array(02564, true),
            array(0x0fde, true),
            array(10, true),
            array('12', true),
            array('02564', true),
            array('0b0101', false),
            array('0x0fde', false)
        );
    }

    /**
     * @dataProvider isNumberProvider
     */
    public function testIsNumber(mixed $value, bool $result) : void {
        $constraint_is_number = new Core\Validation\Constraint\IsNumber();
        $r = $constraint_is_number->assert('', $value);
        $this->assertEquals($result, $r->isValid());
    }

    public function regexProvider() : array {
        return array(
            array('ssssssss', '/ssss/', true),
            array('azzzR', '/[a-z][A-Z]/', true),
            array('azzz', '/[a-z][A-Z]/', false),
            array('Ra', '/[a-z][A-Z]/', false),
            array('RaR', '/[a-z][A-Z]/', true),
            array('RaR', '/^[a-z][A-Z]$ /', false)
        );
    }

    /**
     * @dataProvider regexProvider
     */
    public function testRegex(mixed $value, string $regex, bool $result) : void {
        $constraint_regex = new Core\Validation\Constraint\Regex($regex);
        $r = $constraint_regex->assert('', $value);
        $this->assertEquals($result, $r->isValid());
    }

    public function regexExceptionProvider() : array {
        return array(
            array('ssss'),
            array('[a-z][A-Z]'),
            array('/[a-z]/[A-Z]'),
            array('\/[a-z][A-Z]/')
        );
    }

    /**
     * @dataProvider regexExceptionProvider
     */
    public function testRegexException(string $regex) : void {
        $this->expectException(InvalidArgumentException::class);
        new Core\Validation\Constraint\Regex($regex);
    }

    public function emailProvider() : array {
        return array(
            array('email@toto.com', true),
            array('email@toto', false),
            array(array('email@toto.com'), false),
            array('emailtoto.com', false),
            array(0, false),
            array(10, false),
            array('email.truc@toto.com', true),
            array('"#~}ss"@toto.com', false),
            array('☺@toto.com', false)
        );
    }

    /**
     * @dataProvider emailProvider
     */
    public function testEmail(mixed $email, bool $result) : void {
        $constraint_email = new Core\Validation\Constraint\Email;
        $r = $constraint_email->assert('', $email);
        $this->assertEquals($result, $r->isValid());
    }

    public function lengthProvider() : array {
        return array(
            // Tests sans contrainte
            array('', array(), true),
            array('TEST', array(), true),
            array('toto#"€{}+?', array(), true),
            array('0', array(), true),
            array(0, array(), false),
            array(10, array(), false),
            array(array(), array(), false),
            // Tests avec min
            array('', array('min' => 0), true),
            array(0, array('min' => 0), false),
            array('', array('min' => 3), false),
            array('de', array('min' => 3), false),
            array('☺☺', array('min' => 3), true),
            array('tro', array('min' => 3), true),
            array("troooooooooooooooo\
                ooooooooooooooooooooo\
                ooooooooooooooooooooo\
                ooooooooooooooooooooo\
                oooooooooo", array('min' => 3), true),
            array('☺☺☺', array('min' => 3), true),
            array('', array('min' => -1), true),
            // Tests avec max
            array('', array('max' => 0), true),
            array(0, array('max' => 0), false),
            array('', array('max' => 3), true),
            array('de', array('max' => 3), true),
            array('☺☺', array('max' => 3), false),
            array('tro', array('max' => 3), true),
            array('☺☺☺', array('max' => 3), false),
            array('caca', array('max' => 3), false),
            array('☺☺☺☺', array('max' => 3), false),
            array('', array('max' => -1), false),
            // Tests avec equals
            array('', array('equals' => 0), true),
            array(0, array('equals' => 0), false),
            array('', array('equals' => 3), false),
            array('de', array('equals' => 3), false),
            array('☺☺', array('equals' => 3), false),
            array('tro', array('equals' => 3), true),
            array('☺☺☺', array('equals' => 3), false),
            array('caca', array('equals' => 3), false),
            array('☺☺☺☺', array('equals' => 3), false),
            array('', array('equals' => -1), false),
            // Tests multiples normaux
            array('', array('min' => 1, 'max' => 3), false),
            array(0, array('min' => 1, 'max' => 3), false),
            array('de', array('min' => 1, 'max' => 3), true),
            array('d', array('min' => 1, 'max' => 3), true),
            array('☺☺☺', array('min' => 1, 'max' => 3), false),
            array('☺', array('min' => 1, 'max' => 3), true),
            array('trop', array('min' => 1, 'max' => 3), false),
            // Tests multiples étranges
            array('', array('min' => 3, 'max' => 1), false),
            array(0, array('min' => 3, 'max' => 1), false),
            array('de', array('min' => 3, 'max' => 1), false),
            array('d', array('min' => 3, 'max' => 1), false),
            array('☺☺☺', array('min' => 3, 'max' => 1), false),
            array('☺', array('min' => 3, 'max' => 1), false),
            array('trop', array('min' => 3, 'max' => 1), false),
            array('', array('min' => -1, 'max' => -1), false),
            array('q', array('min' => 1, 'equals' => 2), false),
            array('qu', array('min' => 1, 'equals' => 2), true),
            array('q', array('max' => 1, 'equals' => 2), false),
            array('qu', array('max' => 1, 'equals' => 2), false),
            array('que', array('min' => 3, 'equals' => 2), false),
            array('qu', array('min' => 3, 'equals' => 2), false),
            array('que', array('max' => 3, 'equals' => 2), false),
            array('qu', array('max' => 3, 'equals' => 2), true),
            array('', array('min' => -1, 'equals' => -1), false),
            // Tests valeurs inexistentes
            array('', array('nan'), true),
            array('TEST', array('nan'), true),
            array('toto#"€{}+?', array('nan'), true),
            array('0', array('nan'), true),
            array(0, array('nan'), false),
            array(10, array('nan'), false),
            array(array(), array('nan'), false)
        );
    }

    /**
     * @dataProvider lengthProvider
     */
    public function testLength(mixed $value, array $constraints, bool $result) : void {
        $constraint_length = new Core\Validation\Constraint\Length($constraints);
        $r = $constraint_length->assert('', $value);
        $this->assertEquals($result, $r->isValid());
    }

    public function fileProvider() : array {
        return array(
            array(0, array(), false),
            array('', array(), false),
            array(__FILE__, array(), true),
            array(__DIR__, array(), false),
            array(__DIR__ . '/files/.haha', array(), true),
            array(__DIR__ . '/files/test.png', array(), true),
            array(__DIR__ . '/files/test.zip', array(), true),
            array(__DIR__ . '/files/test.txt', array(), true),
            array(__DIR__ . '/files/big.txt', array(), true),
            array(__DIR__ . '/files', array(), false),
            array(__DIR__ . '/files/', array(), false),
            array(__DIR__ . '/files/test.png', array('max_size' => '30'), false),
            array(__DIR__ . '/files/test.txt', array('max_size' => '30'), false),
            array(__DIR__ . '/files/big.txt', array('max_size' => '30'), false),
            array(__DIR__ . '/files/test.png', array('max_size' => '60'), false),
            array(__DIR__ . '/files/test.txt', array('max_size' => '60'), true),
            array(__DIR__ . '/files/big.txt', array('max_size' => '60'), false),
            array(__DIR__ . '/files/test.png', array('max_size' => '1 Ko'), true),
            array(__DIR__ . '/files/test.txt', array('max_size' => '1 Ko'), true),
            array(__DIR__ . '/files/big.txt', array('max_size' => '1 Ko'), false),
            array(__DIR__ . '/files/test.txt', array('max_size' => '1 Mo'), true),
            array(__DIR__ . '/files/big.txt', array('max_size' => '1 Mo'), true),
            array(__DIR__ . '/files/big.txt', array('max_size' => '1 Kio'), false),
            array(__DIR__ . '/files/big.txt', array('max_size' => '1 K'), false),
            array(__DIR__ . '/files/big.txt', array('max_size' => '1 k'), false),
            array(__DIR__ . '/files/test.txt', array('types' => array('image/png')), false),
            array(__DIR__ . '/files/test.png', array('types' => array('image/png')), true),
            array(__DIR__ . '/files/test.png', array('types' => array('image/*')), true),
            array(__DIR__ . '/files/test.txt', array('types' => array('image/*')), false),
            array(__DIR__ . '/files/test.txt', array('types' => array('image/png', 'text/plain')), true),
            array(__DIR__ . '/files/test.png', array('types' => array('image/png', 'text/plain')), true),
            array(array(
                'name' => 'test.png',
                'type' => 'image/png',
                'tmp_name' => __DIR__ . '/files/test.png',
                'error' => UPLOAD_ERR_OK,
                'size' => 2000
            ), array('types' => array('image/png', 'text/plain')), true),
            array(array(
                'name' => 'test.png',
                'type' => 'image/png',
                'tmp_name' => __DIR__ . '/files/testss.png',
                'error' => UPLOAD_ERR_OK,
                'size' => 2000
            ), array(), false),
            array(array(
                'name' => 'test.png',
                'type' => 'image/png',
                'tmp_name' => __DIR__ . '/files/test.png',
                'error' => UPLOAD_ERR_OK,
                'size' => 2000000000
            ), array('max_size' => '1 G'), false),
            array(array(
                'name' => 'test.png',
                'type' => 'image/png',
                'tmp_name' => __DIR__ . '/files/test.png',
                'error' => UPLOAD_ERR_OK,
                'size' => 2
            ), array('max_size' => '1 o'), false),
        );
    }

    /**
     * @dataProvider fileProvider
     */
    public function testFile(mixed $value, array $constraints, bool $result) : void {
        $constraint_file = new Core\Validation\Constraint\File($constraints);
        $r = $constraint_file->assert('', $value);
        $this->assertEquals($result, $r->isValid());
    }

    public function dateProvider() : array {
        return array(
            array('0', array(), false),
            array('2 Janvier 2000', array(), false),
            array(0, array(), false),
            array('2000-09-01', array(), true),
            array('2000-09-01 23:32', array(), true),
            array('2000-09-01 23:32:12', array(), true),
            array('1st Jan 2100', array(), true),
            array('2000-09-01', array('strict_format' => true), true),
            array('2000-09-01 23:32', array('strict_format' => true), false),
            array('2000-09-01 23:32:12', array('strict_format' => true), false),
            array('1st Jan 2100', array('strict_format' => true), false),
            array('23/01/2000', array('format' => 'd/m/Y'), true),
            array('2000/01/23', array('format' => 'd/m/Y'), false),
            array('23/01/2000 23:00', array('format' => 'd/m/Y'), false),
            array('2000/01/23 23:00', array('format' => 'd/m/Y', 'strict_format' => false), true),
            array('2000-09-01', array('max' => 'now'), true),
            array('2000-09-01', array('max' => 'now | -1 week'), true),
            array('2000-09-01', array('min' => 'now'), false),
            array('2000-09-01', array('min' => '2000-09-03 | -1 week'), true),
            array('2000-09-02', array('max' => '2000-09-01', 'min' => '2000-09-03'), false),
            array('2000-09-02', array('min' => '2000-09-01', 'max' => '2000-09-03'), true),
            array('2000-09-03', array('min' => '2000-09-01', 'max' => '2000-09-03'), true),
            array('2000-09-01', array('min' => '2000-09-01', 'max' => '2000-09-03'), true),
            array('2000-09-01', array('format' => 'mY/d', 'min' => '2000-09-01', 'max' => '2000-09-03'), false),
            array('092000/01', array('format' => 'mY/d', 'min' => '2000-09-01', 'max' => '2000-09-03'), true),
            array('092000/01', array('format' => 'mY/d', 'min' => '092000/01', 'max' => '092000/03'), true),
            array('2000-09-01', array('min' => '092000/01', 'max' => '092000/03'), false),
            array('2000-09-01', array('min' => 'dazdaz', 'max' => 'sa'), false),
            array('2000-09-01', array('min' => 'dazdaz', 'max' => '2000-09-01'), false),
            array('2000-09-01', array('min' => '', 'max' => '2000-09-01'), false),
            array('2000-09-01', array('min' => '2000-09-01', 'max' => ''), false),
            array('2000-09-01', array('min' => '', 'max' => ''), false),
        );
    }

    /**
     * @dataProvider dateProvider
     */
    public function testDate(mixed $value, array $constraints, bool $result) : void {
        $constraint_date = new Core\Validation\Constraint\DateTime($constraints);
        $r = $constraint_date->assert('', $value);
        $this->assertEquals($result, $r->isValid());
    }

    public function requiredProvider() : array {
        return array(
            array(0, false),
            array(array(), false),
            array('', false),
            array(' ', true),
            array("\n", true),
            array("\t", true),
            array('test', true),
            array('☺', true)
        );
    }

    /**
     * @dataProvider requiredProvider
     */
    public function testRequired(mixed $value, bool $result) : void {
        $constraint_required = new Core\Validation\Constraint\Required();
        $r = $constraint_required->assert('', $value);
        $this->assertEquals($result, $r->isValid());
    }

    public function phoneProvider() : array {
        return array(
            array(0, false),
            array(array(), false),
            array('', false),
            array('00 00 00 00 00', true),
            array('00  00 00 00 00', false),
            array('00 0000 0000', true),
            array('0 0 0000 0000', false),
            array('+00 0 0000 0000', true),
            array('+99 9 9999 9999', true),
            array('+33 0 00 00 00 00', true),
            array('+33 0 00 00 00 0 0', false),
            array('*33 0 00 00 00 00', false),
            array('33 0 00 00 00 00', false),
            array('+33 00 00 00 00', false),
        );
    }

    /**
     * @dataProvider phoneProvider
     */
    public function testPhone(mixed $value, bool $result) : void {
        $constraint_phone = new Core\Validation\Constraint\Phone();
        $r = $constraint_phone->assert('', $value);
        $this->assertEquals($result, $r->isValid());
    }

    public function nullProvider() : array {
        return array(
            array(new Core\Validation\Constraint\Collection(array()), array(), true),
            array(new Core\Validation\Constraint\Collection(array()), array(''), false),
            array(new Core\Validation\Constraint\IsNumber(array()), '', true),
            array(new Core\Validation\Constraint\IsNumber(array()), 0, false),
            array(new Core\Validation\Constraint\IsNumber(array()), '0', false),
            array(new Core\Validation\Constraint\IsNumber(array()), array(), false),
            array(new Core\Validation\Constraint\IsString(array()), '', true),
            array(new Core\Validation\Constraint\IsString(array()), '0', false),
            array(new Core\Validation\Constraint\IsString(array()), ' ', false),
            array(new Core\Validation\Constraint\IsString(array()), array(), false),
            array(new Core\Validation\Constraint\File(array()), array(), false),
            array(new Core\Validation\Constraint\File(array()), '', true),
            array(new Core\Validation\Constraint\File(array()), '0', false),
            array(new Core\Validation\Constraint\File(array()), ' ', false),
            array(new Core\Validation\Constraint\File(array()), array('error' => UPLOAD_ERR_NO_FILE), true),
            array(new Core\Validation\Constraint\File(array()), array('error' => 0), false)
        );
    }

    /**
     * @dataProvider nullProvider
     */
    public function testNull(/* IConstraint */ $constraint, mixed $value, bool $null) : void {
        $this->assertEquals($constraint->isNull($value), $null);
    }
}