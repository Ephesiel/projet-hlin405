<?php 
require_once __DIR__ . '/../site-tests-header.php';
use PHPUnit\Framework\TestCase;

final class TestFormsFunctions extends TestCase {

    // ------------------------------------------------------------------------
    // Test pour les résultats

    public function testResultArray() : void {
        ph_save_form_data(array('a', 'b'), array('c', 'd'));
        $this->assertTrue(isset($_SESSION[PH_FORM_SESSION_KEY]));
        $this->assertTrue(
            array_key_exists('results', $_SESSION[PH_FORM_SESSION_KEY]) &&
            array_key_exists('values', $_SESSION[PH_FORM_SESSION_KEY]) && 
            array_key_exists('global_errors', $_SESSION[PH_FORM_SESSION_KEY])
        );
        $arr = $_SESSION[PH_FORM_SESSION_KEY];
        $same = ph_get_validation_result();
        $this->assertFalse(isset($_SESSION[PH_FORM_SESSION_KEY]));
        $this->assertSame($arr, $same);
        $this->assertSame($arr, ph_get_validation_result());
    }

    // ------------------------------------------------------------------------
    // Batterie de test pour les fichiers

    public function testFileArrayCreation() : void {
        $this->assertFalse(isset($_SESSION[PH_FORM_FILES_KEY]));
        ph_create_session_files_array();
        $this->assertTrue(isset($_SESSION[PH_FORM_FILES_KEY]));
    }

    public function fileProvider() : array {
        return array(
            array(
                array(
                    'a' => array('error' => UPLOAD_ERR_OK, 'size' => 10, 'name' => 'a.txt', 'tmp_name' => __DIR__ . '/files/a.txt'),
                    'b' => array('error' => UPLOAD_ERR_OK, 'size' => 10, 'name' => 'b.txt', 'tmp_name' => __DIR__ . '/files/b.txt'),
                    'c' => array('error' => UPLOAD_ERR_OK, 'size' => 10, 'name' => 'c.txt', 'tmp_name' => __DIR__ . '/files/c.txt'),
                ),
                true
            ),
            array(
                array(
                    'a' => array('error' => UPLOAD_ERR_FORM_SIZE, 'size' => 0, 'name' => '', 'tmp_name' =>''),
                    'b' => array('error' => UPLOAD_ERR_INI_SIZE, 'size' => 0, 'name' => '', 'tmp_name' => ''),
                    'c' => array('error' => UPLOAD_ERR_NO_FILE, 'size' => 0, 'name' => '', 'tmp_name' => ''),
                ),
                false
            ),
        );
    }

    /**
     * @dataProvider fileProvider
     * @depends testFileArrayCreation
     */
    public function testFileSave(array $files, bool $result) : void {
        $_FILES = $files;

        ph_save_current_files();

        if (true === $result) {
            $copiedArray = $_SESSION[PH_FORM_FILES_KEY];
            foreach ($files as $name => $v) {
                $this->assertTrue(array_key_exists($name, $_SESSION[PH_FORM_FILES_KEY]));
            }
        }

        $_SESSION[PH_FORM_FILES_KEY] = array();
        ph_clear_tmp_files();

        $this->assertFalse(isset($_SESSION[PH_FORM_FILES_KEY]));
    }

    // ------------------------------------------------------------------------
    // Test register

    public function emailProvider() : array {
        return array(
            array(ADMIN_EMAIL, true),
            array('', false),
            array(ADMIN_PASSWD, false),
        );
    }

    /**
     * @dataProvider emailProvider
     */
    public function testUserExistence(string $email, bool $result) : void {
        $this->assertSame(ph_user_does_not_exists($email), !$result);
    }

    // ------------------------------------------------------------------------
    // Test des redirections

    public function testRedirectSession() : void {
        $this->assertFalse(isset($_SESSION[PH_REDIRECT_KEY]));
        ph_set_redirect();
        $this->assertSame(ph_get_redirect(), ROOT . '/');
        ph_remove_redirect();
        $this->assertFalse(isset($_SESSION[PH_REDIRECT_KEY]));
    }
}