<?php
require_once __DIR__ . '/../core-tests-header.php';
use PHPUnit\Framework\TestCase;

final class TestValidation extends TestCase {

    public function loginDatas() : array {
        return array(
            // Premier test
            array(
                array(
                    'name' => array(
                        'first_name' => 'David',
                        'last_name' => 'Blanchard'
                    ),
                    'email' => 'david.blanchard@example.com',
                    'password' => '1234abcd',
                    'telephone' => '0956856593'
                ),
                true
            ),
            // Données dans le désordre
            array(
                array(
                    'telephone' => '0956856593',
                    'email' => 'david.blanchard@example.com',
                    'name' => array(
                        'last_name' => 'Blanchard',
                        'first_name' => 'David'
                    ),
                    'password' => '1234abcd'
                ),
                true
            ),
            // Manque une donnée
            array(
                array(
                    'name' => array(
                        'first_name' => 'David',
                        'last_name' => 'Blanchard'
                    ),
                    'email' => 'david.blanchard@example.com',
                    'password' => '1234abcd'
                ),
                false
            ),
            // Manque une donnée de donnée
            array(
                array(
                    'name' => array(
                        'first_name' => 'David'
                    ),
                    'email' => 'david.blanchard@example.com',
                    'password' => '1234abcd',
                    'telephone' => '0956856593'
                ),
                false
            ),
            // Données vides
            array(
                array(
                    'name' => array(
                        'first_name' => '',
                        'last_name' => ''
                    ),
                    'email' => '',
                    'password' => '',
                    'telephone' => ''
                ),
                false
            ),
            // Une seule donnée vide
            array(
                array(
                    'name' => array(
                        'first_name' => '',
                        'last_name' => 'Blanchard'
                    ),
                    'email' => 'david.blanchard@example.com',
                    'password' => '1234abcd',
                    'telephone' => '0956856593'
                ),
                false
            ),
            // Trop court
            array(
                array(
                    'name' => array(
                        'first_name' => 'Da',
                        'last_name' => 'Blanchard'
                    ),
                    'email' => 'david.blanchard@example.com',
                    'password' => '1234abcd',
                    'telephone' => '0956856593'
                ),
                false
            ),
            // Trop long
            array(
                array(
                    'name' => array(
                        'first_name' => 'David',
                        'last_name' => 'Blanchard'
                    ),
                    'email' => 'david.blanchard@example.com',
                    'password' => '1234abcd12345',
                    'telephone' => '0956856593'
                ),
                false
            ),
            // Pas égal
            array(
                array(
                    'name' => array(
                        'first_name' => 'David',
                        'last_name' => 'Blanchard'
                    ),
                    'email' => 'david.blanchard@example.com',
                    'password' => '1234abcd',
                    'telephone' => '+33956856593'
                ),
                false
            ),
            // Pas bon email
            array(
                array(
                    'name' => array(
                        'first_name' => 'David',
                        'last_name' => 'Blanchard'
                    ),
                    'email' => 'david.blanchard@.com',
                    'password' => '1234abcd',
                    'telephone' => '0956856593'
                ),
                false
            ),
            // Type non valide 1
            array(
                array(
                    'name' => 'David Blanchard',
                    'email' => 'david.blanchard@example.com',
                    'password' => '1234abcd',
                    'telephone' => '0956856593'
                ),
                false
            ),
            // Type non valide 2
            array(
                array(
                    'name' => array(
                        'first_name' => 'David',
                        'last_name' => 'Blanchard'
                    ),
                    'email' => 'david.blanchard@example.com',
                    'password' => '1234abcd',
                    'telephone' => array(
                        'region' => '+33',
                        'numero' => '0956856593'
                    )
                ),
                false
            ),
            // Type non valide 3
            array(
                array(
                    'name' => array(
                        'first_name' => 'David',
                        'last_name' => 'Blanchard'
                    ),
                    'email' => 12,
                    'password' => '1234abcd',
                    'telephone' => '0956856593',
                ),
                false
            ),
            // Trop de clé 1
            array(
                array(
                    'name' => array(
                        'first_name' => 'David',
                        'last_name' => 'Blanchard',
                        'autre' => 'on s\'en fout'
                    ),
                    'email' => 'david.blanchard@example.com',
                    'password' => '1234abcd',
                    'telephone' => '0956856593'
                ),
                true
            ),
            // Trop de clé 2
            array(
                array(
                    'name' => array(
                        'first_name' => 'David',
                        'last_name' => 'Blanchard'
                    ),
                    'email' => 'david.blanchard@example.com',
                    'password' => '1234abcd',
                    'telephone' => '0956856593',
                    'autre' => 'on s\'en fout'
                ),
                true
            ),
            // Rien
            array(
                array(),
                false
            )
        );
    }

    /**
     * @dataProvider loginDatas
     */
    public function testLoginValidation(array $datas, bool $result) : void {
        $constraints = new Core\Validation\Constraint\Collection(array(
            'name' => new Core\Validation\Constraint\Collection(array(
                'first_name' => new Core\Validation\Constraint\Length(array('min' => 3)),
                'last_name' => new Core\Validation\Constraint\Length(array('min' => 2))
            )),
            'email' => new Core\Validation\Constraint\Email(),
            'password' => new Core\Validation\Constraint\Length(array(
                'min' => 8,
                'max' => 12
            )),
            'telephone' => new Core\Validation\Constraint\Length(array('equals' => 10))
        ));

        $validator = new Core\Validation\FormValidator($datas, $constraints);
        $this->assertEquals($result, $validator->isValid());

        $array = $validator->formMessages();
        $this->assertTrue(array_key_exists('success', $array));
        $this->assertTrue(array_key_exists('fields', $array));
        $this->assertFalse(array_key_exists('name', $array));
        $this->assertSame($array['success'], $result);

        $fields = $array['fields'];
        foreach ($fields as $field) {
            $this->assertTrue(array_key_exists('name', $field));
            $this->assertTrue(array_key_exists('success', $field));
            $this->assertTrue(is_bool($field['success']));
            if (array_key_exists('messages', $field)) {
                $this->assertTrue(is_array($field['messages']));
                $this->assertFalse(empty($field['messages']));
            }
            if (array_key_exists('fields', $field)) {
                $this->assertTrue(is_array($field['fields']));
                $this->assertFalse(empty($field['fields']));
                $this->assertEquals($field['fields']['first_name']['name'], 'first_name');
                $this->assertEquals($field['fields']['last_name']['name'], 'last_name');
            }
        }

        $validator->addMessageToField('first_name', 'petit message');
        $validator->addMessageToField('telephone', 'petit message');
        $this->assertEquals($result, $validator->isValid());
    
        $array = $validator->formMessages();
        if (!isset($array['fields']['name']['fields'])) {
            return;
        }

        $first_name = $array['fields']['name']['fields']['first_name'];
        $telephone = $array['fields']['telephone'];

        $this->assertTrue(array_key_exists('messages', $first_name));
        $this->assertTrue(array_key_exists('messages', $telephone));
        $this->assertTrue(in_array('petit message', $first_name['messages'], $strict = true));
        $this->assertTrue(in_array('petit message', $telephone['messages'], $strict = true));

        $validator->addErrorToField('first_name', 'petite erreur');
        $this->assertFalse($validator->isValid());
        $validator->addErrorToField('telephone', 'petite erreur');
        $this->assertFalse($validator->isValid());
    
        $array = $validator->formMessages();
        $first_name = $array['fields']['name']['fields']['first_name'];
        $telephone = $array['fields']['telephone'];

        $this->assertTrue(array_key_exists('messages', $first_name));
        $this->assertTrue(array_key_exists('messages', $telephone));
        $this->assertTrue(in_array('petite erreur', $first_name['messages'], $strict = true));
        $this->assertTrue(in_array('petite erreur', $telephone['messages'], $strict = true));
        $this->assertFalse($first_name['success']);
        $this->assertFalse($telephone['success']);

        $this->expectException(Exception::class);
        $validator->addErrorToField('none-existing-field', 'petite erreur');
    }
}