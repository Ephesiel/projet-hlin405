PATH=$PATH:.

XDEBUG_MODE=coverage phpunit --configuration phpunit.xml --exclude-group serverTestsOnly,longTests --coverage-text --colors=never --coverage-html coverage
