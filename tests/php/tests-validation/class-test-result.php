<?php
require_once __DIR__ . '/../core-tests-header.php';
use PHPUnit\Framework\TestCase;

final class TestResult extends TestCase {
    public function testArray() : void {
        $result = new Core\Validation\Constraint\Result('test');
        $array = $result->toArray();

        $this->assertTrue(is_array($array));
        $this->assertEquals(count($array), 2);
        $this->assertTrue(array_key_exists('success', $array));
        $this->assertTrue(array_key_exists('name', $array));
        $this->assertEquals($array['name'], 'test');
    }

    /**
     * @depends testArray
     */
    public function testValidation() : void {
        $result = new Core\Validation\Constraint\Result('test');
        $this->assertTrue($result->isValid());
        $this->assertTrue($result->toArray()['success']);
        $result->setInvalid();
        $this->assertFalse($result->isValid());
        $this->assertFalse($result->toArray()['success']);
        $result->setInvalid();
        $this->assertFalse($result->isValid());
        $this->assertFalse($result->toArray()['success']);
    }

    /**
     * @depends testArray
     */
    public function testMessages() : void {
        $result = new Core\Validation\Constraint\Result('test');

        $array = $result->toArray();
        $this->assertFalse(array_key_exists('messages', $array));

        $result->addMessage('Un joli petit message');
        $result->addMessage('Un deuxième message');

        $array = $result->toArray();
        $this->assertTrue(array_key_exists('messages', $array));
        $this->assertEquals(count($array['messages']), 2);
        $this->assertEquals($array['messages'][0], 'Un joli petit message');
        $this->assertEquals($array['messages'][1], 'Un deuxième message');

        $this->assertEquals($array['messages'], $result->getMessages());
    }

    /**
     * @depends testValidation
     */
    public function testSubResults() : void {
        $result = new Core\Validation\Constraint\Result('test');
        $sub_result = new Core\Validation\Constraint\Result('sub_test');

        $result->addResult($sub_result);
        $this->assertTrue($result->isValid());
        
        $sub_result->setInvalid();
        $this->assertFalse($result->isValid());

        $array = $result->toArray();
        $this->assertTrue(array_key_exists('fields', $array));
        $this->assertTrue(is_array($array['fields']));
        $this->assertEquals(count($array['fields']), 1);
        $this->assertTrue(array_key_exists('sub_test', $array['fields']));
        $this->assertTrue(is_array($array['fields']['sub_test']));
        $this->assertEquals(count($array['fields']['sub_test']), 2);
        $this->assertTrue(array_key_exists('success', $array['fields']['sub_test']));
        $this->assertTrue(array_key_exists('name', $array['fields']['sub_test']));
        $this->assertFalse($array['fields']['sub_test']['success']);
        $this->assertEquals($array['fields']['sub_test']['name'], 'sub_test');
    }
}