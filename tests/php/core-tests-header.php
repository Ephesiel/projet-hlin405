<?php
// S'occupe des inclusions nécessaires pour le bon fonctionnement des tests sur les classes du coeur.
// Pour tout ce qui est test sur le site, utilisez plutôt `site-tests-header.php`
// Les fichiers incluent ici sont aussi incluent pour le site

declare(strict_types = 1);
require_once __DIR__ . '/../../src/core/tags/tags-utils.php';