<?php
require_once __DIR__ . '/../site-tests-header.php';
use PHPUnit\Framework\TestCase;

final class TestFormRenderer extends TestCase {
    private string $form = __DIR__ . '/includes/form-test.php';

    public function testConstructorPathException() : void {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('Erreur : le chemin pour inclure un formulaire ne doit pas être vide.');

        $form = new PH\Templates\BootstrapForm('', array());
    }

    public function testConstructorArrayException() : void {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage('Erreur : le tableau donné ne correspond pas à celui attendu pour rendre le formulaire.');

        $form = new PH\Templates\BootstrapForm($this->form, array());
    }

    public function testBasicRender() : void {
        $form = new PH\Templates\BootstrapForm($this->form, null);

        $regex = '/(?s).*<form[^<>]*>.*';
        $regex .= '<input[^<>]*name=(\'email\'|"email")[^<>]*>.*';
        $regex .= '<input[^<>]*name=(\'password\'|"password")[^<>]*>.*';
        $regex .= '<input[^<>]*name=(\'profile-picture\'|"profile-picture")[^<>]*>.*';
        $regex .= '<input[^<>]*name=(\'test1\'|"test1")[^<>]*value=(\'1\'|"1")[^<>]*>.*';
        $regex .= '<input[^<>]*name=(\'test1\'|"test1")[^<>]*value=(\'2\'|"2")[^<>]*>.*';
        $regex .= '<input[^<>]*name=(\'test1\'|"test1")[^<>]*value=(\'3\'|"3")[^<>]*>.*';
        $regex .= '<input[^<>]*name=(\'test-bis\'|"test-bis")[^<>]*value=(\'1\'|"1")[^<>]*>.*';
        $regex .= '<input[^<>]*name=(\'test-bis2\'|"test-bis2")[^<>]*value=(\'2\'|"2")[^<>]*>.*';
        $regex .= '<input[^<>]*name=(\'test-bis3\'|"test-bis3")[^<>]*value=(\'3\'|"3")[^<>]*>.*';
        $regex .= '<\/form>.*/';

        $this->expectOutputRegex($regex);
        $form->render();
    }

    public function validDataProvider() : array {
        return array(
            array(array(
                'values' => array(
                    'email' => 'abcd@e.f',
                    'password' => 'qwewrqwer', 
                    'profile-picture' => 'truc.machin', 
                    'test-bis3' => '3',
                    'test1' => '1', 
                ),
                'results' => array(
                    'success' => true,
                    'fields' => array(
                        array('success' => true, 'name' => 'email', 'messages' => array()),
                        array('success' => true, 'name' => 'password', 'messages' => array()),
                        array('success' => true, 'name' => 'profile-picture', 'messages' => array()),
                    ),
                ),
                'global_errors' => array(),
            )),
            array(array(
                'values' => array(
                    'email' => 'abcd@e.f',
                    'password' => 'qwewrqwer', 
                    'profile-picture' => 'truc.machin',
                    'test1' => '1', 
                ),
                'results' => array(
                    'success' => true,
                    'fields' => array(
                        array('success' => true, 'name' => 'email', 'messages' => array()),
                        array('success' => true, 'name' => 'password', 'messages' => array()),
                        array('success' => true, 'name' => 'profile-picture', 'messages' => array()),
                    ),
                ),
                'global_errors' => array(),
            )),
        );
    }

    /**
     * @dataProvider validDataProvider
     */
    public function testValidEmail(array $valid_datas) : void {
        $form = new PH\Templates\BootstrapForm($this->form, $valid_datas);

        $value = $valid_datas['values']['email'];

        $q = '(\'|")';
        $regex = "/(?s).*<input +name={$q}email$q +type={$q}email$q +class={$q}form-control is-valid$q +id={$q}email$q +placeholder={$q}nom@exemple.fr$q +value={$q}$value$q *>.*/";
        $this->expectOutputRegex($regex);
        $form->render();
    }

    /**
     * @dataProvider validDataProvider
     * @depends testValidEmail
     */
    public function testValidPassword(array $valid_datas) : void {
        $form = new PH\Templates\BootstrapForm($this->form, $valid_datas);

        $value = $valid_datas['values']['password'];

        $q = '(\'|")';
        $regex = "/(?s).*<input +name={$q}password$q +type={$q}password$q +class={$q}form-control is-valid$q +id={$q}password$q +placeholder=$q$q +value={$q}$value$q *>.*/";
        $this->expectOutputRegex($regex);
        $form->render();
    }

    /**
     * @dataProvider validDataProvider
     * @depends testValidPassword
     */
    public function testValidPP(array $valid_datas) : void {
        $form = new PH\Templates\BootstrapForm($this->form, $valid_datas);

        $value = $valid_datas['values']['profile-picture'];

        $q = '(\'|")';
        $regex = "/(?s).*<input +name={$q}profile-picture$q +class={$q}form-control is-valid$q +type={$q}file$q +id={$q}profile-picture{$q}[^<>]*>.*";
        $regex .= "<script>.*let newFile0 = new File\(\[''\], '$value', {type: ''}\);.*<\/script>/";
        $this->expectOutputRegex($regex);
        $form->render();
    }

    /**
     * @dataProvider validDataProvider
     * @depends testValidPP
     */
    public function testValidRadio(array $valid_datas) : void {
        $form = new PH\Templates\BootstrapForm($this->form, $valid_datas);

        $value = $valid_datas['values']['test1'];

        $q = '(\'|")';
        $regex = "/(?s).*<input +name={$q}test1$q type={$q}radio$q +class={$q}form-check-input$q +[^<>]*value={$q}$value{$q} +checked[^<>]*>.*/";
        $this->expectOutputRegex($regex);
        $form->render();
    }

    /**
     * @dataProvider validDataProvider
     * @depends testValidRadio
     */
    public function testValidCheckBox(array $valid_datas) : void {
        $form = new PH\Templates\BootstrapForm($this->form, $valid_datas);

        $vs = $valid_datas['values'];
        $value = isset($vs['test-bis']) ? $vs['test-bis'] : (isset($vs['test-bis2']) ? $vs['test-bis2'] : (isset($vs['test-bis3']) ? $vs['test-bis3'] : ''));

        $q = '(\'|")';
        $regex = '';
        if (empty($value)) {
            $regex = "/(?s).*<input((?!(checked)).)*type={$q}checkbox$q((?!(checked)).)*>.*/";
        }
        else {
            $regex = "/(?s).*<input[^<>]*type={$q}checkbox{$q}[^<>]*value={$q}$value$q +checked[^<>]*>.*/";
        }
        $this->expectOutputRegex($regex);

        $form->render();
    }

    public function invalidDataProvider() : array {
        return array(
            array(array(
                'values' => array(
                    'email' => '',
                    'password' => '', 
                    'profile-picture' => 'test', 
                    'test-bis' => '1',
                    'test1' => '2', 
                ),
                'results' => array(
                    'success' => false,
                    'fields' => array(
                        array('success' => false, 'name' => 'email', 'messages' => array('L\'email ne doit pas être vide.')),
                        array('success' => false, 'name' => 'password', 'messages' => array('Le mot de passe ne doit pas être vide')),
                        array('success' => false, 'name' => 'profile-picture', 'messages' => array('La photo de profil ne doit pas être vide.')),
                    ),
                ),
                'global_errors' => array('Une erreur.'),
            )),
        );
    }

    /**
     * @dataProvider invalidDataProvider
     */
    public function testGlobalError(array $datas) : void {
        $form = new PH\Templates\BootstrapForm($this->form, $datas);

        $regex = "/(?s).*<div[^<>]*class=('alert alert-danger'|\"alert alert-danger\")[^<>]*>.*<\/div>.*<form.*>.*/";

        $this->expectOutputRegex($regex);
        $form->render();
    }

    /**
     * @dataProvider invalidDataProvider
     */
    public function testInvalidEmail(array $valid_datas) : void {
        $form = new PH\Templates\BootstrapForm($this->form, $valid_datas);

        $value = $valid_datas['values']['email'];
        $result = null;

        foreach ($valid_datas['results']['fields'] as $res) {
            if ('email' === $res['name']) {
                $result = $res;
            }
        }

        $q = '(\'|")';
        $regex = "/(?s).*<input +name={$q}email$q +type={$q}email$q +class={$q}form-control is-invalid$q +id={$q}email$q +placeholder={$q}nom@exemple.fr$q ";
        if (false === empty($value)) {
            $regex .= "+value={$q}$value$q ";
        }
        $regex .= "*>.*<div class={$q}invalid-feedback{$q}[^<>]*>";

        foreach ($result['messages'] as $message) {
            $regex .= '.*' . htmlentities($message);
        }
        $regex .= ".*<\/div>.*/";

        $this->expectOutputRegex($regex);
        $form->render();
    }

    /**
     * @dataProvider invalidDataProvider
     * @depends testInvalidEmail
     */
    public function testInvalidPassword(array $valid_datas) : void {
        $form = new PH\Templates\BootstrapForm($this->form, $valid_datas);

        $value = $valid_datas['values']['password'];
        $result = null;

        foreach ($valid_datas['results']['fields'] as $res) {
            if ('password' === $res['name']) {
                $result = $res;
            }
        }

        $q = '(\'|")';
        $regex = "/(?s).*<input +name={$q}password$q +type={$q}password$q +class={$q}form-control is-invalid$q +id={$q}password$q +placeholder=$q$q ";
        if (false === empty($value)) {
            $regex .= "+value={$q}$value$q ";
        }
        $regex .= "*>.*<div class={$q}invalid-feedback{$q}[^<>]*>";

        foreach ($result['messages'] as $message) {
            $regex .= '.*' . htmlentities($message);
        }
        $regex .= ".*<\/div>.*/";
        $this->expectOutputRegex($regex);
        $form->render();
    }

    /**
     * @dataProvider invalidDataProvider
     * @depends testInvalidPassword
     */
    public function testInvalidPP(array $valid_datas) : void {
        $form = new PH\Templates\BootstrapForm($this->form, $valid_datas);

        $value = $valid_datas['values']['profile-picture'];
        $result = null;

        foreach ($valid_datas['results']['fields'] as $res) {
            if ('profile-picture' === $res['name']) {
                $result = $res;
            }
        }

        $q = '(\'|")';
        $regex = "/(?s).*<input +name={$q}profile-picture$q +class={$q}form-control is-invalid$q +type={$q}file$q +id={$q}profile-picture{$q}[^<>]*>.*";
        $regex .= "<div class={$q}invalid-feedback{$q}[^<>]*>";

        foreach ($result['messages'] as $message) {
            $regex .= '.*' . htmlentities($message);
        }
        $regex .= ".*<\/div>.*";
        $regex .= "<script>.*let newFile0 = new File\(\[''\], '$value', {type: ''}\);.*<\/script>/";
        $this->expectOutputRegex($regex);
        $form->render();
    }
}