\documentclass[french,12pt]{report}

% Packages avec option
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[toc,page]{appendix} 

% Others packages
% Fullpage = Asked
% Hyperref = Links
% Graphicx = Include images
% Float    = figures placement
\usepackage{babel, titlesec, fullpage, hyperref, graphicx, float, epigraph, minted, csquotes}

\usepackage{chngcntr}
\counterwithout{figure}{chapter}
% Paramètres Hyperliens
\hypersetup{
    colorlinks=true,        % Faux : liens avec une boite autour
    linkcolor=black,        % Couleur des liens internes au document
    citecolor=black,        % Couleur des liens de citation bibliographique
    filecolor=black,        % Couleur des liens vers des fichiers
    urlcolor=cyan           % Couleur des liens externes
}

% FORMAT DES CHAPITRES
\titleformat
{\chapter} % command
[display] % shape
{\bfseries\Large\itshape} % format
{Chapitre \ \thechapter} % label
{0ex} % sep
{
    \rule{\textwidth}{1pt}
    \centering
} % before-code
[
\vspace{-1.5ex}%
\rule{\textwidth}{0.3pt}
] % after-code  
% FIN FORMAT DES CHAPITRES 

\renewcommand{\appendixtocname}{Annexes}
\renewcommand{\appendixpagename}{Annexes}
\renewcommand{\thefigure}{\arabic{figure}}

\setlength{\epigraphwidth}{0.5\textwidth}

\begin{document}
    \begin{titlepage}
        \begin{center}
            {\large Université de Montpellier } \\
            {\large L2 Informatique} \\
            \vspace{3cm}
            \centerline{\rule{\textwidth}{0.5pt}}
            \vspace{.5cm}
            \begin{huge} Bracket Creator \end{huge} \\
            \vspace{.2cm}
            {\large Un outil d'édition de tournois en ligne} \\
            \centerline{\rule{\textwidth}{0.5pt}}
            \vspace{3cm}
            {\large Rapport de T.E.R.} \\
            \vspace{.2cm}
            {\large Projet Informatique HLIN405} \\
            \vspace{1cm}
            \textbf{Encadrant} : \\ M. David \textsc{Delahaye} \\
            \vspace{.5cm}
            \textbf{Étudiants} : \\ M. Arda \textsc{Uzan} \\ M. Quentin \textsc{Baiget} \\ M. Benoît \textsc{Huftier} \\ M. Johann \textsc{Rosain} \\
            \vspace{1cm}
            \textbf{Code source} : \url{https://gitlab.com/Ephesiel/projet-hlin405/} \\
            \vspace{1cm}
            \includegraphics[height=3cm]{resources/UM-logo.png} \includegraphics[height=3cm]{resources/logo_fds.png}
        \end{center}
    \end{titlepage}

    % On ne veut pas compter le sommaire dans le total des pages
    \tableofcontents\addtocontents{toc}{\protect\thispagestyle{empty}}
    \clearpage
    
    \setcounter{page}{1}
    \chapter*{Introduction}
    \addcontentsline{toc}{part}{Introduction}
    %\epigraph{Les petits riens d'une construction négligée, comme une porte qui ferme mal ou un carreau légèrement ébréché, [$\dots$] annulent totalement le charme de l'ensemble.}{James O. Coplien, \textit{Coder proprement} (2009, Préface)}

    Dans le cadre du projet informatique du second semestre de deuxième année de licence à l'Université de Montpellier, nous avons été amenés à développer un site web qui permet la création et la gestion de tournois.

    \vspace{.1cm}

    Notre groupe était au départ composé de quatre personnes : Arnaud \textsc{Peler}, Quentin \textsc{Baiget}, Benoît \textsc{Huftier} et Johann \textsc{Rosain}. Suite à des problèmes personnels, Arnaud \textsc{Peler} a malheureusement quitté le groupe, avant de faire une quelconque contribution au projet. Nous avons ensuite, vers le milieu du projet, été rejoints par Arda \textsc{Uzan}. Le projet a été réalisé sur une période de 3 mois, du 8 février au 7 mai 2021, et notre groupe a été encadré par M. David \textsc{Delahaye}.

    \vspace{.1cm}

    Notre groupe étant composé de deux étudiants ayant déjà développé un site professionnel, nous nous sommes fixés pour objectif une chose : d'avoir un code propre, qu'il est facile de maintenir, d'utiliser, et de réutiliser.

    \vspace{.1cm}

    Nous avons ainsi défini une liste d'éléments nécessaires à l'élaboration d'un site structuré :
    \begin{itemize}
        \item Un outil de travail collaboratif avec du code qui fonctionne sur toutes les plateformes.
        \item Des règles de programmation, pour que le style du code soit uniforme. 
        \item Une structure flexible, qui permet une factorisation maximale des différentes parties à programmer.
        \item Un système de \textit{tests}, pour s'assurer que ce que nous faisons fonctionne correctement et continue de fonctionner dans le futur.
        \item Des \textit{code reviews} faites par nos pairs, qui permettent d'éviter des \textit{bugs} flagrants, et qui s'assurent que tout le monde est à jour dans le projet.
    \end{itemize}

    \vspace{.1cm}

    Nous allons donc tout d'abord parler de l'organisation du groupe et de la division des tâches, pour ensuite s'arrêter sur l'architecture de notre site et sa base de données associée, et enfin aborder les particularités techniques, et conclure sur l'expérience que ce projet nous a apporté.

    \chapter{Organisation du groupe}

    %\epigraph{La code review ne sert pas à juger. La code review sert à l’amélioration de la qualité de l’application et aux partages des connaissances dans l’équipe.}{Jesuisundev, \textit{Bien donner et recevoir une code review}}

    \section{Outil recherché}

    Il y a 3 points parmi ceux définis précédemment qui entrent dans le domaine de l'organisation, et plus précisément, dans le domaine du développement collaboratif. 
    Notre but était donc de trouver une solution web qui permettait : 
    \begin{enumerate}
        \item {\textbf{Le partage de code}, pour que tout le monde puisse utiliser la dernière version du site, et puisse travailler de manière agréable.}
        \item {\textbf{L'attribution de tâches}, pour savoir en détail ce que chaque personne doit faire, et avoir un historique de ce que chacun a fait.}
        \item {\textbf{D'avoir un espace « wiki »}, afin de noter les règles importantes à respecter, et décrire l'utilisation de l'architecture développée.}
        \item {\textbf{De pouvoir faire des \textit{Code Reviews}}, facilement et rapidement.}
    \end{enumerate}  

    Après s'être renseignés sur les solutions existantes, nous avons choisi l'outil \textit{GitLab}\footnote{GitLab : \url{https://gitlab.com/Ephesiel/projet-hlin405}}, sur un serveur hébergé par les développeurs de cette solution (car celui de la faculté était souvent hors service, ce qui nous empêchait d'avancer).
    
    Nous avons ainsi pu compartimenter le travail à faire, et défini des règles simples pour l'utilisation de cet outil.

    \section{Notre utilisation de \textit{GitLab}}

    Pour garder une organisation du projet structurée, nous nous sommes servis des modules proposés par l'outil, en mettant en place :
    \begin{itemize}
        \item Un système d'attribution de tâches, en utilisant des \textit{issues} avec le \textit{label}\footnote{Label : Étiquette en français, cela permet de catégoriser les l'\textit{issues}.} « Nouvelle fonctionnalité », qui définit chaque tâche à faire et la date de rendu, et qui assigne la tâche à un membre du groupe.
        \item Plusieurs pages de \textit{wiki}, qui référencent les règles d'écriture des différents langages, l'utilisation de la structure \textit{php} pour créer une page, et quelques guides, notamment pour l'utilisation des commandes de base de \textit{git}.
        \item Un système de \textit{Code Reviews} en utilisant des \textit{issues} avec le \textit{label} \textquote{Code Review}, où un \textit{template} de description de la \textit{review} est fourni et le développeur n'a qu'à compléter.
        \item Une \textit{pipeline} qui exécute automatiquement les \textit{tests php}\footnote{Une section sera dédiée aux tests dans la suite du document} pour vérifier que rien n'a été cassé sur un \textit{commit}\footnote{Commit : Procédé de git qui permet de valider les modifications faites.}.
    \end{itemize}

    \section{Répartition des tâches}

    Les tâches ont rapidement été découpées pour que tout le monde ait du travail à faire. Nous avons fractionné le travail en 3 catégories : 
    \begin{itemize}
        \item \textsc{html} / \textsc{css} : Quentin \textsc{Baiget}
        \item Javascript : Arda \textsc{Uzan}, Benoît \textsc{Huftier}, Johann \textsc{Rosain}
        \item {\textsc{php} :
            \begin{enumerate}
                \item Benoît \textsc{Huftier}, qui a conçu et développé le système de validation de formulaire et de contraintes dans le module \textit{core}, ainsi que plusieurs pages du site.
                \item Johann \textsc{Rosain}, qui a conçu et développé l'architecture du site (notamment les deux systèmes de \textit{renderer}), et qui a aussi travaillé sur la plupart des validations de formulaire.
            \end{enumerate}
        } 
    \end{itemize}

    Ainsi, tout le monde pouvait travailler sur une fonctionnalité différente en même temps, grâce au \textit{git} bien organisé et à la création d'une \textit{branche} par fonctionnalité à programmer.

    Au début du projet, nous avions l'intention de travailler pour sortir une version 1 en un peu plus d'un mois, puis une version 2 après le temps qu'il nous restait avant de clôturer le projet. Néanmoins, petit à petit, au cours du projet, nous nous sommes aperçus que nous n'aurions pas le temps de faire cela, car nous avons privilégié un code propre, relu, et testé plutôt que d'aller vite et avoir de nombreux bugs.

    Finalement, notre développement n'a donc pas du tout suivi le planning posé au début du projet, et nous nous sommes retrouvés avec le planning décrit dans l'\hyperref[sec:B]{annexe A}.

    \chapter{L'architecture}

    %\epigraph{Nous sommes nombreux à avoir vécu le cauchemar d'un projet sans aucune méthode pour le guider. L'absence de méthode efficace entraîne une imprévisibilité, des erreurs répétées et des efforts inutiles}{Robert C. Martin, \textit{Agile software development principles, patterns, and practices}}

    Comme évoqué précédemment, deux membres de l'équipe ont déjà eut affaire à des projets professionnels. Ils connaissent bien les problèmes liés à la mauvaise gestion d'un projet, en particulier le fait de ne pas avoir une architecture flexible.

    C'est ainsi que la première semaine du projet a été passée à concevoir et à développer une architecture qui soit :
    \begin{itemize}
        \item Flexible 
        \item Facile à utiliser 
        \item Facile à modifier
    \end{itemize}

    Nous avons décidé de regrouper tout ce qui n'impacte pas directement le site dans un module \textit{core}, qui contient alors deux systèmes pensés pour suivre tous les points précédents, pour finalement arriver au schéma de conception de la \hyperref[fig:Conception]{figure 1}.

    \begin{figure}[htp!]
        \begin{center}
            \includegraphics[width=\textwidth]{resources/UML-architecture-site.png}
            \caption{Conception simplifiée de l'architecture du site.}
            \label{fig:Conception}
        \end{center}
    \end{figure}

    \section{Le système de rendu de page}

    Nous avons donc conçu un système avec deux fonctionnalités principales. Il permet de \textbf{gérer les sections affichées} de la page et de \textbf{configurer l'agencement de la page}. 

    \subsection{La gestion des sections}

    Ce système permet de contrôler l'affichage de certaines sections de deux manières différentes :

    \begin{enumerate}
        \item Grâce à des \textit{permissions}, qui est en fait un système de \textit{flags}\footnote{Flag : c'est un ensemble de \textit{bits} qui fournissent une information selon leur état.}. Nous associons ces permissions à des classes \textsc{html} de la page, et selon les permissions de l'utilisateur actuel, ces sections sont supprimées s'il n'a pas le droit de les voir.
        \item Grâce à des \textit{prédicats}, avec un système assez similaire à celui des permissions. Un certain prédicat est associé à une classe \textsc{html}, et s'il est faux, alors cette section est supprimée.
    \end{enumerate}

    De cette manière, le développeur \textit{frontend}, donc \textsc{html} / \textsc{css}, ne s'occupe que de son code à lui. Il n'y a pas (ou très peu) de \textsc{php} dans les pages \textsc{html}, et cela évite d'avoir une page incompréhensible et dure à maintenir. Il n'y a que le strict nécessaire, ce qui fait que refaire la page ou bien en changer une partie n'est pas difficile. 

    \subsection{L'agencement d'une page}

    Nous nous sommes également dit, qu'outre le système précédent, mettre en place un moyen efficace d'avoir toujours le même agencement de pages est très pratique pour éviter la répétition de code, ainsi que pour éviter de devoir changer les sections qui structurent notre site dans les 15 pages qui le composent.

    C'est ici, qu'encore une fois, la classe \textit{PageRenderer} entre en jeu. Cette classe est \textit{générique}, c'est une classe-mère qui gère tout le système qu'on ne veut pas avoir à refaire dans ses classes-filles. Elle fournit la fonction \textit{render}, qui appelle une méthode qui construit un objet \textit{DOMDocument}, et l'écrit sur la page.

    C'est aux classes-filles d'implémenter cette méthode, et c'est le système qui nous permet d'agencer la page comme nous l'entendons. Par exemple, le modèle utilisé pour toutes les pages de notre site, appelé \textit{BootstrapPage} (car il inclut la feuille css et le script js bootstrap), affiche les éléments suivants :

    \begin{itemize}
        \item La balise \textit{head}, ainsi que son contenu
        \item {Le \textit{corps} de la page, avec : 
            \begin{itemize}
                \item Le \textit{header} du site, configuré au préalable
                \item Une section avec la classe \textit{container}
                \item Les messages de succès, s'il y en a
                \item Le corps de la page en lui même 
                \item Le footer
                \item L'inclusion des fichiers javascript
            \end{itemize}
        }
    \end{itemize}

    De plus, les sections restreintes sur le \textit{header} et le \textit{footer} peuvent aussi être gérées dans cette classe-fille, ce qui nous permet également de factoriser la gestion des accès. 

    \section{La construction d'une page}

    En plus de ce système de rendu, nous avons fait en sorte qu'il soit facile à utiliser. Pour ce faire, nous nous sommes inspirés de \textit{wordpress}\footnote{Wordpress : \url{https://github.com/WordPress/WordPress/}}, et à la création d'une page du site, il faut inclure un fichier : \textit{site-header.php}. Ce fichier sert à charger tout ce dont nous avons besoin :

    \begin{itemize}
        \item Définition de constantes du site lues depuis le fichier \textit{env.cfg}.
        \item Inclusion de tous les fichiers de fonction.
        \item Inclusion de l'autoloader.
        \item Installation et mise à jour de la base de données.
        \item Définition des constantes de l'utilisateur.
    \end{itemize}

    Nous savons que tout le monde n'est pas sur le même environnement. Nous avons envisagé l'installation pour tous les membres de l'équipe d'un conteneur \textit{Docker}, qui règlerait ce problème. Seulement, nous nous sommes dit que ce serait peut-être beaucoup demander à chacun de faire ceci, et comme personne n'avait déjà travaillé avec cet outil, les problèmes rencontrés auraient pris plus de temps à résoudre que d'autres problèmes de compatibilité.

    Ainsi, chaque développeur peut définir ses propres paramètres depuis le fichier \textit{config.php}. Notamment, il définit le compte administrateur, la base de données à laquelle se connecter, ainsi que l'ajout ou non des \textit{placeholders} dans la base de données. 

    Tout ce système permet de créer une page, minimale, en quelques lignes :

    \begin{minted}{php}
<?php

require_once __DIR__ . '/site-header.php';

// Création de la page
$page = new PH\Templates\BootstrapPage();

// Ajout de notre fichier HTML
$page->setBody(ph_get_body('accueil.php'));

// Ajout d'un titre (optionnel, mais conseillé)
$page->setTitle("Accueil de mon super site !");

// On rend la page !
$page->render();
    \end{minted}

    \chapter{La base de données}

    %\epigraph{Il ne suffit pas d'éviter les mauvais \textit{designs}. Il peut y avoir un grand nombre de bonnes conceptions parmi lesquelles nous devons choisir. [...] Ce choix, bien que simple, peut faire une différence importante dans les aspects [du projet] qui peuvent être bien modélisés.}{A. Silberschatz, H. F. Korth, S. Sudarshan, \textit{Database System Concepts 7th edition}, 2019}

    La base de données, tout comme le planning de développement, est \textit{vivante}. En effet, elle née au début du projet, où les concepteurs essaient de faire au mieux pour tout prévoir. Cependant elle évolue tout au long du projet, pour répondre aux besoins grandissants des développeurs. 
    
    La structure globale reste inchangée, mais de nombreuse petites modifications sont effectuées sur les différentes tables pour mieux répondre aux besoins réels des développeurs.

    Nous allons ici décrire le schéma final, avec la version simplifiée consultable sur la \hyperref[fig:BDD]{figure 2}, et une version complète sur l'\hyperref[sec:D]{annexe B}.

    \begin{figure}[htp]
        \begin{center}
            \includegraphics[width=0.8\textwidth]{resources/UML-BDD-site.png}
            \caption{Schéma simplifié de la base de données.}
            \label{fig:BDD}
        \end{center}
    \end{figure}

    \section{Les utilisateurs}

    Nous allons parler dans cette première partie des utilisateurs. C'était la première chose à mettre en place dans le site pour avoir accès aux différentes pages. Il y a trois tables intéressantes :
    \begin{itemize}
        \item \textit{user}, qui enregistre les données d'un utilisateur.
        \item \textit{role}, qui est une liste des rôles disponibles dans le site.
        \item \textit{user\_role}, qui est une liste de rôles par utilisateur.
    \end{itemize}

    Nous avons procédé de cette façon pour une raison principale : un utilisateur \textit{peut avoir} plusieurs rôles. En effet, un utilisateur peut être \textit{en même temps} un joueur et un gestionnaire de tournois.
    
    Ainsi, ces 3 tables permettent de mettre facilement plusieurs rôles à un utilisateur. Elles permettent aussi d'ajouter autant de rôles que nous le souhaiterions dans le futur, et qu'ils pourront toujours être assignés facilement. Changer le nom d'un rôle peut-être fait tout aussi facilement. C'est une structure facile à utiliser et flexible, qui permet de faire des modifications dans le futur sans avoir à repenser la base de données.

    \section{Les équipes}

    Les équipes sont assez liées aux utilisateurs. En effet, dans notre système, un joueur doit être dans une équipe pour pouvoir jouer un tournoi. Ainsi, c'est la seconde étape après les utilisateurs pour pouvoir créer un tournoi. Ici encore, il y a 3 tables en particulier qui nous intéressent :
    \begin{itemize}
        \item \textit{team}, qui enregistre les informations d'une équipe.
        \item \textit{postulate\_team}, qui gère les postulats à une équipe.
        \item \textit{player\_team}, qui enregistre quel joueur est dans quelle équipe.
    \end{itemize}

    Un joueur crée une équipe, il est donc son capitaine. Ensuite, d'autres joueurs peuvent postuler dans son équipe, et c'est à lui de choisir de les accepter ou de les refuser. De plus, aucune donnée de \textit{postulate\_team} n'est effacée, pour que nous puissions garder un historique des postulats. Ainsi, il y a 3 états possibles :
    \begin{itemize}
        \item \textit{accepted}, qui signifie que le joueur a été accepté dans l'équipe. Dans ce cas, les informations vont aussi être renseignées dans \textit{player\_team}.
        \item \textit{pending}, qui signifie que le postulat n'a pas encore été accepté ou refusé, le joueur est en attente.
        \item \textit{refused}, qui signifie que le joueur a été refusé. Il ne peut ainsi plus postuler dans cette équipe. 
    \end{itemize}

    \section{Tournois}

    \subsection{Inscription}

    Nous avons utilisé un système de postulats similaire pour les inscriptions. Cependant, ici, il n'y a pas de 3ème table pour savoir quelle équipe est inscrite. Il suffit simplement de regarder quelles équipes ont été acceptées dans \textit{postulate\_tournament}.
    
    \subsection{Type}

    Il y a plusieurs types de tournois renseignés dans la base de données : des coupes, des championnats, et des coupes avec poules. Ainsi, pour chaque type de tournoi, nous devons pouvoir assigner un score différent selon le résultat. C'est là que 3 tables entrent en jeu :

    \begin{itemize}
        \item \textit{tournament\_type}, pour le type de tournoi.
        \item \textit{outcome\_type}, pour les différents résultats possibles. Pour le moment, seuls \textquote{défaite}, \textquote{victoire} et \textquote{nul} existent.
        \item \textit{score\_tournament}, qui répertorie le score selon le type de tournoi et le résultat d'un match. Par exemple, en coupe, il n'y a que victoire et défaite. Impossible d'avoir un match nul. Donc par exemple, 1 point pour une victoire, et 0 pour une défaite. Cependant, en championnat, il est possible d'avoir un match nul. Il faut donc un système de points correspondant. Cette table existe pour configurer ce système.
    \end{itemize}

    \subsection{Matches}

    Les matches sont un peu délicats à représenter. Il faut d'un côté connaître les deux équipes et le tournoi, mais d'un autre, il faut aussi savoir quand le match a été joué, pour pouvoir faire un arbre de tournoi correct. Ainsi, nous avons décidé de faire un arbre binaire \textit{inversé}, c'est à dire, qu'au lieu de partir de la racine et d'avoir des enfants, tous les noeuds, à l'exception des feuilles, référencent leurs parents.

    De cette façon, nous partons des feuilles pour remonter petit à petit vers la racine de l'arbre, qui sera la finale. Cela permet aussi de gérer un nombre d'équipes aléatoire, et pas seulement une puissance de 2.
    
    %Ainsi, nous pouvons facilement construire un arbre de tournoi, en ayant un nombre d'équipe aléatoire. En effet, nous ne nous limitons pas à une puissance de 2. Il est facile de connaître le nombre de matches par \textit{round} grâce à la formule suivante :
    %Soit $n$ le nombre d'équipes du tournoi.
    %\[
    %    m = n - 2^{\lfloor log_2(n) \rfloor} 
    %\]
    %Par exemple, s'il y a 15 équipes, il devra y avoir $m = 15 - 2^3 = 7$ rencontres dans le premier \textit{round}, la dernière équipe sera introduite 

    \section{Le lieu}

    Pour cette partie, nous avons choisi de laisser les utilisateurs remplir notre base de données pour nous. Nous avons supposé que les tournois se déroulaient seulement en France pour cette première version du site. Il y a 3 tables importantes :
    
    \begin{itemize}
        \item \textit{city}, avec juste le nom d'une ville.
        \item \textit{zip\_code}, qui prend un code postal et une ville.
        \item \textit{location}, qui récupère une adresse et l'associe à un code postal.
    \end{itemize}

    Une fois que les utilisateurs entrent une ville et un code postal, ces informations seront ensuite proposées aux autres utilisateurs lorsqu'ils veulent entrer un nouveau lieu.


    \chapter{Les particularités techniques}

    %\epigraph{La factorisation est un outil de conception qu'il est préférable d'appliquer avant d'écrire beaucoup de code.}{Robert C. Martin, \textit{Agile software development principles, patterns, and practices}}

    \section{PHP} 

    Nous avons fait en sorte de factoriser au maximum notre code \textsc{php}. En effet, nous voulions éviter de faire la même chose sur plusieurs pages différentes, chose qui s'avère compliquée sur un site, notamment au niveau des formulaires.

    C'est pourquoi nous avons développé deux systèmes pour éviter de répéter les mêmes lignes pour chaque formulaire que nous créions.

    \subsection{Les validations} 

    Le premier système est celui des validations. En faisant des recherches, nous sommes tombés sur le système de validations de \textit{symfony}\footnote{Validator Symfony : \url{https://symfony.com/doc/current/validation.html}}. Nous nous sommes inspirés de ce modèle pour développer notre propre système. Il permet de créer des contraintes sur chaque champ d'un formulaire puis de les tester. 

    Pour chaque contrainte, un objet \textit{Result} est créé. Il possède deux propriétés. La première est la validité du champ. La seconde concerne les messages d'erreurs sur le champ. Si le champ est valide, cette dernière recevra un tableau vide.
    
    C'est un système facilement extensible, attribut qu'il nous fallait absolument. En effet, un ensemble de contraintes a été développé au début de ce système. Cependant, nous n'avons pas pensé à certaines contraintes, et donc nous avons dû les ajouter par la suite. C'était très facile, autant à ajouter qu'à utiliser. La flexibilité de ce système et sa facilité d'utilisation nous a permis de faire des vérifications extensibles sur les différents formulaires très facilement. 

    \subsection{Le rendu de formulaires}

    Suite au développement de ce premier système, nous avons eu l'idée de factoriser les erreurs des formulaires. En effet, comme dit précédemment, nous voulions éviter au maximum d'avoir du \textsc{php} dans nos pages \textsc{html}.

    De plus, nous avions maintenant un système « normalisé » de résultats, le mieux serait donc aussi de faire un système « normalisé » de formulaires.

    C'est ainsi qu'est née l'idée de la classe \textit{FormRenderer}. Grâce à cette classe, le programmeur \textsc{html} ne s'occupe que du \textsc{html}. Il ne s'occupe pas des erreurs ou des résultats valides possibles. De plus, les erreurs sont gérées automatiquement par cette nouvelle classe. Elle facilite ainsi la vie de tout le monde.

    Cette classe utilise une structure assez similaire à celle de \textit{PageRenderer} : 
    \begin{itemize}
        \item Calcul du \textit{DOMDocument}.
        \item Rendu des erreurs, ou des champs valides, par les classes-filles.
        \item Affichage du formulaire sur la page.
    \end{itemize}

    Les données entrées par l'utilisateur sont sauvegardées, pour être replacées ensuite dans le formulaire, afin de savoir où il a des erreurs, et pourquoi c'est une erreur. Les champs valides sont aussi gardés, car c'est plus simple de ne pas les ressaisir. 

    De plus, un système de stockage des fichiers temporaires a été créé. Si l'utilisateur téléverse un fichier, et qu'il y a des erreurs dans le formulaire, les fichiers téléversés vont être sauvegardés dans un répertoire temporaire, et leurs noms seront écrits, par défaut, dans les champs associés. Cela permet d'éviter d'avoir à re-téléverser un fichier un peu lourd.

    \subsection{Les rôles}

    En plus de ces deux systèmes, nous avons aussi une classe de rôles, qui permet à un utilisateur d'avoir un ou plusieurs rôles.
    
    Pour ce faire, nous avons utilisé un système de \textit{flags} : un drapeau (bit) est levé pour chaque rôle que l'utilisateur possède. Par exemple, un administrateur aura les drapeaux suivants : 

    \begin{center}
        \begin{tabular}{|c|c|c|c|}
            \hline 
            0 & 0 & 0 & 1 \\ \hline
        \end{tabular}
    \end{center}

    Si un utilisateur est joueur et administrateur, ses drapeaux ressembleront plutôt à ceci :

    \begin{center}
        \begin{tabular}{|c|c|c|c|}
            \hline 
            0 & 1 & 0 & 1 \\ \hline
        \end{tabular}
    \end{center}

    Bien sûr, c'est la représentation binaire des nombres qui nous intéresse ici. En \textsc{php}, cela sera stocké dans des entiers. Un administrateur aura alors le rôle qui est égal à 1, et un administrateur et joueur aura le rôle égal à 5.

    \section{Javascript}

    Nous avons réalisé beaucoup moins de \textit{javascript} que de \textsc{php} car, dans le planning de développement initial, notre objectif était d'ajouter du javascript sur la version 2 du site. Cependant, nous avons tout de même développé deux systèmes assez complets.

    \subsection{Les filtres de recherche}

    Dans notre site, il y a plusieurs tableaux qui représentent des données. Notamment, nous avons le tableau qui liste les tournois, ainsi que celui qui liste les équipes. À défaut d'opter pour une barre de recherche classique, qui ne filtre qu'un seul champ, nous nous sommes dirigés sur un système de conditions.
    
    Chaque condition correspond à une colonne du tableau de données. Dans la plupart des cas, une colonne du tableau correspond à une colonne de la table associée dans la base de données.

    De plus, les conditions sont connectés logiquement par un « ET », ce qui permet d'affiner facilement les recherches.

    Ce système est facilement utilisable sur plusieurs pages différentes. Il n'a besoin que d'un champ de type \textit{select} dans la page \textsc{html} qui présente toutes les clés possibles, puis de configurer tous les champs d'entrée en \textit{javascript}.

    Lorsque l'utilisateur clique sur le bouton de soumission du formulaire, tous les filtres qu'il a ajoutés sont alors envoyés sur la page d'\textit{action} définie du formulaire. Les champs qui n'ont pas été ajoutés ne seront pas envoyés.

    \subsection{L'ajax}

    Nous avons également fait un petit peu d'\textit{ajax}\footnote{\textsc{ajax} : Asynchronous JavaScript and XML}, afin de faciliter la vie aux utilisateurs. Nous avons une tâche assez répétitive dans le site : accepter ou refuser des postulats. 

    Il peut y avoir une somme assez élevée de postulats, que ce soit pour un capitaine d'équipe ou bien pour un gestionnaire de tournois. De plus, pour gérer ces postulats, il faut ouvrir un modal. Ainsi, ce n'était pas vraiment \textit{user-friendly} de devoir recharger la page à chaque postulat accepté ou refusé. 

    Par conséquent, nous avons développé un ensemble de fonctions pour chaque page où nous voulions ce système.

    %\subsection{Affichage des tournois}

    \chapter{Conclusion}

    \section{Travail réalisé}

    \noindent
    Pour résumé, notre site possède les fonctionnalités suivantes :

    \begin{itemize}
        \item Création de compte et permissions : joueur, gestionnaire de tournoi, administrateur.
        \item Création de tournoi de type coupe par l'administrateur.
        \item Gestion d'un ou plusieurs tournois par un gestionnaire.
        \item Création des équipes et inscription sur un ou plusieurs tournois.
        \item Affichage public des tournois et des matches.
        \item Gestion des rôles des différents comptes par l'administrateur.
    \end{itemize}

    \section{Les difficultés organisationnelles}

    Durant ce projet, nous n'avons pas spécialement rencontré de difficultés techniques. Les difficultés principales étaient sur le plan de l'organisation. En effet, malgré un \textit{git} méthodique et structuré, nous nous sommes aperçus d'une chose importante : tout le monde n'a pas le même niveau en programmation.
    
    C'est une observation qui paraît évidente. Seulement, cela nous a frappé de plein fouet. En effet, comme le niveau des membres du groupe est hétérogène, nous n'avançons pas tous à la même allure. Ce qui est apparent pour certains, ne l'est pas pour d'autres. 

    Ainsi, cela nous a fait nous repencher sur le planning de développement, qui était initialement très ambitieux. 

    \section{Perspectives}

    Le site est actuellement fonctionnel, et il pourrait être utilisé tel quel. Cependant, il y a plusieurs choses qui n'ont pas encore été faites et qui pourraient améliorer grandement l'expérience d'un utilisateur :

    \begin{itemize}
        \item La gestion de différent types de tournois (pour le moment, seulement les coupes sont gérées).
        \item Le moyen de réinitialiser le mot de passe d'un utilisateur grâce à un mail dédié.
        \item L'activation d'un compte grâce à un mail dédié.
        \item Un système de seeding pour que les meilleures équipes se rencontrent le plus tard possible dans la coupe.
    \end{itemize}

    \section{Conclusions tirées}

    Ce projet nous a appris quelque chose d'important sur le travail en équipe. Nous avons compris qu'avoir une méthode de travail structurée et méticuleuse ne suffit pas à la réussite d'un projet, et qu'il faut aussi \textit{savoir} travailler en équipe. Communiquer, collaborer, et s'entraider, plutôt que de faire chacun sa chose dans son coin et espérer que tout marche quand nous mettons en commun.

    Pour conclure, ce projet nous a apporté de nouvelles perspectives sur le travail de groupe, et a ainsi grandement enrichi notre expérience. 

    \begin{appendices}

        % FORMAT DES CHAPITRES
\titleformat
{\chapter} % command
[display] % shape
{\bfseries\Large\itshape} % format
{Annexe \ \thechapter} % label
{0ex} % sep
{
    \rule{\textwidth}{1pt}
    \centering
} % before-code
[
\vspace{-1.5ex}%
\rule{\textwidth}{0.3pt}
] % after-code  
% FIN FORMAT DES CHAPITRES 

    \chapter{Planning de développement}
    \label{sec:B}
 
    \begin{center}
        \includegraphics[trim=0 0 2000 0, angle=-90, origin=c, scale=0.7]{resources/planning-final.png}{\thispagestyle{empty}}\clearpage
        \includegraphics[trim=867 0 750 0, angle=-90, origin=c, scale=0.7]{resources/planning-final.png}{\thispagestyle{empty}}\clearpage
        \includegraphics[trim=2000 0 0 0, angle=-90, origin=c, scale=0.7]{resources/planning-final.png}\clearpage
    \end{center}

    \chapter{Schéma complet de la base de données}
    \label{sec:D}

    \begin{center}
        \includegraphics[trim=0 0 1500 0, angle=-90,scale=0.6]{resources/database_schema_final.png}{\thispagestyle{empty}}
        \includegraphics[trim=995 0 0 0, angle=-90,scale=0.6]{resources/database_schema_final.png}{\thispagestyle{empty}}\clearpage
    \end{center}

    \end{appendices}

\end{document}