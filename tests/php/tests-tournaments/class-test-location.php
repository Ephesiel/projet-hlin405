<?php 
require_once __DIR__ . '/../site-tests-header.php';

final class TestLocation extends MultipleTest {
    // ------------------------------------------------------------------------
    // Depuis des données brutes

    public function locationsProvider() : array {
        return array(
            array(
                array(
                    'id' => 1,
                    'code' => '34000',
                    'address1' => 'Boulevard de Lodève',
                    'address2' => '',
                    'city' => 'Montpellier',
                ),
                '34', 
                false
            ),
            array(
                array(1, '34000', 'test'),
                '34',
                true
            ),
            array(
                array('id' => 1, 'code' => '75000', 'address1' => 'Test', 'address2' => null, 'city' => 'Paris'),
                '75',
                false
            ),
            array(
                array('id' => '2', 'code' => '75000', 'address1' => '', 'address2' => null, 'city' => ''),
                '75',
                true
            ),
            array(
                array('id' => 2, 'code' => '34', 'address1' => null, 'address2' => null, 'city' => ''),
                '34',
                true
            ),
            array(
                array('id' => 42, 'code' => '02345', 'address1' => '', 'address2' => '', 'city' => ''),
                '02',
                false
            ),
        );
    }

    /**
     * @dataProvider locationsProvider
     */
    public function testLocation(array $raw_datas, string $expected_department, bool $expect_exception) : void {
        if ($expect_exception) {
            $this->expectException(Exception::class);
        }
        $location = PH\Location::fromRawData($raw_datas);

        if (!$expect_exception) {
            $this->assertSame($location->getId(), $raw_datas['id']);
            $this->assertSame($location->getZipCode(), $raw_datas['code']);
            $this->assertSame($location->getAddress(), $raw_datas['address1']);
            $this->assertSame($location->getAddressComplement(), is_null($raw_datas['address2']) ? '' : $raw_datas['address2']);
            $this->assertSame($location->getCity(), $raw_datas['city']);
            $this->assertSame($location->getDepartment(), $expected_department);
        }
    }

    // --------------------------------------------------------------------------------------------
    // Depuis une requête.

    /**
     * @dataProvider multipleTests
     * @group longTests
     */
    public function testLocationFromId() : void {
        global $phdb;

        $city_id = ph_test_create_new_unique_value_for_table('city');
        $city_name = ph_test_create_new_unique_value_for_table('city', 'name', 'string');
        $zip_code_id = ph_test_create_new_unique_value_for_table('zip_code');
        $zip_code = ph_test_create_new_unique_value_for_table('zip_code', 'code', 'string', array('length' => 5, 'chars' => '0123456789'));
        $location_id = ph_test_create_new_unique_value_for_table('location');

        $address = '59 boulevard de lodève';
        $phdb->runInstallation(
            "INSERT INTO city VALUES($city_id, '$city_name');
             INSERT INTO zip_code VALUES($zip_code_id, '$zip_code', $city_id);
             INSERT INTO location VALUES($location_id, '$address', '', $zip_code_id)"
        );

        $loc = PH\Location::fromId($location_id);

        $this->assertSame($loc->getId(), $location_id);
        $this->assertSame($loc->getZipCode(), $zip_code);
        $this->assertSame($loc->getAddress(), $address);
        $this->assertSame($loc->getAddressComplement(), '');
        $this->assertSame($loc->getCity(), $city_name);
        $this->assertSame($loc->getDepartment(), substr($zip_code, 0, 2));

        $phdb->runInstallation(
            "DELETE FROM city WHERE id = $city_id;
             DELETE FROM zip_code WHERE id = $zip_code_id;
             DELETE FROM location WHERE id = $location_id"
        );

        $phdb->resetCache();

        $this->expectException(Exception::class);
        $loc = PH\Location::fromId($location_id);
    }
}