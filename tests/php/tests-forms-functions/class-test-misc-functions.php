<?php 
require_once __DIR__ . '/../site-tests-header.php';
use PHPUnit\Framework\TestCase;

final class TestMiscFunctions extends TestCase {

    public function fileProvider() : array {
        return array(
            array(
                array('error' => UPLOAD_ERR_OK, 'size' => 10, 'name' => 'a.txt', 'tmp_name' => __DIR__ . '/files/a.txt'),
                true
            ),
            array(
                array('error' => UPLOAD_ERR_OK, 'size' => 10, 'name' => 'b.txt', 'tmp_name' => __DIR__ . '/files/b.txt'),
                true
            ),
            array(
                array('error' => UPLOAD_ERR_OK, 'size' => 10, 'name' => 'c.txt', 'tmp_name' => __DIR__ . '/files/c.txt'),
                true
            ),
            array(
                array('error' => UPLOAD_ERR_NO_FILE, 'size' => 0, 'name' => 'd.txt', 'tmp_name' => __DIR__ . '/files/d.txt'),
                false
            ),
        );
    }


    /**
     * @dataProvider fileProvider
     */
    public function testSaveUploadFile(array $file, bool $success) {
        $name = ph_save_upload_file($file, '');

        if ($success) {
            $file_name = ph_get_upload_path($name);
            $this->assertTrue(file_exists($file_name));
            $this->assertTrue(rename($file_name, $file['tmp_name']));
        }
        else {
            $this->assertNull($name);
        }

        $name = ph_save_upload_file($file, '', 'test');

        if ($success) {
            $file_name = ph_get_upload_path($name);
            $this->assertTrue(file_exists($file_name));
            $this->assertTrue(rename($file_name, $file['tmp_name']));
        }
        else {
            $this->assertNull($name);
        }

        $name = ph_save_upload_file($file, 'dir', 'test');

        if ($success) {
            $file_name = ph_get_upload_path($name);
            $this->assertTrue(file_exists($file_name));
            $this->assertTrue(rename($file_name, $file['tmp_name']));

            $dir = ph_get_upload_path('dir');
            $this->assertTrue(is_dir($dir));
    
            // Si le répertoire est vide (seulement . et ..)
            if (count(scandir($dir)) == 2) {
                $this->assertTrue(rmdir($dir));
            }
        }
        else {
            $this->assertNull($name);
        }
    }
}