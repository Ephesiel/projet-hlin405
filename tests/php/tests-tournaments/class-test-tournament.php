<?php
require_once __DIR__ . '/../site-tests-header.php';

final class TestTournament extends MultipleTest {
    // ------------------------------------------------------------------------
    // Depuis des données brutes.

    public function testRawDataTournament() : void {
        $name = 'Tournoi';
        $date = '2021-04-01';
        $preregistrations = '2021-03-30';
        $duration = 7;
        $type = 'Coupe';
        $location = array(
            'id' => 1,
            'code' => 34000,
            'address1' => 'Boulevard de Lodève',
            'address2' => '',
            'city' => 'Montpellier',
        );
        $manager = 1;

        $tournament = PH\Tournament::fromRawData(array(
            'id' => 1,
            'name' => $name,
            'starting-date' => $date,
            'end-inscription' => $preregistrations,
            'duration' => $duration,
            'type' => $type,
            'location' => $location,
            'manager' => $manager
        ));

        $this->assertSame($tournament->getName(), $name);
        $this->assertSame($tournament->getType(), $type);
        $this->assertSame($tournament->getLocation()->getId(), 1);
        $this->assertSame($tournament->getStatus(), Status::fromDate($preregistrations, $date, $duration));
        $this->assertSame($tournament->getStatusString(), Status::toString(Status::fromDate($preregistrations, $date, $duration)));
        $this->assertSame($tournament->getDuration(), $duration);
        $this->assertSame($tournament->getFormattedStartingDate('d/m/Y'), '01/04/2021');
        $this->assertSame($tournament->getFormattedEndingDate('d/m/Y'), '07/04/2021');
    }

    // --------------------------------------------------------------------------------------------
    // rawData exceptions.

    public function rawDataException1Provider() : array {
        return array(
            array(array(
                'name' => '',
                'starting-date' => '',
                'end-inscription' => '',
                'duration' => 1,
                'type' => '',
                'location' => array(),
            )),
            array(array(
                'id' => 1,
                'starting-date' => '',
                'end-inscription' => '',
                'duration' => 1,
                'type' => '',
                'location' => array(),
            )),
            array(array(
                'id' => 1,
                'name' => '',
                'duration' => 1,
                'type' => '',
                'location' => array(),
            )),
            array(array(
                'id' => 1,
                'name' => '',
                'starting-date' => '',
                'type' => '',
                'location' => array(),
            )),
            array(array(
                'id' => 1,
                'name' => '',
                'starting-date' => '',
                'duration' => 1,
                'location' => array(),
            )),
            array(array(
                'id' => 1,
                'name' => '',
                'starting-date' => '',
                'duration' => 1,
                'type' => '',
            )),
            array(array()),
        );
    }

    /**
     * @dataProvider rawDataException1Provider
     */
    public function testRawDataException1(array $raw_data) : void {
        $this->expectException(Exception::class);
        $tournament = PH\Tournament::fromRawData($raw_data);
    }

    public function rawDataException2Provider() : array {
        return array(
            array(array(
                'id' => '',
                'name' => '',
                'starting-date' => '',
                'end-inscription' => '',
                'duration' => 1,
                'type' => '',
                'location' => array(),
            )),
            array(array(
                'id' => 1,
                'name' => 2,
                'starting-date' => '',
                'end-inscription' => '',
                'duration' => 1,
                'type' => '',
                'location' => array(),
            )),
            array(array(
                'id' => 1,
                'name' => '',
                'starting-date' => array(),
                'end-inscription' => '',
                'duration' => 1,
                'type' => '',
                'location' => array(),
            )),
            array(array(
                'id' => 1,
                'name' => '',
                'starting-date' => '',
                'end-inscription' => '',
                'duration' => '',
                'type' => '',
                'location' => array(),
            )),
            array(array(
                'id' => 1,
                'name' => '',
                'starting-date' => '',
                'end-inscription' => '',
                'duration' => 1,
                'type' => 4,
                'location' => array(),
            )),
            array(array(
                'id' => 1,
                'name' => '',
                'starting-date' => '',
                'end-inscription' => '',
                'duration' => 1,
                'type' => '',
                'location' => '',
            )),
            array(array(
                'id' => '',
                'name' => 3,
                'starting-date' => array(),
                'end-inscription' => '',
                'duration' => '',
                'type' => 2,
                'location' => array(),
            )),
        );
    }

    /**
     * @dataProvider rawDataException2Provider
     */
    public function testRawDataException2(array $raw_data) : void {
        $this->expectException(Exception::class);
        $tournament = PH\Tournament::fromRawData($raw_data);
    }

    // --------------------------------------------------------------------------------------------
    // Depuis une requête.

    /**
     * @dataProvider multipleTests
     * @group longTests
     */
    public function testTournamentFromId() : void {
        global $phdb;

        $city_id = ph_test_create_new_unique_value_for_table('city');
        $city_name = ph_test_create_new_unique_value_for_table('city', 'name', 'string');
        $zip_code_id = ph_test_create_new_unique_value_for_table('zip_code');
        $zip_code = ph_test_create_new_unique_value_for_table('zip_code', 'code', 'string', array('length' => 5, 'chars' => '0123456789'));
        $location_id = ph_test_create_new_unique_value_for_table('location');
        $manager_id = ph_test_create_new_unique_value_for_table('user');
        $manager_email = ph_test_create_new_unique_value_for_table('user', 'email', 'string');
        $tournament_id = ph_test_create_new_unique_value_for_table('tournament');

        $address = '59 boulevard de lodève';
        $phdb->runInstallation(
            "INSERT INTO city VALUES($city_id, '$city_name');
             INSERT INTO zip_code VALUES($zip_code_id, '$zip_code', $city_id);
             INSERT INTO location VALUES($location_id, '$address', '', $zip_code_id);
             INSERT INTO user VALUES($manager_id, '$manager_email', 'test', 'test', null);
             INSERT INTO user_role VALUES($manager_id, 2);
             INSERT INTO tournament VALUeS($tournament_id, 'Tournoi test', '2021-04-01', '2021-03-30', 10, $manager_id, $location_id, 1)"
        );

        $trnm = PH\Tournament::fromId($tournament_id);

        $this->assertSame($trnm->getId(), $tournament_id);
        $this->assertSame($trnm->getName(), 'Tournoi test');
        $this->assertSame($trnm->getFormattedStartingDate('d/m/Y'), '01/04/2021');
        $this->assertSame($trnm->getFormattedEndingDate('d/m/Y'), '10/04/2021');
        $this->assertSame($trnm->getDuration(), 10);
        $this->assertSame($trnm->getStatus(), Status::fromDate('2021-03-30', '2021-04-01', 10));
        $this->assertSame($trnm->getStatusString(), Status::toString(Status::fromDate('2021-03-30', '2021-04-01', 10)));

        $phdb->runInstallation(
            "DELETE FROM city WHERE id = $city_id;
             DELETE FROM zip_code WHERE id = $zip_code_id;
             DELETE FROM location WHERE id = $location_id;
             DELETE FROM user WHERE id = $manager_id;
             DELETE FROM user_role WHERE user_id = $manager_id;
             DELETE FROM tournament WHERE id = $tournament_id"
        );

        $phdb->resetCache();

        $this->expectException(Exception::class);
        $loc = PH\Location::fromId($tournament_id);
    }
}