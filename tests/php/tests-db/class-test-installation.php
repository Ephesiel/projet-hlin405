<?php 
require_once __DIR__ . '/../site-tests-header.php';
use PHPUnit\Framework\TestCase;

final class TestInstallation extends TestCase {

    public function testRequireDb() : void {
        global $phdb;

        unset($phdb);
        $phdb = new PH\PHDB(DB_USER, DB_PASSWORD, DB_HOST, DB_NAME, DB_CHARSET, DB_COLLATE);
        $this->assertTrue($phdb->exists());
    }

    /**
     * @group serverTestsOnly
     */
    public function testDbContent() : void {
        $file = ph_db_force_repair();

        global $phdb;

        $phdb->resetCache();
        $this->assertTrue(ph_db_installed());
        $this->assertFalse(ph_db_differs());
        $this->assertFalse(ph_default_values_differs());

        unlink($file);
    }

    /**
     * @group serverTestsOnly
     * @depends testDbContent
     */
    public function testInstallationException() : void {
        global $phdb;

        $this->expectException(Exception::class);
        $this->expectExceptionMessage('Le schéma de la base de données ne doit pas être vide.');
        $phdb->runInstallation('');
    }

    /**
     * @group serverTestsOnly
     * @depends testDbContent
     */
    public function testDropTable() : void {
        global $phdb;

        $phdb->runInstallation('DROP TABLE postulate_tournament');
        
        $this->performUpdate();
    }

    /**
     * @group serverTestsOnly
     * @depends testDbContent
     */
    public function testAddTable() : void {
        global $phdb;

        $phdb->runInstallation('CREATE TABLE test_lul(id INT, PRIMARY KEY(id));');
        
        $this->performUpdate();
    }

    /**
     * @group serverTestsOnly
     */
    public function testRemoveColumn() : void {
        global $phdb;

        $phdb->runInstallation('ALTER TABLE postulate_tournament DROP COLUMN tournament_id');

        $this->performUpdate();
    }

    /**
     * @group serverTestsOnly
     * @depends testDbContent
     */
    public function testAddColumn() : void {
        global $phdb;

        $phdb->runInstallation('ALTER TABLE postulate_tournament ADD test INT');

        $this->performUpdate();
    }

    private function performUpdate() : void {
        global $phdb;

        $phdb->resetCache();
        $this->assertTrue(ph_db_differs());
        ph_update_db_tables();
        $phdb->resetCache();
        $this->assertFalse(ph_db_differs());
    }

    /**
     * @group serverTestsOnly
     * @depends testDbContent
     */
    public function testAdminModification() : void {
        global $phdb;

        $phdb->runInstallation('UPDATE user SET email = \'haha\', passwd = \'haha\', name = \'haha\' WHERE id = 1');

        $this->performValuesUpdate();
    }

    /**
     * @group serverTestsOnly
     * @depends testDbContent
     */
    public function testDropDefaultValue() : void {
        global $phdb;

        $phdb->runInstallation('DELETE FROM role WHERE id = 4');

        $this->performValuesUpdate();
    }

    private function performValuesUpdate() : void {
        global $phdb;

        $phdb->resetCache();
        $this->assertTrue(ph_default_values_differs());
        ph_update_default_values();
        $phdb->resetCache();
        $this->assertFalse(ph_default_values_differs());
    }
}