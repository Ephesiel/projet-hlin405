<?php

// ----------------------------------------------------------------------------
// Paramètres de la base de données.

define('DB_NAME', 'projet_hlin405');
define('DB_USER', 'root');
define('DB_PASSWORD', 'root');
define('DB_HOST', 'mysql');
define('DB_CHARSET', 'utf8mb4');
define('DB_COLLATE', 'utf8mb4_general_ci');

// ----------------------------------------------------------------------------
// Configuration du compte administrateur. Peut être changé à tout moment!

define('ADMIN_EMAIL', 'webmaster@localhost.com');
define('ADMIN_NAME', 'admin');
define('ADMIN_PASSWD', 'test');

// ----------------------------------------------------------------------------
// Constantes utilitaires en mode développement

define('RESET_PLACEHOLDERS', false);
