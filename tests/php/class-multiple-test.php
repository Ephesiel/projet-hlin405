<?php 
use PHPUnit\Framework\TestCase;

/**
 * Classe générique de test permettant l'assertion d etests multiple
 * Il suffit d'hériter de cette classe au lieu de TestCase et d'ensuite
 * ajouter à la documentation :
 * 
 * {at}depends multipleTests
 * 
 * Le test sera alors fait 100 fois
 */
class MultipleTest extends TestCase {
    public function multipleTests() : array {
        return array_fill(0, 20, array());
    }

    public function testTest() : void {
        $this->assertTrue(true);
    }
}