<?php
require_once __DIR__ . '/../site-tests-header.php';
use PHPUnit\Framework\TestCase;

/**
 * Cette classe a pour but de permettre les tests sur la classe abstraite \PH\Templates\SiteLayout.
 * Il n'y a aucun méthode abstraite.
 * 
 * @author Benoît Huftier
 */
final class NonAbstractSiteLayout extends \PH\Templates\SiteLayout {
    public function __construct() {
        parent::__construct();
    }

    protected function configurateHeader() : void {
        parent::configurateHeader();
        $this->setHeader(__DIR__ . '/includes/test-header.php');
    }

    protected function configurateFooter() : void {
        parent::configurateFooter();
        $this->setFooter(__DIR__ . '/includes/test-footer.php');
    }
}

final class TestTemplate extends TestCase {
    public function testFooterHeader() : void {
        $site_layout = new NonAbstractSiteLayout;
        $site_layout->setBody(__DIR__ . '/includes/simple-body.php');

        $header = preg_quote(file_get_contents(__DIR__ . '/includes/test-header.php'), '/');
        $footer = preg_quote(file_get_contents(__DIR__ . '/includes/test-footer.php'), '/');
        $content = preg_quote(file_get_contents(__DIR__ . '/includes/simple-body.php'), '/');
        $this->expectOutputRegex("/(?s)<body>.*$header.*$content.*$footer.*<\/body>/");
        $site_layout->render();
    }

    public function forbidden() : void {
        throw new \Exception('forbidden site layout');
    }

    public function testForbidden() : void {
        $site_layout = new NonAbstractSiteLayout;
        $site_layout->setActionOnForbidden(array($this, 'forbidden'));

        try {
            $site_layout->forbidAccessIfNotPermitted();
        }
        catch (\Exception $e) {
            $this->fail("Une exception a été levée et n'aurait pas dû : {$e->getMessage()}");
        }

        $site_layout->setPermissionsNeeded(Role::Connected);

        try {
            $site_layout->forbidAccessIfNotPermitted();
            $this->fail('L\'utilisateur n\'est pas censé avoir le droit de regarder la page');
        }
        catch (\Exception $e) {
            $this->assertSame($e->getMessage(), 'forbidden site layout');
        }

        ph_connect_user(ADMIN_EMAIL);

        $site_layout->setPermissionsNeeded(Role::Administrator);

        try {
            $site_layout->forbidAccessIfNotPermitted();
        }
        catch (\Exception $e) {
            $this->fail("Une exception a été levée et n'aurait pas dû : {$e->getMessage()}");
        }
    }

    public function testBootstrapContent() : void {
        $bootstrap_page = new \PH\Templates\BootstrapPage;
        $bootstrap_page->setBody(__DIR__ . '/includes/simple-body.php');

        $bootstrap_css = preg_quote('https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css', '/');
        $bootstrap_js = preg_quote('https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js', '/');
        $content = preg_quote(file_get_contents(__DIR__ . '/includes/simple-body.php'), '/');
        $this->expectOutputRegex("/(?s)<head>.*<link [^<>]*href=\"$bootstrap_css\"[^<>]*>.*<\/head>.*<body>.*<div class=\"container\">.*$content.*<\/div>.*<script [^<>]*src=\"$bootstrap_js\"[^<>]*>[^<>]*<\/script>.*<\/body>/");
        $bootstrap_page->render();
    }
}