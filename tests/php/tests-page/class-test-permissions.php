<?php
require_once __DIR__ . '/../core-tests-header.php';
use PHPUnit\Framework\TestCase;

abstract class RoleTest {
    const Administrator = 1 << 0;
    const Moderator = 1 << 1;
    const Public = 1 << 4;
    const Connected = RoleTest::Administrator | RoleTest::Moderator;
}

final class TestPermissions extends TestCase {
    public function testPermissions() : void {
        $all = new \Core\Permissions(0); 
        $this->assertFalse($all->hasAny(RoleTest::Connected | RoleTest::Public));
        $this->assertFalse($all->hasFlag(RoleTest::Administrator));
        $this->assertFalse($all->hasFlag(RoleTest::Moderator));
        $this->assertFalse($all->hasFlag(RoleTest::Public));
        $this->assertTrue($all->hasFlag(0));
        $this->assertEquals($all->getFlags(), 0);

        $public = new \Core\Permissions(RoleTest::Public); 
        $this->assertTrue($public->hasAny(RoleTest::Connected | RoleTest::Public));
        $this->assertFalse($public->hasAny(RoleTest::Connected));
        $this->assertFalse($public->hasFlag(RoleTest::Administrator));
        $this->assertFalse($public->hasFlag(RoleTest::Moderator));
        $this->assertTrue($public->hasFlag(RoleTest::Public));
        $this->assertTrue($public->hasFlag(0));
        $this->assertEquals($public->getFlags(), RoleTest::Public);

        $modo = new \Core\Permissions(RoleTest::Moderator); 
        $this->assertTrue($modo->hasAny(RoleTest::Connected | RoleTest::Public));
        $this->assertTrue($modo->hasAny(RoleTest::Connected));
        $this->assertFalse($modo->hasFlag(RoleTest::Administrator));
        $this->assertTrue($modo->hasFlag(RoleTest::Moderator));
        $this->assertFalse($modo->hasFlag(RoleTest::Public));
        $this->assertTrue($modo->hasFlag(0));
        $this->assertFalse($modo->hasFlag(RoleTest::Administrator | RoleTest::Moderator));
        $this->assertEquals($modo->getFlags(), RoleTest::Moderator);

        $admin = new \Core\Permissions(RoleTest::Administrator); 
        $this->assertTrue($admin->hasAny(RoleTest::Connected | RoleTest::Public));
        $this->assertTrue($admin->hasAny(RoleTest::Connected));
        $this->assertTrue($admin->hasFlag(RoleTest::Administrator));
        $this->assertFalse($admin->hasFlag(RoleTest::Moderator));
        $this->assertFalse($admin->hasFlag(RoleTest::Public));
        $this->assertTrue($admin->hasFlag(0));
        $this->assertFalse($admin->hasFlag(RoleTest::Administrator | RoleTest::Moderator));
        $this->assertEquals($admin->getFlags(), RoleTest::Administrator);

        $admin_modo = new \Core\Permissions(RoleTest::Administrator | RoleTest::Moderator); 
        $this->assertTrue($admin_modo->hasAny(RoleTest::Connected | RoleTest::Public));
        $this->assertTrue($admin_modo->hasAny(RoleTest::Connected));
        $this->assertTrue($admin_modo->hasFlag(RoleTest::Administrator));
        $this->assertTrue($admin_modo->hasFlag(RoleTest::Moderator));
        $this->assertFalse($admin_modo->hasFlag(RoleTest::Public));
        $this->assertTrue($admin->hasFlag(0));
        $this->assertTrue($admin_modo->hasFlag(RoleTest::Administrator | RoleTest::Moderator));
        $this->assertEquals($admin_modo->getFlags(), RoleTest::Administrator | RoleTest::Moderator);
        $this->assertEquals($admin_modo->getFlags(), RoleTest::Connected);
    }
}