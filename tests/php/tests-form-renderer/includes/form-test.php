<form method="POST" action="">
    <div class="row">
            <div class="mb-3">
                <label for="email" class="form-label">Adresse email</label>
                <input name="email" type="email" class="form-control" id="email" placeholder="nom@exemple.fr" >
            </div>
    </div>
    <div class="row">
        <div class="mb-3">
            <label for="password" class="form-label">Mot de passe</label>
            <input name="password" type="password" class="form-control" id="password" placeholder="">
        </div>
    </div>
    <div class="row">
        <div class="mb-3">
            <label for="profile-picture" class="form-label">Photo de profil</label>
            <input name="profile-picture" class="form-control" type="file" id="profile-picture" accept=".jpg,.gif,.png" />
        </div>
    </div>
    <div class="row">
        <div class="mb-3">
            <input name="test1" type="radio" class="form-check-input" id="test" value="1">
            <label for="test" class="form-check-label">Unvalidated</label>
            <input name="test1" type="radio" class="form-check-input" id="test1" value="2">
            <label for="test1" class="form-check-label">Validated</label>
            <input name="test1" type="radio" class="form-check-input" id="test2" value="3">
            <label for="test2" class="form-check-label">Noob</label>
        </div>
    </div>
    <div class="row">
        <div class="mb-3">
            <input name="test-bis" type="checkbox" class="form-check-input" id="test" value="1">
            <label for="test" class="form-check-label">Unvalidated</label>
            <input name="test-bis2" type="checkbox" class="form-check-input" id="test1" value="2" >
            <label for="test1" class="form-check-label">Validated</label>
            <input name="test-bis3" type="checkbox" class="form-check-input" id="test2" value="3" >
            <label for="test2" class="form-check-label">Noob</label>
        </div>
    </div>
</form>