<?php
require_once __DIR__ . '/../site-tests-header.php';

final class TestTeam extends MultipleTest {
    public function createContact() : array {
        global $phdb;

        $datas = array(
            'city_id' => ph_test_create_new_unique_value_for_table('city'),
            'city_name' => ph_test_create_new_unique_value_for_table('city', 'name', 'string'),
            'zip_code_id' => ph_test_create_new_unique_value_for_table('zip_code'),
            'zip_code' => ph_test_create_new_unique_value_for_table('zip_code', 'code', 'string', array('length' => 5, 'chars' => '0123456789')),
            'location_id' => ph_test_create_new_unique_value_for_table('location'),
            'contact_id' => ph_test_create_new_unique_value_for_table('contact')
        );

        $phdb->runInstallation(
            "INSERT INTO city VALUES({$datas['city_id']}, '{$datas['city_name']}');
             INSERT INTO zip_code VALUES({$datas['zip_code_id']}, '{$datas['zip_code']}', {$datas['city_id']});
             INSERT INTO location VALUES({$datas['location_id']}, 'Super adresse', '', {$datas['zip_code_id']});
             INSERT INTO contact VALUES({$datas['contact_id']}, '+33 0 00 00 00', 'email@contact.fr', {$datas['location_id']});"
        );

        return $datas;
    }

    public function deleteContact(array $datas) : void {
        global $phdb;

        $phdb->runInstallation(
            "DELETE FROM city WHERE id = {$datas['city_id']};
             DELETE FROM zip_code WHERE id = {$datas['zip_code_id']};
             DELETE FROM location WHERE id = {$datas['location_id']};
             DELETE FROM contact WHERE id = {$datas['contact_id']};"
        );
    }

    /**
     * @dataProvider multipleTests
     * @group longTests
     */
    public function testTeam() : void {
        global $phdb;

        $contact_datas = $this->createContact();

        // Création de l'équipe et du capitaine
        $captain_user_id = ph_test_create_new_unique_value_for_table('user');
        $captain_user_email = ph_test_create_new_unique_value_for_table('user', 'email', 'string');
        $captain_player_id = ph_test_create_new_unique_value_for_table('player');
        $team_id = ph_test_create_new_unique_value_for_table('team');
        $team_name = ph_test_create_new_unique_value_for_table('team', 'name', 'string');
        $captain_join_date = ph_test_create_new_unique_value_for_table('player_team', 'join_date', 'date', array('max' => date_create("now -10 days")->format('Y-m-d h:i:s')));

        $pp = null;

        if (rand(0, 1)) {
            $pp = 'test-pp.png';
        }

        $active = rand(0, 1);

        $phdb->runInstallation(
            "INSERT INTO user VALUES($captain_user_id, '$captain_user_email', 'test', 'test', null);
             INSERT INTO user_role VALUES($captain_user_id, 4);
             INSERT INTO player VALUES($captain_player_id, 'Super description', $captain_user_id);
             INSERT INTO player_team VALUES($team_id, $captain_player_id, '$captain_join_date', null);
             INSERT INTO team VALUES($team_id, '$team_name', 1, " . (is_null($pp) ? "null" : "'$pp'") . ", $active, $captain_player_id, {$contact_datas['contact_id']});"
        );

        $captain = PH\User::createFromId($captain_user_id);

        // Création de tous les joueurs de l'équipe et des postulats
        $nb_players = rand(1, 10);
        $nb_blocked = rand(1, 10);
        $nb_pending = rand(1, 10);
        $players = array($captain);
        $blocked = array();
        $pending = array();

        for ($i = 1; $i < $nb_players + $nb_blocked + $nb_pending; $i++) {
            $player_user_id = ph_test_create_new_unique_value_for_table('user');
            $player_email = ph_test_create_new_unique_value_for_table('user', 'email', 'string');
            $player_id = ph_test_create_new_unique_value_for_table('player');
            $player_date = ph_test_create_new_unique_value_for_table('player_team', 'join_date', 'date', array('min' => $captain_join_date, 'max' => date_create()->format('Y-m-d h:i:s')));

            $postulate = $i < $nb_players ? Postulate::Accepted : ($i < $nb_players + $nb_blocked ? Postulate::Refused : Postulate::Pending);

            $phdb->runInstallation(
                "INSERT INTO user VALUES($player_user_id, '$player_email', 'test', 'test', null);
                 INSERT INTO user_role VALUES($player_user_id, 4);
                 INSERT INTO player VALUES($player_id, 'Super description', $player_user_id);
                 INSERT INTO postulate_team VALUES($team_id, $player_id, '$player_date', '$postulate')"
            );

            if ($postulate === Postulate::Accepted) {
                $phdb->query("INSERT INTO player_team VALUES($team_id, $player_id, '$player_date', null);");
            }

            $player = PH\User::createFromId($player_user_id);

            switch ($postulate) {
                case Postulate::Accepted:
                    $players[] = $player;
                    break;
                case Postulate::Refused:
                    $blocked[] = $player;
                    break;
                case Postulate::Pending:
                    $pending[] = $player;
                    break;
            }
        }

        $team1 = PH\Team::createFromId($team_id);
        $team2 = PH\Team::createFromName($team_name);
        
        $this->assertEquals($team1->getId(), $team_id);
        $this->assertEquals($team1->getName(), $team_name);
        $this->assertEquals($team1->getLevel(), 1);
        $this->assertEquals($team1->isActive(), (bool) $active);
        $this->assertEquals($team1->getContactInformations()['email'], 'email@contact.fr');
        $this->assertEquals($team1->getContactInformations()['phone'], '+33 0 00 00 00');
        $this->assertTrue($team1->getContactInformations()['location'] instanceof PH\Location);
        $this->assertTrue($team1->getCaptain()->sameUserThan($captain));
        $this->assertTrue($captain->sameUserThan($team1->getCaptain()));
        $this->assertTrue($team1->hasPlayer($captain));
        $this->assertTrue(ph_is_captain($captain));

        if (is_null($pp)) {
            $this->assertEquals($team1->getProfilePicture(), ph_get_upload_link(PH_TEAM_DEFAULT_PROFILE_PICTURE));
        }
        else {
            $this->assertEquals($team1->getProfilePicture(), ph_get_upload_link($pp));
        }
        
        $this->assertEquals($team1->getId(), $team2->getId());
        $this->assertEquals($team1->getName(), $team2->getName());
        $this->assertEquals($team1->getLevel(), $team2->getLevel());
        $this->assertEquals($team1->getProfilePicture(), $team2->getProfilePicture());
        $this->assertEquals($team1->isActive(), $team2->isActive());
        $this->assertEquals($team1->getContactInformations(), $team2->getContactInformations());
        $this->assertTrue($team1->getCaptain()->sameUserThan($team2->getCaptain()));
        $this->assertTrue($team2->getCaptain()->sameUserThan($team1->getCaptain()));
        $this->assertTrue($team2->hasPlayer($captain));
        $this->assertEquals($team1->getPlayers(), $team2->getPlayers());
        $this->assertEquals($team1->getAcceptedPlayers(), $team2->getAcceptedPlayers());
        $this->assertEquals($team1->getRefusedPlayers(), $team2->getRefusedPlayers());
        $this->assertEquals($team1->getPendingPlayers(), $team2->getPendingPlayers());

        // On vérifie tous les joueurs
        $this->assertEquals(count($team1->getPlayers()), count($players));

        foreach ($team1->getPlayers() as $player) {
            $this->assertTrue($player instanceof PH\User);
            $this->assertTrue($player->isPlayer());
            $this->assertTrue($team1->hasPlayer($player));
            $this->assertTrue($team2->hasPlayer($player));
            $found = false;

            foreach ($players as $player2) {
                if ($player->sameUserThan($player2)) {
                    $found = true;
                    break;
                }
            }

            $this->assertTrue($found, 'Le joueur n\'existe pas dans la liste des joueurs de l\'équipe');
        }

        // On vérifie tous les postulats acceptés
        $this->assertEquals(count($team1->getAcceptedPlayers()), count($players) - 1);

        foreach ($team1->getAcceptedPlayers() as $player) {
            $player = $player['player'];
            $this->assertTrue($player instanceof PH\User);
            $this->assertTrue($player->isPlayer());
            $this->assertTrue($team1->hasPlayer($player));
            $this->assertTrue($team2->hasPlayer($player));
            $this->assertFalse(ph_is_captain($player));
            $this->assertEquals($team1->getPostulateTypeForPlayer($player), \Postulate::Accepted);
            $found = false;

            foreach ($players as $player2) {
                if ($player->sameUserThan($player2)) {
                    $found = true;
                    break;
                }
            }

            $this->assertTrue($found, 'Le joueur n\'existe pas dans la liste des joueurs acceptés de l\'équipe');
        }

        // On vérifie tous les postulats refusé
        $this->assertEquals(count($team1->getRefusedPlayers()), count($blocked));

        foreach ($team1->getRefusedPlayers() as $player) {
            $player = $player['player'];
            $this->assertTrue($player instanceof PH\User);
            $this->assertTrue($player->isPlayer());
            $this->assertFalse($team1->hasPlayer($player));
            $this->assertFalse($team2->hasPlayer($player));
            $this->assertFalse(ph_is_captain($player));
            $this->assertEquals($team1->getPostulateTypeForPlayer($player), \Postulate::Refused);
            $found = false;

            foreach ($blocked as $player2) {
                if ($player->sameUserThan($player2)) {
                    $found = true;
                    break;
                }
            }

            $this->assertTrue($found, 'Le joueur n\'existe pas dans la liste des joueurs blockés de l\'équipe');
        }

        // On vérifie tous les postulats en attente
        $this->assertEquals(count($team1->getPendingPlayers()), count($pending));

        foreach ($team1->getPendingPlayers() as $player) {
            $player = $player['player'];
            $this->assertTrue($player instanceof PH\User);
            $this->assertTrue($player->isPlayer());
            $this->assertFalse($team1->hasPlayer($player));
            $this->assertFalse($team2->hasPlayer($player));
            $this->assertFalse(ph_is_captain($player));
            $this->assertEquals($team1->getPostulateTypeForPlayer($player), \Postulate::Pending);
            $found = false;

            foreach ($pending as $player2) {
                if ($player->sameUserThan($player2)) {
                    $found = true;
                    break;
                }
            }

            $this->assertTrue($found, 'Le joueur n\'existe pas dans la liste des joueurs en attente de l\'équipe');
        }

        // Vérification qu'un utilisateur non joueur peut bien être comparé dans une équipe
        
        $user_id = ph_test_create_new_unique_value_for_table('user');
        $user_email = ph_test_create_new_unique_value_for_table('user', 'email', 'string');

        $phdb->runInstallation(
            "INSERT INTO user VALUES($user_id, '$user_email', 'test', 'test', null);
             INSERT INTO user_role VALUES($user_id, 2);"
        );

        $user = PH\User::createFromId($user_id);
        $this->assertFalse($team1->hasPlayer($user));
        $this->assertFalse(ph_is_captain($user));
        $this->assertEquals($team1->getPostulateTypeForPlayer($user), \Postulate::None);

        // Suppresion de l'équipe
        $phdb->runInstallation(
            "DELETE FROM team WHERE id = $team_id;
             DELETE FROM user WHERE id = $user_id;
             DELETE FROM user_role WHERE user_id = $user_id;"
        );

        // Suppresion de tous les joueurs
        foreach (array_merge($players, $blocked, $pending) as $player) {
            $player_user_id = $player->getId();
            $player_id = $player->getPlayerId();

            $phdb->runInstallation(
                "DELETE FROM user WHERE id = $player_user_id;
                 DELETE FROM user_role WHERE user_id = $player_user_id;
                 DELETE FROM player WHERE id = $player_id;
                 DELETE FROM player_team WHERE player_id = $player_id;
                 DELETE FROM postulate_team WHERE player_id = $player_id;"
            );
        }

        $this->deleteContact($contact_datas);

        $phdb->resetCache();
    }
    
    /**
     * @dataProvider multipleTests
     * @group longTests
     */
    public function testExceptionNoId() : void {
        $team_id = ph_test_create_new_unique_value_for_table('team');

        $this->expectException(Exception::class);
        PH\Team::createFromId($team_id);
    }
    
    /**
     * @dataProvider multipleTests
     * @group longTests
     */
    public function testExceptionNoName() : void {
        $team_name = ph_test_create_new_unique_value_for_table('team', 'name', 'string');

        $this->expectException(Exception::class);
        PH\Team::createFromName($team_name);
    }
    
    /**
     * @dataProvider multipleTests
     * @group longTests
     */
    public function testSearchFromName() : void {
        global $phdb;

        $contact_datas = $this->createContact();

        // Création de l'équipe et du capitaine
        $captain_user_id = ph_test_create_new_unique_value_for_table('user');
        $captain_user_email = ph_test_create_new_unique_value_for_table('user', 'email', 'string');
        $captain_player_id = ph_test_create_new_unique_value_for_table('player');
        $team_id = ph_test_create_new_unique_value_for_table('team');
        $team_name = ph_test_create_new_unique_value_for_table('team', 'name', 'string');

        $phdb->runInstallation(
            "INSERT INTO user VALUES($captain_user_id, '$captain_user_email', 'test', 'test', null);
             INSERT INTO user_role VALUES($captain_user_id, 4);
             INSERT INTO player VALUES($captain_player_id, 'Super description', $captain_user_id);
             INSERT INTO player_team VALUES($team_id, $captain_player_id, '2021-04-11 00:00:00', null);
             INSERT INTO team VALUES($team_id, '$team_name', 1, null, 1, $captain_player_id, {$contact_datas['contact_id']});"
        );

        for ($i = 0; $i < strlen($team_name); $i++) {
            $page = 0;
            $search = substr($team_name, $i);
            $found = false;

            do {
                $page++;
                $dummy = array();
                $teams = ph_get_teams_which_match(array('name' => $search), $page, $dummy);
                $found = array_key_exists($team_id, $teams['teams']);
            }
            while (!$found && count($teams) !== 0);

            $this->assertTrue($found);
        }
        
        // Suppresion de l'équipe
        $phdb->runInstallation(
            "DELETE FROM team WHERE id = $team_id;
             DELETE FROM user WHERE id = $captain_user_id;
             DELETE FROM user_role WHERE user_id = $captain_user_id;
             DELETE FROM player WHERE id = $captain_player_id;
             DELETE FROM player_team WHERE player_id = $captain_player_id;"
        );

        $this->deleteContact($contact_datas);

        $phdb->resetCache();
    }
}