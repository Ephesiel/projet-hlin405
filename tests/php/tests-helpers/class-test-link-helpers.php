<?php
require_once __DIR__ . '/../site-tests-header.php';
use PHPUnit\Framework\TestCase;

final class TestLinkHelpers extends TestCase {
    public function assetsProviders() : array {
        return array(
            array('test'),
            array('dossier/mon_fichier'),
            array('dossier/sous_dossier/truc')
        );
    }
    
    /**
     * @dataProvider assetsProviders
     */
    public function testAssets(string $filename) : void {
        $this->assertSame(ph_get_css_link($filename), ROOT . '/assets/css/' . $filename);
        $this->assertSame(ph_get_js_link($filename), ROOT . '/assets/js/' . $filename);
        $this->assertSame(ph_get_upload_link($filename), ROOT . '/assets/uploads/' . $filename);
        $this->assertSame(ph_get_resource_link($filename), ROOT . '/assets/resources/' . $filename);
    }
    
    /**
     * @dataProvider assetsProviders
     */
    public function testAbsoluteLink(string $filename) : void {
        $this->assertSame(ph_include($filename), ABSPATH . '/includes/' . $filename);
        $this->assertSame(ph_get_body($filename), ABSPATH . '/html/' . $filename);
        $this->assertSame(ph_get_upload_path($filename), ABSPATH . '/public_html/assets/uploads/' . $filename);
    }
    
    /**
     * @dataProvider assetsProviders
     */
    public function testTagsObject(string $filename) : void {
        $css_tag = ph_create_css_object($filename);
        $css_link = ph_get_css_link($filename);
        $css_link = preg_quote($css_link, '/');
        $css_reg = "/href=('$css_link'|\"$css_link\")/";
        $this->assertMatchesRegularExpression($css_reg, $css_tag->make());
        
        $js_tag = ph_create_js_object($filename);
        $js_link = ph_get_js_link($filename);
        $js_link = preg_quote($js_link, '/');
        $js_reg = "/src=('$js_link'|\"$js_link\")/";
        $this->assertMatchesRegularExpression($js_reg, $js_tag->make());
    }
    
    public function testRoute() : void {
        $this->assertSame(ph_get_route_link('test.php'), ROOT . '/test');
        $this->assertSame(ph_get_route_link('dossier/test.php'), ROOT . '/dossier/test');
        $this->assertSame(ph_get_route_link('dossier/sous_dossier/test.php'), ROOT . '/dossier/sous_dossier/test');
        $this->assertSame(ph_get_route_link('test.html'), ROOT . '/test');
        $this->assertSame(ph_get_route_link('test.php', array('arg' => 'value', 'arg2' => 'value')), ROOT . '/test?arg=value&arg2=value');
        $this->assertSame(ph_get_route_link('test.php', array('arg' => 'value', 'arg' => 'value2')), ROOT . '/test?arg=value2');
        $this->assertSame(ph_get_route_link('index.php'), ROOT . '/');
        $this->assertSame(ph_get_route_link('index.php', array('arg' => 'value')), ROOT . '/?arg=value');
        $this->assertSame(ph_get_route_link('index.php', array('arg' => 'value', 'arg2' => 'value')), ROOT . '/?arg=value&arg2=value');
        $this->assertSame(ph_get_route_link('index.php', array('arg' => 'value', 'arg' => 'value2')), ROOT . '/?arg=value2');
    }
}